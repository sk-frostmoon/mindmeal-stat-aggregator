<?php

use Access\AccessRules;

$bu = Yii::app()->baseUrl;

unset($_SERVER['DEV']);

$cs = Yii::app()->clientScript;
$cs->registerCssFile($bu . 'css/bootstrap.min.css');
$cs->registerCssFile($bu . 'css/font-awesome.min.css');
$cs->registerCssFile($bu . 'css/ionicons.min.css');
$cs->registerCssFile($bu . 'css/morris/morris.css');
#$cs->registerCssFile($bu . 'css/jvectormap/jquery->jvectormap-1.1.2.css');
$cs->registerCssFile($bu . 'css/fullcalendar/fullcalendar.css');
$cs->registerCssFile($bu . 'css/daterangepicker/daterangepicker-bs3.css');
$cs->registerCssFile($bu . 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');
$cs->registerCssFile($bu . 'css/AdminLTE.css');
$cs->registerCssFile($bu . 'css/iCheck/flat/red.css');
$cs->registerCssFile($bu . 'css/iCheck/flat/green.css');

$cs->registerCoreScript('jquery');
$cs->registerScriptFile('js/bootstrap.min.js');
$cs->registerScriptFile('js/jquery-ui-1.10.3.min.js');

$cs->registerScriptFile('/js/AdminLTE/app.js');

#$cs->registerScriptFile();

$rootDir = __DIR__ . "/../../../../";
$statSrc = $rootDir . "stat_src/src/";
include_once $rootDir . "stat_src/bootstrap/path.php";
Yii::setPathOfAlias('Predis',     $rootDir . "vendor/predis/predis/src");
Yii::setPathOfAlias('User',       $statSrc . 'User');
Yii::setPathOfAlias('Storage',    $statSrc . 'Storage');
Yii::setPathOfAlias('Utils',      $statSrc . 'Utils');
Yii::setPathOfAlias('Statistics', $statSrc . 'Statistics');
Yii::setPathOfAlias('Event',      $statSrc . 'Event');
Yii::setPathOfAlias('Ad',         $statSrc . 'Ad');
Yii::setPathOfAlias('Report',     $statSrc . 'Report');
Yii::setPathOfAlias('Access',     $statSrc . 'Access');

if (defined('YII_DEBUG') && YII_DEBUG) {
    exec('cat /etc/hostname', $hostname);
}

$User = User::model()->findByPk(Yii::app()->user->id);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title><?=Yii::app()->name;?></title>
        <!-- script type="text/javascript" src="<?=$bu?>js/jquery.min.js"></script -- >

        <!-- script type="text/javascript" src="<?=$bu?>js/jquery-ui-1.10.3.min.js"></script>
        <script type="text/javascript" src="<?=$bu?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?=$bu?>js/AdminLTE/app.js"></script -->
        <!-- Morris.js charts -->
        <!-- script type="text/javascript" src="<?=$bu?>js/plugins/flot/jquery.flot.js"></script>
        <script type="text/javascript" src="<?=$bu?>js/plugins/flot/jquery.flot.resize.min.js"></script>
        <script type="text/javascript" src="<?=$bu?>js/plugins/flot/jquery.flot.crosshair.min.js"></script>
        <!- - script type="text/javascript" src="../../js/plugins/flot/jquery.flot.pie.min.js"></script- ->
        <script type="text/javascript" src="<?=$bu?>js/plugins/flot/jquery.flot.categories_mm.js"></script-->

    </head>
    <body class="skin-blue wysihtml5-supported pace-done">
        <header class="header">
            <a class="logo" href="<?=Yii::app()->baseUrl;?>">MM Stat</a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a class="navbar-btn sidebar-toggle" role="button" data-toggle="offcanvas" href="#">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?=$User ? $User->getFullName() : null?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="images/no-avatar.jpg" class="img-circle" alt="User Image">
                                    <p>
                                        <?=$User ? $User->getFullName() : null?>
<!--                                        <small>Member since Nov. 2012</small>-->
                                    </p>
                                </li>
                                <!-- Menu Body -->
<!--                                <li class="user-body">-->
<!--                                    <div class="col-xs-4 text-center">-->
<!--                                        <a href="#">Followers</a>-->
<!--                                    </div>-->
<!--                                    <div class="col-xs-4 text-center">-->
<!--                                        <a href="#">Sales</a>-->
<!--                                    </div>-->
<!--                                    <div class="col-xs-4 text-center">-->
<!--                                        <a href="#">Friends</a>-->
<!--                                    </div>-->
<!--                                </li>-->
                                <!-- Menu Footer-->
                                <li class="user-footer">
<!--                                    <div class="pull-left">-->
<!--                                        <a href="#" class="btn btn-default btn-flat">Profile</a>-->
<!--                                    </div>-->
                                    <div class="pull-right">
                                        <a href="<?=Yii::app()->createUrl('/site/logout');?>" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <? if (!Yii::app()->user->isGuest) { ?>
            <aside class="left-side sidebar-offcanvas">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="images/no-avatar.jpg" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?=$User->getFullName()?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <? $userId = Yii::app()->user->getId(); ?>

                        <? if (AccessRules::hasAccess($userId, AccessRules::RULE_REPORT_PAGE, AccessRules::ACTION_VIEW)) : ?>
                        <li class=""><a href="<?=Yii::app()->createUrl('/cpanel/reports')?>"><i class="fa fa-table"></i> <span>Отчет</span></a></li>
                        <? endif; ?>

                        <? if (AccessRules::hasAccess($userId, AccessRules::RULE_REPORT_PAGE, AccessRules::ACTION_VIEW)) : ?>
                        <li class=""><a href="<?=Yii::app()->createUrl('/cpanel/reports/details')?>"><i class="fa fa-sitemap"></i> <span>Подробный отчет</span></a></li>
                        <? endif; ?>

                        <? if (AccessRules::hasAccess($userId, AccessRules::RULE_USERS_PAGE, AccessRules::ACTION_VIEW)) : ?>
                        <li class=""><a href="<?=Yii::app()->createUrl('/cpanel/user')?>"><i class="fa fa-users"></i> Пользователи</a></li>
                        <? endif; ?>

                        <? if (AccessRules::hasAccess($userId, AccessRules::RULE_AD_CAMPAIGN_PAGE, AccessRules::ACTION_VIEW)) : ?>
                        <li class=""><a href="<?=Yii::app()->createUrl('/cpanel/campaigns')?>"><i class="fa fa-shopping-cart"></i> <span>Рекламные кампании</span></a></li>
                        <? endif; ?>

<!--                            <li class=""><a href="--><?//=Yii::app()->createUrl('cpanel/solReport')?><!--"><i class="fa fa-table"></i> <span>SOL отчет</span></a></li>-->
                    </ul>
                </section>
            </aside>
            <? } ?>
            <aside class="right-side <? if (Yii::app()->user->isGuest) { echo 'strech'; } ?>">
                <?if (isset($this->clips['content-header'])) {
                    echo $this->clips['content-header'];
                } elseif (isset($this->pageTitle)) { ?>
                    <section class="content-header">
                        <h1><?=$this->pageTitle;?></h1>
                    </section>
                <? } ?>

                <section class="content">
                    <? if (method_exists($this, 'renderFlashes')) { ?>
                        <div class="col-sm-12">
                            <? $this->renderFlashes(); ?>
                        </div>
                    <? } ?>
                    <?=$content;?>
                </section>

                <section class="content-footer">
                </section>
            </aside>
        </div>
        <!--script type="text/javascript" src="/js/.min.js"></script>
        <script type="text/javascript" src="/js/.min.js"></script>
        <script type="text/javascript" src="/js/.min.js"></script>
        <script type="text/javascript" src="/js/.min.js"></script>
        <script type="text/javascript" src="/js/.min.js"></script>
        <script type="text/javascript" src="/js/.min.js"></script-->
        <script>
            $(function() {
                $('.btn-group[data-toggle="btn-toggle"]').each(function() {
                    var group = $(this);
                    $(this).find(".btn").click(function(e) {
                        group.find(".btn.active").removeClass("active");
                        $(this).addClass("active");
                        e.preventDefault();
                    });

                });
            });
        </script>
    </body>
</html>
