<?php
use Access\AccessGroupConfig;
use Access\AccessRules;

return [
    /**
     * Страница администрирования пользователей в админке
     * @author akiselev
     */
    AccessRules::RULE_USERS_PAGE => [
        // Просмотр страницы
        AccessRules::ACTION_VIEW => [
            AccessGroupConfig::KIRILL => AccessRules::LEVEL_ALL,
        ],

        // Создание юзера
        AccessRules::ACTION_CREATE => [
            AccessGroupConfig::KIRILL => AccessRules::LEVEL_ALL,
        ],

        // Редактирвоание юзера
        AccessRules::ACTION_EDIT => [
            AccessGroupConfig::KIRILL => AccessRules::LEVEL_ALL,
        ],
    ],

    /**
     * Страница рекламных кампаний
     * @author akiselev
     */
    AccessRules::RULE_AD_CAMPAIGN_PAGE => [
        // Просмотр страницы
        AccessRules::ACTION_VIEW => [
            AccessGroupConfig::KIRILL => AccessRules::LEVEL_ALL,
            AccessGroupConfig::MISHA  => AccessRules::LEVEL_ALL,
        ],

        // Просмотр страницы
        AccessRules::ACTION_VIEW_ITEM => [
            AccessGroupConfig::KIRILL => AccessRules::LEVEL_ALL,
            AccessGroupConfig::MISHA  => AccessRules::LEVEL_ALL,
        ],

        // Создание рк
        AccessRules::ACTION_CREATE => [
            AccessGroupConfig::KIRILL => AccessRules::LEVEL_ALL,
            AccessGroupConfig::MISHA  => AccessRules::LEVEL_ALL,
        ],

        // Редактирвоание рк
        AccessRules::ACTION_EDIT => [
            AccessGroupConfig::KIRILL => AccessRules::LEVEL_ALL,
            AccessGroupConfig::MISHA  => AccessRules::LEVEL_ONLY_SELF,
        ],
    ],

    /**
     * Страница рекламных кампаний - категории
     * @author akiselev
     */
    AccessRules::RULE_AD_CAMPAIGN_CATEGORY => [
        // Создание рк
        AccessRules::ACTION_CREATE => [
            AccessGroupConfig::KIRILL => AccessRules::LEVEL_ALL,
            AccessGroupConfig::MISHA  => AccessRules::LEVEL_ALL,
        ],
    ],

    /**
     * Страница отчета
     * @author akiselev
     */
    AccessRules::RULE_REPORT_PAGE => [
        // Просмотр страницы
        AccessRules::ACTION_VIEW => [
            AccessGroupConfig::MISHA    => AccessRules::LEVEL_ALL,
            AccessGroupConfig::KIRILL   => AccessRules::LEVEL_ALL,
            AccessGroupConfig::WATCHER  => AccessRules::LEVEL_ALL,
            AccessGroupConfig::INVESTOR => AccessRules::LEVEL_ALL,
        ],
    ],

    /**
     * Страница отчета
     * @author akiselev
     */
    AccessRules::RULE_ADVANCED_REPORT_PAGE => [
        // Просмотр страницы
        AccessRules::ACTION_VIEW => [
            AccessGroupConfig::MISHA    => AccessRules::LEVEL_ALL,
            AccessGroupConfig::KIRILL   => AccessRules::LEVEL_ALL,
            AccessGroupConfig::WATCHER  => AccessRules::LEVEL_ALL,
            AccessGroupConfig::INVESTOR => AccessRules::LEVEL_ALL,
        ],
    ],
];