<?php
return array(
    'default' => array(
        'host'     => '127.0.0.1',
        'port'     => 6379,
        'database' => 0,
    ),
    'dev' => array(
        'host'     => '127.0.0.1',
        'port'     => 6379,
        'database' => 1,
    ),

    'from_local' => array(
        'host'     => '95.213.129.108',
        'port'     => 6379,
        'database' => 0,
    ),
);