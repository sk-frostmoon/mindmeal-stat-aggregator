<?php
/**
 * Правила для статистики просмотров сайтов
 * @author akiselev
 */
return array(
    /**
     * Статистика с сайта solgame.ru
     */
    'solgame.ru' => array(
        'alias' => 'solgame',
        'pages' => array(
            '/' => array(
                'alias' =>'index',
            ),
            'news' => array(
                'alias' =>'news',
            ),
            'news/([0-9]+)' => array(
                'handler' => function($news_id) {
                    return array(
                        'page'    => 'concrete_news',
                        'news_id' => $news_id,
                    );
                }
            ),
            'user/register' => array(
                'alias' =>'register',
            ),
            'login' => array(
                'alias' =>'login',
            ),
            'media' => array(
                'alias' =>'media',
            ),
            'media/download' => array(
                'alias' =>'download_client',
            ),
            'static/landing/([0-9]+)' => array(
                'handler' => function($landing_id) {
                        return array(
                            'page'       => 'landing',
                            'landing_id' => $landing_id,
                        );
                    }
            ),
            'forumdisplay.php' => array(
                'alias' =>'forum',
            ),
        ),
    ),

    /**
     * Статистика с сайта mindmeal.ru
     */
    'mindmeal.ru' => array(
        'alias' => 'mindmeal',
        'pages' => array(
            '/' => array(
                'alias' => 'index',
            ),
            'ru' => array(
                'alias' => 'index',
            ),
            'en' => array(
                'alias' => 'index_en',
            ),
            'vacancies' => array(
                'alias' => 'index',
            ),
            'vacancies/more/([0-9]+)' => array(
                'handler' => function($vacancies_id) {
                    return array(
                        'page' => 'vacancies_more',
                        'vacancies_id' => $vacancies_id,
                    );
                }
            ),
            'news' => array(
                'alias' => 'news',
            ),
            'news/index' => array(
                'alias' => 'news',
            ),
            'en/news/index' => array(
                'alias' => 'news_en',
            ),
            'news/more/([0-9]+)' => array(
                'handler' => function($vacancies_id) {
                    return array(
                        'page' => 'news_more',
                        'news_id' => $vacancies_id,
                    );
                }
            ),
            'en/news/more/([0-9]+)' => array(
                'handler' => function($vacancies_id) {
                    return array(
                        'page' => 'news_more_en',
                        'news_id' => $vacancies_id,
                    );
                }
            ),
        ),
    ),

    /**
     * Статистика с сайта login.mindmeal.ru
     */
    'login.mindmeal.ru' => array(
        'alias' => 'mindmeal_login',
        'pages' => array(
            'user/login' => array(
                'alias' => 'login'
            ),
            'user/register' => array(
                'alias' => 'register'
            ),
            'user/profile' => array(
                'alias' => 'profile'
            ),
            'user/edit' => array(
                'alias' => 'settings'
            ),
        ),
    ),
);