<?php
return array(
    'default' => array(
        'host'     => 'localhost',
        'dbname'   => 'mm_stat',
        'user'     => 'mm_stat',
        'password' => 'ssx5i5uJ9IG7',
    ),

    'replication_core_char01' => array(
        'host'     => '95.213.129.107',
        'port'     => '3308',
        'dbname'   => 'core_char01',
        'user'     => 'webuser',
        'password' => 'Op3Eishaib',
    ),
    'replication_core_char02' => array(
        'host'     => '95.213.129.107',
        'port'     => '3308',
        'dbname'   => 'core_char02',
        'user'     => 'webuser',
        'password' => 'Op3Eishaib',
    ),
    'replication_core_char03' => array(
        'host'     => '95.213.129.107',
        'port'     => '3308',
        'dbname'   => 'core_char03',
        'user'     => 'webuser',
        'password' => 'Op3Eishaib',
    ),

    'core_char01' => array(
        'host'     => 'localhost',
        'dbname'   => 'core_char01',
        'user'     => 'mm_stat',
        'password' => 'ssx5i5uJ9IG7',
    ),
    'core_char02' => array(
        'host'     => 'localhost',
        'dbname'   => 'core_char02',
        'user'     => 'mm_stat',
        'password' => 'ssx5i5uJ9IG7',
    ),
    'core_char03' => array(
        'host'     => 'localhost',
        'dbname'   => 'core_char03',
        'user'     => 'mm_stat',
        'password' => 'ssx5i5uJ9IG7',
    ),
    'core_stats' => array(
        'host'     => 'localhost',
        'dbname'   => 'core_char03',
        'user'     => 'mm_stat',
        'password' => 'ssx5i5uJ9IG7',
    ),

    // LOCAL
    'core_char01_local' => array(
        'host'     => 'localhost',
        'dbname'   => 'core_char01',
        'user'     => 'root',
        'password' => 'root',
    ),
    'core_char02_local' => array(
        'host'     => 'localhost',
        'dbname'   => 'core_char02',
        'user'     => 'root',
        'password' => 'root',
    ),
    'core_char03_local' => array(
        'host'     => 'localhost',
        'dbname'   => 'core_char03',
        'user'     => 'root',
        'password' => 'root',
    ),
    'core_stats_local' => array(
        'host'     => 'localhost',
        'dbname'   => 'core_char03',
        'user'     => 'root',
        'password' => 'root',
    ),
);