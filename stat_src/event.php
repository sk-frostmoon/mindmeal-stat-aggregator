<?php
/**
 * Принимает событие и вызывает его обработчики события
 * @author akiselev
 */
require_once __DIR__ . '/bootstrap/bootstrap.php';

header('Content-Type: application/json');

$EventProcessor = new Event\EventRunner();

// Принимаем входящие данные от клиента
$request_body = file_get_contents('php://input');
$requestData  = json_decode($request_body, true);
$requestData  = array_merge((array) $requestData, (array) $_GET, (array) $_POST);

if (!array_key_exists('action', $requestData) || !array_key_exists('data', $requestData)) {
    http_response_code(405);
    echo json_encode(array(
        'status' => false,
        'error' => 'Не передано действие или данные',
    ));

    return;
}

try {
//    Profiler::start();

    // Вызываем событие
    if ($requestData['action'] == 'associate_login_id') {
        $path = PATH_LOGS . '/assoc.log';
        file_put_contents($path, $requestData['action'] . ': '. var_export($requestData['data'], true) . PHP_EOL, FILE_APPEND);
    }

    $result = $EventProcessor->fire($requestData['action'], $requestData['data']);

//    echo Profiler::stop();
} catch (Exception $Ex) {

    http_response_code($Ex->getCode());
    echo json_encode(array(
        'status' => false,
        'error' => $Ex->getMessage(),
    ));

    return;
}

if (!$result) {
    $result = array();
}

http_response_code(200);
echo json_encode(array(
    'status' => true,
    'response' => $result,
));