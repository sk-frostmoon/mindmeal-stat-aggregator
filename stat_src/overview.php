<?php
require_once __DIR__ . '/bootstrap/bootstrap.php';

$Maker = new OverviewJsMaker();

// Отдаем клиенту overview.js
header('Content-Type: application/javascript');
echo $Maker->make();
