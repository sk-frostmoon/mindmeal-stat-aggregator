<?php
namespace Ad;

use Ad\System\Storage;

class AdCampaign extends Storage {
    public $id = null;
    public $hashId = '';
    public $name = '';
    public $createTime = null;
    public $targetUrl = '';
    public $author = '';
    public $image = '';
    public $beginDate = null;
    public $endDate = null;
    public $utm_campaign = null;
    public $utm_source = null;
    public $utm_content = null;
    public $utm_medium = null;
    public $data_array = array();
    public $categoryId = null;

    /**
     * @return array
     */
    public static function getProperties() {
        return array(
            'id' => array(
                'title'      => 'ID кампании',
                'isEditable' => false,
                'isAddable'  => false,
                'type'       => 'int',
            ),
            'hashId' => array(
                'title'      => 'Hash ID кампании',
                'isEditable' => false,
                'isAddable'  => false,
                'type'       => 'string',
            ),
            'categoryId' => array(
                'title'      => 'ID Категории',
                'isEditable' => false,
                'isAddable'  => false,
                'type'       => 'int',
            ),
            'author' => array(
                'title'      => 'Автор',
                'isEditable' => false,
                'isAddable'  => false,
                'type'       => 'string',
            ),
            'createTime' => array(
                'title'      => 'Дата создания',
                'isEditable' => false,
                'isAddable'  => false,
                'type'       => 'datetime',
            ),
            'name' => array(
                'title'      => 'Название кампании',
                'isEditable' => true,
                'isAddable'  => true,
                'type'       => 'string',
            ),
            'targetUrl' => array(
                'title'      => 'Целевой url',
                'isEditable' => true,
                'isAddable'  => true,
                'type'       => 'string',
            ),
            'beginDate' => array(
                'title'       => 'Дата начала',
                'isEditable'  => true,
                'isAddable'   => true,
                'showOnTable' => false,
                'type'        => 'datetime',
            ),
            'endDate' => array(
                'title'       => 'Дата окончания',
                'isEditable'  => true,
                'isAddable'   => true,
                'showOnTable' => false,
                'type'        => 'datetime',
            ),
            'utm_campaign' => array(
                'title'      => 'utm_campaign',
                'isEditable' => true,
                'isAddable'  => true,
                'type'       => 'string',
            ),
            'utm_source' => array(
                'title'      => 'utm_source',
                'isEditable' => true,
                'isAddable'  => true,
                'type'       => 'string',
            ),
            'utm_content' => array(
                'title'      => 'utm_content',
                'isEditable' => true,
                'isAddable'  => true,
                'type'       => 'string',
            ),
            'utm_medium' => array(
                'title'      => 'utm_medium',
                'isEditable' => true,
                'isAddable'  => true,
                'type'       => 'string',
            ),
            'data_array' => array(
                'title'       => 'Данные',
                'isEditable'  => true,
                'isAddable'   => true,
                'showOnTable' => false,
                'type'        => 'array',
            ),
            'image' => array(
                'title'       => 'Баннер',
                'isEditable'  => true,
                'isAddable'   => true,
                'showOnTable' => false,
                'type'        => 'file',
            ),
        );
    }

    /**
     * Ноавая ли запись
     * @return bool
     */
    public function isNew() {
        return $this->id == 0;
    }
}