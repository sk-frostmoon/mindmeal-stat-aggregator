<?php
namespace Ad;

use Ad\Category\AdCategory;
use Storage\AbstractRedisStorage;

class AdStorage extends AbstractRedisStorage {
    /**
     * Поле с id кампании
     */
    const FIELD_CAMPAIGN_ID = 'ad:campaign:id';
    const HASH_ID_LENGTH    = 14;
    const HASH_ID_SALT      = 'WfjW2191fNEnapzcvIf@89f#hf30F#9jf#@fjf2t5!51';

    const INDEX_ID_AND_HASH_ID = 'index_id';
    const INDEX_AD_LIST        = 'ad_list';

    protected static $Instance = null;

    /**
     * Кэш объявлений
     * @var AdCampaign[]
     */
    protected $adCache = array();

    /**
     * @return static
     */
    public static function getInstance() {
        if (!static::$Instance) {
            static::$Instance = new AdStorage();
        }

        return static::$Instance;
    }

    protected function __construct() { }

    /**
     * @param string $hashId
     * @return AdCampaign|null
     */
    public function getByHashId($hashId) {
        if (isset($adCache[$hashId])) {
            return $adCache[$hashId];
        }

        $key  = $this->makeKey($hashId);
        $data = $this->execute(['GET', $key]);

        if (!$data) {
            return null;
        }
        $data = json_decode($data, true);
        $Campaign = new AdCampaign($data);

        $adCache[$hashId] = $Campaign;

        return $Campaign;
    }

    /**
     * @param int $id
     * @return AdCampaign|null
     */
    public function getById($id) {
        $key = $this->makeIndexKey(static::INDEX_ID_AND_HASH_ID, $id);
        $hashId = $this->execute(['GET', $key]);

        if (!$hashId) {
            return null;
        }

        return $this->getByHashId($hashId);
    }

    /**
     * @param AdCampaign $Campaign
     * @param AdCategory $Category
     * @return bool
     */
    public function save(AdCampaign $Campaign, AdCategory $Category) {
        $isNew = false;
        if (!$Campaign->id) {
            $isNew                = true;
            $Campaign->id         = $this->incrementId();
            $Campaign->hashId     = $this->makeIdHash($Campaign->id);
        }
        $Campaign->categoryId = $Category->id;

        $data = $Campaign->export();
        $jsonData = json_encode($data);

        // Сохраним саму кампанию
        $key = $this->makeKey($Campaign->hashId);
        $this->execute(['SET', $key, $jsonData]);

        if ($isNew) {
            // Сохраним в индекс id => hashId
            $key = $this->makeIndexKey(static::INDEX_ID_AND_HASH_ID, $Campaign->id);
            $this->execute(['SET', $key, $Campaign->hashId]);

            // Сохраним в индекс всех кампаний
            $key = $this->makeIndexKey(static::INDEX_AD_LIST, $Category->id);
            $this->execute(['RPUSH', $key, $Campaign->hashId]);
        }

        return true;
    }

    /**
     * @param AdCategory $Category
     * @return string[]|null
     */
    public function getAdHashIdList(AdCategory $Category) {
        $key   = $this->makeIndexKey(static::INDEX_AD_LIST, $Category->id);
        $total = $this->execute(['LLEN', $key]);
        return $this->execute(['LRANGE', $key, 0, $total]);
    }

    /**
     * Увеличивает id компании и возвращает его
     * @return int
     */
    protected function incrementId() {
        return $this->execute(['INCR', static::FIELD_CAMPAIGN_ID]);
    }

    /**
     * @return mixed
     */
    protected function totalAd() {
        return $this->execute(['GET', static::FIELD_CAMPAIGN_ID]);
    }

    /**
     * @param string $id
     * @return string
     */
    protected function makeKey($id) {
        return 'ad:campaign:data:' . $id;
    }

    /**
     * @param int|string $indexName
     * @param int|string $id
     * @return string
     */
    protected function makeIndexKey($indexName, $id) {
        return 'ad:campaign:' . $indexName . ':' . $id;
    }

    /**
     * @param int $id
     * @return string
     */
    protected function makeIdHash($id) {
        return substr(sha1($id . static::HASH_ID_SALT), 0, static::HASH_ID_LENGTH);
    }
} 