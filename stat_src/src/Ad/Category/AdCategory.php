<?php
namespace Ad\Category;

use Ad\System\Storage;

/**
 * Категория рекламной кампании
 * @author akiselev
 */
class AdCategory extends Storage {
    public $id       = null;
    public $parentId = null;
    public $title    = 'Root';

    /**
     * @return array
     */
    public static function getProperties() {
        return array(
            'id' => array(
                'title' => 'ID категории',
            ),
            'parentId' => array(
                'title' => 'ID родительской категории',
            ),
            'title' => array(
                'title' => 'Заголовок',
            ),
        );
    }
} 