<?php
namespace Ad\Category;

use Storage\AbstractRedisStorage;

class AdCategoryStorage extends AbstractRedisStorage {
    const FIELD_CATEGORY_ID   = 'ad:category:id';
    const FIELD_CATEGORY_LIST = 'ad:category:list';

    const INDEX_TREE = 'tree';

    protected static $Instance = null;

    /**
     * @return static
     */
    public static function getInstance() {
        if (!static::$Instance) {
            static::$Instance = new AdCategoryStorage();
        }

        return static::$Instance;
    }
    /**
     * Возвращает список ID категорий в указанной родительской
     * @param AdCategory $Category
     * @return int[]
     */
    public function getList(AdCategory $Category) {
        $key = $this->makeIndexKey(static::INDEX_TREE, $Category->id);
        $total = $this->totalElements();
        return $this->execute(['LRANGE', $key, 0, $total]);
    }

    /**
     * Возвращает категорию по ID
     * @param int $id
     * @return AdCategory|null
     */
    public function getById($id) {
        $key  = $this->makeKey($id);
        $data = $this->execute(['GET', $key]);

        if (!$data) {
            return null;
        }
        $data = json_decode($data, true);
        $Campaign = new AdCategory($data);

        return $Campaign;
    }

    /**
     * Возвращает цепочку родителей
     * @param AdCategory $Category
     * @return AdCategory[]
     */
    public function getParents(AdCategory $Category) {
        $Parents = array();

        while (true) {
            $Parent = $this->getById($Category->parentId);
            if ($Parent) {
                $Parents[] = $Parent;
                $Category = $Parent;
            } else {
                if ($Category->id) {
                    $Parents[] = new AdCategory();
                }
                break;
            }
        }

        return array_reverse($Parents);
    }

    /**
     * Возвращает массив категорий по идентификаторам.
     * Не гарантирует, что вернет то же количество категорий.
     * Если не находит категорию по id, то пропускает
     *
     * @param int[] $ids
     * @return AdCategory[]
     */
    public function getByIds(array $ids) {
        $Categories = array();
        foreach ($ids as $id) {
            $Category = $this->getById($id);

            if ($Category) {
                $Categories[] = $Category;
            }
        }

        return $Categories;
    }

    /**
     * Сохраняет категорию в хранилище, добавляя в требующиеся индексы
     *
     * @param AdCategory $Category
     * @return bool
     */
    public function save(AdCategory $Category) {
        $isNew = false;
        if (!$Category->id) {
            $isNew        = true;
            $Category->id = $this->incrementId();
        }

        $data = $Category->export();
        $jsonData = json_encode($data);

        // Сохраним саму категорию
        $key = $this->makeKey($Category->id);
        $this->execute(['SET', $key, $jsonData]);

        if ($isNew) {
            // Индекс дерева категорий
            $key = $this->makeIndexKey(static::INDEX_TREE, $Category->parentId);
            $this->execute(['RPUSH', $key, $Category->id]);

            // Сохраним в индекс всех категорий
            $this->execute(['RPUSH', static::FIELD_CATEGORY_LIST, $Category->id]);
        }

        return true;
    }

    /**
     * Увеличивает id компании и возвращает его
     * @return int
     */
    protected function incrementId() {
        return $this->execute(['INCR', static::FIELD_CATEGORY_ID]);
    }

    /**
     * Всего категорий в указанной родительской. NULL - корневая
     * @param int|null $parentId
     * @return int
     */
    protected function totalElements($parentId = null) {
        $key = $this->makeIndexKey(static::INDEX_TREE, $parentId);
        return $this->execute(['LLEN', $key]);
    }

    /**
     * @param string $id
     * @return string
     */
    protected function makeKey($id) {
        return 'ad:category:data:' . $id;
    }

    /**
     * @param int|string $indexName
     * @param int|string $id
     * @return string
     */
    protected function makeIndexKey($indexName, $id = null) {
        return 'ad:category:' . $indexName . ':' . $id;
    }
}