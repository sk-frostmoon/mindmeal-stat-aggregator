<?php
namespace Ad\Cityads;
use Utils\Time;

/**
 * Обработчик запросов от Cityads
 * @author akiselev
 */
class CityadsRequestHandler {
    /**
     * Запрос
     * @var array
     */
    protected $request = array();

    public function __construct() {
        $this->init();
    }

    /**
     * Возвращает запрос
     * @return array
     */
    public function getRequest() {
        return $this->request;
    }

    /**
     * Метод инициализации обработчика запросов
     * @return $this
     * @throws CityadsAuthorizationErrorException
     */
    protected function init() {
        $request    = $this->buildRequest();
        $AuthModule = new CityadsAuthorization($request);

        if (!$AuthModule->isAuth()) {
            throw new CityadsAuthorizationErrorException();
        }

        $this->request = $request;

        return $this;
    }

    /**
     * Парсит запрос и отдает массив
     * @param array $request
     * @throws CityadsBadRequestException
     * @return array
     */
    protected function parseRequest($request) {
        if (!isset($request['xml'])) {
            throw new CityadsBadRequestException("Not found request param: \"xml\"");
        }

        $xml = @simplexml_load_string($request['xml']);
        if (!$xml) {
            throw new CityadsBadRequestException("Is not valid request param \"xml\"");
        }

        $request['xml'] = array(
            'date_from' => (int) $xml->date_from,
            'date_to'   => (int) $xml->date_to,
        );

        return $request;
    }

    /**
     * Строит пришедший запрос
     * @throws CityadsBadRequestException
     * @return array
     */
    protected function buildRequest() {
        /*$_GET['xml'] = '<?xml version="1.0"?><request><date_from>' . (time() -  Time::TIME_HOUR * 10) . '</date_from><date_to>' . time() . '</date_to></request>';
        $_GET['pkey'] = 'Df85cA39051Dd81374F61ed8De';*/

        $request = array_merge($_GET, $_POST);
        $request = $this->parseRequest($request);

        return $request;
    }
} 