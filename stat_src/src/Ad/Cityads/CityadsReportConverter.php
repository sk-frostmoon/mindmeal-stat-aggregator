<?php
namespace Ad\Cityads;

use SimpleXMLElement;

/**
 * Конвертер отчета в xml
 * @author akiselev
 */
class CityadsReportConverter {
    /**
     * Данные отчета
     * @var array
     */
    protected $report = array();

    /**
     * @param array $report
     */
    public function __construct(array $report = array()) {
        $this->load($report);
    }

    /**
     * Загружет данные отчета
     * @param array $report
     */
    public function load(array $report) {
        $this->report = $report;
    }

    /**
     * Конвертирует отчет в XML
     * @return string
     */
    public function toXML() {
        $xml = new SimpleXMLElement('<items></items>');
        $counter = 0;
        foreach ($this->report as $user) {
            $xml->item[$counter]->click_id    = $user['utm'];
            $xml->item[$counter]->id          = $user['account_id'];
            $xml->item[$counter]->first_login = strtotime($user['first_game_login_time']);
            $xml->item[$counter]->level       = $user['level'];
            $xml->item[$counter]->date        = strtotime($user['lvl_time']);
            $counter++;
        }

        return $xml->asXML();
    }
} 