<?php
namespace Ad\Cityads;

/**
 * Модуль авторизации для рекламной конторы Cityads
 * @author akiselev
 */
class CityadsAuthorization {
    /**
     * Типы авторизации запроса
     */
    const AUTH_STATIC_PASSWORD  = 'static_password';
    const AUTH_DYNAMIC_PASSWORD = 'dynamic_password';

    /**
     * Статичкский ключ-пароль
     */
    const PRIVATE_AUTH_STATIC_KEY  = 'Df85cA39051Dd81374F61ed8De';

    /**
     * Динамический ключ-соль
     */
    const PRIVATE_AUTH_DYNAMIC_KEY = 'Amdm9sm214mIAFwqc5201Sfzx';

    /**
     * Текущий тип авторизации для запроса
     * @var string|null
     */
    protected $authType = null;

    /**
     * Дата начала выборки отчета
     * @var int|null
     */
    protected $dateFrom = null;

    /**
     * Дата окончания выборки отчета
     * @var int|null
     */
    protected $dateTo   = null;

    /**
     * Ключ авторизации
     * @var string|null
     */
    protected $authKey = null;

    /**
     * @var array
     */
    protected $request = array();

    /**
     * @param array $request
     */
    public function __construct(array $request) {
        $this->request = $request;
        $this->identifyAuthorizationType();
    }

    /**
     * Проверяет, авторизованый ли запрос
     * @return bool
     */
    public function isAuth() {
        switch ($this->authType) {
            case self::AUTH_STATIC_PASSWORD:
                return $this->authKey == self::PRIVATE_AUTH_STATIC_KEY;

            case self::AUTH_DYNAMIC_PASSWORD:
                return $this->checkDynamicPassword();
                break;
        }

        return false;
    }

    /**
     * Определяет тип авторизации для запроса
     * @throws CityadsBadRequestException
     */
    protected function identifyAuthorizationType() {
        if (isset($this->request['pkey'])) {
            $this->authType = self::AUTH_STATIC_PASSWORD;
            $this->authKey  = $this->request['pkey'];
        } elseif (isset($this->request['skey'])) {
            $this->authType = self::AUTH_DYNAMIC_PASSWORD;
            $this->authKey  = $this->request['skey'];
        } else {
            throw new CityadsBadRequestException("Not found request params: \"pkey\" and \"skey\"");
        }
    }

    /**
     * Проверяет корректность динамического ключа
     * @return bool
     * @throws CityadsBadRequestException
     */
    protected function checkDynamicPassword() {
        if (!isset($this->request['xml']['date_from']) || !isset($this->request['xml']['date_to'])) {
            throw new CityadsBadRequestException("Not found request params in xml: \"date_from\" or \"date_to\"");
        }

        return $this->authKey == $this->makeDynamicPasswordHash($this->request['xml']['date_from'], $this->request['xml']['date_to']);
    }

    /**
     * Строит хэш для сверки динамического пароля
     * @param int $dateFrom
     * @param int $dateTo
     * @return string
     */
    protected function makeDynamicPasswordHash($dateFrom, $dateTo) {
        return md5(static::AUTH_DYNAMIC_PASSWORD . $dateFrom . $dateTo);
    }
}