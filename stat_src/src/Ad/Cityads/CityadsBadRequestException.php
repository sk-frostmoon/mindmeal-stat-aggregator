<?php
namespace Ad\Cityads;

use Exception;

/**
 * Не верный запрос
 * @author akiselev
 */
class CityadsBadRequestException extends Exception {
    protected $code = 500;
    protected $message = 'Bad request';
}