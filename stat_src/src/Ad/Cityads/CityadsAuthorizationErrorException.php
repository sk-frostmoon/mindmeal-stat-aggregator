<?php
namespace Ad\Cityads;

use Exception;

/**
 * Ошибка авторизации
 * @author akiselev
 */
class CityadsAuthorizationErrorException extends Exception {
    protected $code    = 401;
    protected $message = 'Authorization error';
}