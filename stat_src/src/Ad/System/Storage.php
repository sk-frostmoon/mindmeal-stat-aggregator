<?php
namespace Ad\System;


class Storage {
    /**
     * @param array $data
     */
    public function __construct(array $data = array()) {
        $this->import($data);
    }

    /**
     * @return array
     */
    public static function getProperties() {
        return array();
    }

    /**
     * @param array $data
     */
    public function import(array $data) {
        foreach (static::getProperties() as $key => $config) {
            if (array_key_exists($key, $data)) {
                $this->$key = $data[$key];
            }
        }
    }

    /**
     * @return array
     */
    public function export() {
        $data = array();
        foreach (static::getProperties() as $key => $config) {
            $data[$key] = $this->$key;
        }

        return $data;
    }

    /**
     * @param string $field
     * @param mixed $default
     * @return mixed
     */
    public function get($field, $default = null) {
        if (isset($this->$field)) {
            return $this->$field;
        }

        return $default;
    }
} 