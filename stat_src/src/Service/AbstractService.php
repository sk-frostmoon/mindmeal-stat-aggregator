<?php
namespace Service;


abstract class AbstractService {
    /**
     * Запускает сервис
     */
    abstract public function run();

    /**
     * Проверяет наличие прав на выполнение сервиса
     */
    abstract public function hasAccess();
}