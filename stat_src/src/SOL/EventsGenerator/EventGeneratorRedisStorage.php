<?php
namespace SOL\EventsGenerator;

use Storage\AbstractRedisStorage;

class EventGeneratorRedisStorage extends AbstractRedisStorage {
    /**
     * Ключ времени последней генерации события
     */
    const LAST_GENERATE_EVENTS_TIME = 'sol:event_generator:last_execute';

    /**
     * Возвращает время последней генерации события
     * @return int
     */
    public function lastTime() {
        $Storage = $this->getConnection();
        $key     = $this->makeKey();

        return (int) $Storage->executeRaw(['GET', $key]);
    }

    /**
     * Выставляет время последней генерации события
     * @param int $time
     * @return bool
     */
    public function setTime($time) {
        $Storage = $this->getConnection();
        $key     = $this->makeKey();

        return (bool) $Storage->executeRaw(['SET', $key, (int) $time]);
    }

    /**
     * Ключ для записи в redis
     * @return string
     */
    protected function makeKey() {
        return self::LAST_GENERATE_EVENTS_TIME;
    }
} 