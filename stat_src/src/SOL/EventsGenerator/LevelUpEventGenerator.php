<?php
namespace SOL\EventsGenerator;

use PDO;
use SOL\Report\SolUserStorage;


class LevelUpEventGenerator {
    /**
     * Возвращает всех юзеров, у которых время последнего обновления > $lastGenerateTime
     * @param string $lastGenerateTime время в формате "Y-m-d H:i:s"
     * @return array
     */
    public function getUsers($lastGenerateTime) {
        $users = [];
        $users = array_merge(
            $this->getUsersFromDB($lastGenerateTime, 'replication_core_char01'),
            $this->getUsersFromDB($lastGenerateTime, 'replication_core_char02'),
            $this->getUsersFromDB($lastGenerateTime, 'replication_core_char03')
        );

        return $users;
    }

    protected function getUsersFromDB($lastGenerateTime, $db) {
        $StoragePersistent = new SolUserStorage();
        $PDO = $StoragePersistent->getPDO($db);

        $table = $StoragePersistent->getTableName();
        $sql   = "SELECT * FROM {$table} WHERE `char_enable` = 1 AND `char_updatestamp` > '{$lastGenerateTime}';";

        $Statement = $PDO->prepare($sql);
        $Statement->execute();

        return $Statement->fetchAll(PDO::FETCH_ASSOC);
    }
}