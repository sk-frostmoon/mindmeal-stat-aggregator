<?php
namespace SOL\Report;

use Statistics\StatisticsContext;
use Statistics\StatisticsFacade;
use Statistics\StatisticsKeys;
use User\SessionUserStorage;
use User\UserFlags;
use Utils\Time;

/**
 * Класс для построения отчета по SOL
 * 1. Делает выборку зарегистрированных юзеров за указанный период
 * 2. Выгружает из игровой базы данные по каждому юзеру
 * 3. Строит табличку юзеров
 *
 * @author akiselev
 */
class ReportBuilder {
    /**
     * Массив юзеров
     * @var array
     */
    protected $users = array();

    /**
     * Массив с готовым отчетом
     * @var array
     */
    protected $resultReport = array();

    /**
     * Строит "отчет"
     */
    public function build() {
        $ReportStorage = new ReportRedisStorage();

        $lastGenerateTime = $ReportStorage->getLastGeneratedTime();
        $currentTime      = time();
        $this->getRegisteredUsers(100000, $lastGenerateTime, $currentTime);
        $ReportStorage->setLastGeneratedTime($currentTime);

//        $this->getFromMysql('replication_core_char01');
//        $this->getFromMysql('replication_core_char02');
//        $this->getFromMysql('replication_core_char03');

        $this->makeReport();
    }

    public function getCharacters($timeFrom) {
        $chars = array_merge(
            $this->getFromMysql('replication_core_char01', $timeFrom),
            $this->getFromMysql('replication_core_char02', $timeFrom),
            $this->getFromMysql('replication_core_char03', $timeFrom)
        );

        return $chars;
    }

    /**
     * Вытаскивает зарегистрированных юзеров
     * @param int $limit
     * @param int $timeFrom
     * @param int $timeTo
     */
    protected function getRegisteredUsers($limit, $timeFrom, $timeTo = null) {
        $StorageElastic = new ElasticRegisteredUsersStorage();
        $StorageElastic->setInterval($timeFrom, $timeTo);
        $data = $StorageElastic->get(0, $limit);

        foreach ($data['hits']['hits'] as $hit) {
            if (!isset($hit['_source']['user']['login_id'])) {
                continue;
            }

            $this->users[$hit['_source']['user']['login_id']] = $hit['_source']['user'];
        }

        echo "Total " . sizeof($this->users) . " registered users" . PHP_EOL;
    }

    /**
     * @param string $config
     */
    protected function getFromMysql($config, $timeFrom) {
        $timeFrom = date("Y-m-d H:i:s", $timeFrom);

//        var_export($timeFrom);

        $StoragePersistent = new SolUserStorage();
        $PDO   = $StoragePersistent->getPDO($config);
        $table = $StoragePersistent->getTableName();
        $sql   = "SELECT * FROM {$table} WHERE `char_datestamp` > \"{$timeFrom}\"";

        $Statement = $PDO->prepare($sql);
        $Statement->execute();
        $resultTemp = $Statement->fetchAll();

        return $resultTemp;



//        $usersIds = array();
//        foreach ($this->users as $user) {
//            $usersIds[] = $user['login_id'];
//        }
//        $usersIds = implode(', ', $usersIds);
//
//
//
//        /**
//         * Склеиваем данные из elasticsearch и из игровой базы данных
//         */
//        foreach ($resultTemp as $row) {
//            $user = isset($this->resultReport[$row['user_index']]) ? $this->resultReport[$row['user_index']] : $this->users[$row['user_index']];
//
//            $char = [];
//            foreach ($StoragePersistent->getDataScheme() as $param => $type) {
//                $char[$param] = $row[$param];
//            }
//
//            $user['sol']['chars'][] = $char;
//            $this->resultReport[$row['user_index']] = $user;
//        }

    }

    protected function makeReport() {
        $IdentificationService = new IdentificationService();
        $Storage = SessionUserStorage::getInstance();
        $UserFlags = new UserFlags();

        $total_ = 0;
        foreach ($this->resultReport as $total => $user) {
            $firstLoginTime   = 0;
            $lastLoginTime    = 0;
            $totalPlayTime    = 0;
            $totalCharacter   = 0;
            $maxLvl           = 0;
            $totalMoney       = 0;
            $totalPayments    = 0;
            $firstPaymentDate = 0;
            $avgPayment       = 0;
            $avgBill          = 0;
            $totalGameEnter   = 0;

            $totalCharacter = sizeof($user['sol']['chars']);
            foreach ($user['sol']['chars'] as $char) {
                if ($firstLoginTime == 0) {
                    $firstLoginTime = strtotime($char['char_datestamp']);
                } elseif (strtotime($char['char_datestamp']) < $firstLoginTime) {
                    $firstLoginTime = strtotime($char['char_datestamp']);
                }

                if ($lastLoginTime == 0) {
                    $lastLoginTime = strtotime($char['char_updatestamp']);
                } elseif (strtotime($char['char_updatestamp']) > $lastLoginTime) {
                    $lastLoginTime = strtotime($char['char_updatestamp']);
                }

                if ($maxLvl == 0) {
                    $maxLvl = $char['char_level'];
                } elseif (strtotime($char['char_level']) > $maxLvl) {
                    $maxLvl = $char['char_level'];
                }

                $totalPlayTime += $char['char_timestamp'] > 0 ? $char['char_timestamp'] : 0;
                $totalMoney += $char['char_money'] > 0 ? $char['char_money'] : 0;
            }

            $row = [
                'account_id' => $user['login_id'],
                'utm' => isset($user['utm']) ? http_build_query($user['utm']) : '',
                'account_register' => isset($user['created']) ? date('Y-m-d H:i:s', strtotime($user['created']) - Time::TIME_HOUR * 2) : 0,
                'first_game_login_time' => date('Y-m-d H:i:s', $firstLoginTime),
                'last_game_login_time' => date('Y-m-d H:i:s', $lastLoginTime),
                'total_minutes_play_game' => $totalPlayTime,
                'total_character' => $totalCharacter,
                'max_level' => $maxLvl,
                'total_payments' => $totalPayments,
                'first_payment_date' => date('Y-m-d H:i:s', $firstPaymentDate),
                'avg_payment' => $avgPayment,
                'avg_bill' => $avgBill,
                'total_game_enter' => $totalGameEnter,
            ];


            // Генерируем события для статистики
            $loginId     = $row['account_id'];
            $loginIdHash = sha1($loginId . IdentificationService::SALT);
            $statId = $IdentificationService->getStatId($loginId, null, $loginIdHash);
            $User   = $Storage->getById($statId);

            if (!$User) {
                continue;
            }

            $User->sol_first_play_game = strtotime($row['first_game_login_time']);
            $User->sol_last_play_game  = strtotime($row['last_game_login_time']);
            $Storage->save($User);

            // Событие первого входа в игру
            if (!$UserFlags->isChecked(UserFlags::FLAG_SOL_FIRST_PLAY_GAME, $User->id)) {
                $total_ ++;

                StatisticsFacade::write(StatisticsKeys::KEY_SOL_FIRST_GAME_ENTER, array(
                    StatisticsContext::USER      => $User,
                    StatisticsContext::TIMESTAMP => $User->sol_first_play_game,
                ));

                $UserFlags->setFlag(UserFlags::FLAG_SOL_FIRST_PLAY_GAME, $User->id);
            }

            if ($total_ % 5 == 0) {
                echo "Write {$total_} events \n";
            }
        }

        echo "Write {$total_} events \n";
    }
}