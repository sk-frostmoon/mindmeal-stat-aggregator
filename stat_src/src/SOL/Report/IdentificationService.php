<?php
namespace SOL\Report;

use Predis\Client;
use User\IdentificationService as IdentificationServiceDefault;

/**
 * Сервис идентификации юзера
 * @author akiselev
 */
class IdentificationService extends IdentificationServiceDefault {
    public function getConnection($connectionName = 'dev') {
        return parent::getConnection($connectionName);
    }
}