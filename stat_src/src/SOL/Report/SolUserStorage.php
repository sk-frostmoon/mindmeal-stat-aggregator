<?php
namespace SOL\Report;


use PDO;
use Storage\AbstractPersistentStorage;

class SolUserStorage extends AbstractPersistentStorage {

    /**
     * Массив схемы таблицы
     * @return array
     */
    public function getDataScheme() {
        return [
            'index' => PDO::PARAM_INT,
            'user_index' => PDO::PARAM_INT,
            'char_slot' => PDO::PARAM_STR,
            'char_name' => PDO::PARAM_STR,
            'char_datestamp' => PDO::PARAM_STR,
            'char_updatestamp' => PDO::PARAM_STR,
            'char_timestamp' => PDO::PARAM_INT,
            'char_level' => PDO::PARAM_INT,
            'char_leveluptime' => PDO::PARAM_INT,
            'char_hp' => PDO::PARAM_INT,
            'char_mp' => PDO::PARAM_INT,
            'char_money' => PDO::PARAM_INT,
            'char_goldbox' => PDO::PARAM_INT,
            'char_exp' => PDO::PARAM_INT,
            'char_orgexp' => PDO::PARAM_INT,
            'char_energy' => PDO::PARAM_INT,
            'char_maxenergy' => PDO::PARAM_INT,
        ];
    }

    /**
     * Имя таблицы
     * @return string
     */
    public function getTableName() {
        return 'cr_character';
    }
}