<?php
namespace SOL\Report;

use Predis\Client;
use Storage\AbstractRedisStorage;
use Utils\Time;

class ReportRedisStorage extends AbstractRedisStorage {
    /**
     * Сохраняет отчет
     * @param string $reportName
     * @param array $reportData
     */
    public function saveReport($reportName, array $reportData) {
        $key        = $this->buildKey($reportName);
        $reportJson = json_encode($reportData);

        $this->execute(['SET', $key, $reportJson]);
        $this->execute(['RPUSH', 'reports:list', $reportName]);
    }

    /**
     * Возвращает отчет по имени
     * @param string $reportName
     * @return array
     */
    public function getReport($reportName) {
        $key  = $this->buildKey($reportName);
        $data = $this->execute(['GET', $key]);

        return json_decode($data, true);
    }

    /**
     * Время последнего события
     * @return int
     */
    public function getLastGeneratedTime() {
        $t = (int) $this->execute(['GET', 'report:last_generated_time']);
        return !$t ? time() - Time::TIME_DAY  : $t;
    }

    /**
     * Устанавливает время последней генерации события
     * @param int $time
     * @return bool
     */
    public function setLastGeneratedTime($time) {
        return (bool) $this->execute(['SET', 'report:last_generated_time', (int) $time]);
    }

    /**
     * Возвращает список отчетов
     * @return array
     */
    public function getReportsList() {
        $totalReports = $this->totalReports();
        return $this->execute(['LRANGE', 'reports:list', 0, $totalReports]);
    }

    /**
     * Возвращает кол-во отчетов
     * @return int
     */
    public function totalReports() {
        return (int) $this->execute(['LLEN', 'reports:list']);
    }

    /**
     * @param string|number $reportName
     * @return string
     */
    protected function buildKey($reportName) {
        return "report:data:{$reportName}";
    }

    /**
     * @param string $connectionName
     * @return Client
     */
    public function getConnection($connectionName = 'dev') {
        return parent::getConnection($connectionName);
    }
} 