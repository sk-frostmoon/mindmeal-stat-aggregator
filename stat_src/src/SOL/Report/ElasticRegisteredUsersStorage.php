<?php
namespace SOL\Report;
use DateTime;
use Storage\AbstractElasticSearchStorage;

/**
 * Класс для работы с хранилищем зарегистрированных юзеров
 * Данные берутся из индекса elastic search
 * @author akiselev
 */
class ElasticRegisteredUsersStorage extends AbstractElasticSearchStorage {
    protected $startTime = null;
    protected $endTime   = null;


    /**
     * @param int $startTime
     * @param int|null $endTime null - до текущего момента
     */
    public function setInterval($startTime, $endTime = null) {
        if (is_null($endTime)) {
            $endTime = time();
        }

        $this->startTime = $startTime;
        $this->endTime   = $endTime;
    }

    /**
     * @param $offset
     * @param $count
     * @return array
     */
    public function get($offset, $count) {
        $Client = $this->getClient();

        $searchParams = array();
        $searchParams['index']  = 'statistics';
        $searchParams['type']   = 'event';
        $searchParams['from']   = $offset;  // offset
        $searchParams['size']   = $count;  // count
        $searchParams['body']['query']  = [
            'filtered' => [
                'query' => [
                    'bool' => [
                        'should' => [
                            [
                                'query_string' => [
                                    'query' => '@key:user_register',
                                ],
                            ],
                        ],
                    ],
                ],

                'filter' => [
                    'bool' => [
                        'must' => [
                            [
                                'terms' => [
                                    '_type' => [
                                        'event'
                                    ],
                                ],
                            ],
                            [
                                'range' => [
                                    '@timestamp' => [
                                        'from' => date(DateTime::ISO8601, $this->startTime),
                                        'to'   => date(DateTime::ISO8601, $this->endTime),
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $data = $Client->search($searchParams);

        return $data;
    }
} 