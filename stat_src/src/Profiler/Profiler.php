<?php
namespace Profiler;

use XHProfRuns_Default;

/**
 * Класс для работы с профилировщиков xhprof
 * <code>
 * start() - для начала профилирования
 * stop() - для окончания профилирования. Возвращает ссылку на результаты профилирования.
 * </code>
 *
 * @author akiselev
 */
class Profiler {
    protected static $events = array();

    public static function start() {
        xhprof_enable(XHPROF_FLAGS_CPU | XHPROF_FLAGS_MEMORY);
    }

    public static function stop() {
        $xhprof_data = xhprof_disable();
        $namespace = $_SERVER['SERVER_NAME'];
        static::includeXHPfrof();

        $xhprof_runs = new XHProfRuns_Default();
        $run_id = $xhprof_runs->save_run($xhprof_data, $namespace);

        return "http://xhprof.local/index.php?run=$run_id&source=$namespace";
    }

    public static function  microtime_float() {
        return round(microtime(true) * 1000);
    }

    public static function includeXHPfrof() {
        $XHPROF_ROOT = '/usr/share/php';

        include_once $XHPROF_ROOT . "/xhprof_lib/utils/xhprof_lib.php";
        include_once $XHPROF_ROOT . "/xhprof_lib/utils/xhprof_runs.php";
    }
} 