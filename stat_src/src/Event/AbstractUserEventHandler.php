<?php
namespace Event;

use DateTime;
use Event\Handler\PageHit\PageHitRulesBuilder;
use Exception;
use User\IdentificationService;
use User\SessionUser;
use User\SessionUserStorage;
use User\UserFlags;

/**
 * Событие обработки юзерского действия
 * @author akiselev
 */
abstract class AbstractUserEventHandler extends AbstractEventHandler {
    /**
     * @var SessionUser
     */
    protected $User = null;

    /**
     * Добавляет данные к событию
     * @param mixed $data
     * @param null $server
     * @throws \Exception
     */
    public function setData($data, $server = null) {
        parent::setData($data);

        if (!is_array($this->data)) {
            $this->data = array();
        }

        if (!array_key_exists('userId', $this->data) || !$this->data['userId']) {
            throw new Exception('Не передан userId для обработки события', 401);
        }

        if (is_null($server)) {
            $server = $_SERVER;
        }

        $IdentificationService = new IdentificationService();
        $login_id   = isset($this->data['loginId']) ? $this->data['loginId'] : null;
        $login_hash = isset($this->data['loginHash']) ? $this->data['loginHash'] : null;
        $session_id = $IdentificationService->getStatId($login_id, $this->data['userId'], $login_hash);

        $Storage = SessionUserStorage::getInstance();
        $User    = $Storage->getById($session_id);
/*
        if ($session_id != $IdentificationService->getCurrentSessionId()) {
            // Юзер авторизован на той машине, на которой он не регистрировался,
            // но имеет анонимный (без login_id) контейнер с данными

            $OldUser = $Storage->getById($IdentificationService->getCurrentSessionId());

            $oldUserData = $OldUser->export();
            $userData    = $User->export();

            if ($OldUser->created < $User->created) {
                $userData = array_merge($userData, $oldUserData);
                $User->import($userData);

                // todo удалить контейнер моложе
            } else {
                $oldUserData = array_merge($oldUserData, $userData);
                $OldUser->import($oldUserData);

                // todo удалить контейнер моложе
            }
        }
*/
        $browser = $this->getBrowser($server);
        $ip      = $this->getUserIp($server);
        $geo     = $this->getGeoData($server);
        $ref     = parse_url($this->data['ref']);
        $url     = parse_url($this->data['url']);

        if (!isset($ref['host'])) {
            $ref['host'] = '';
        }

        // Page info
        $RulesBuilder = new PageHitRulesBuilder();
        $url = array_merge([
            'host' => '',
            'path' => '',
            'query' => '',
        ], $url);
        $contextSiteData = $RulesBuilder->getContextData($url['host'], $url['path'], $url['query']);
        $this->data['params'] = array_merge($this->data['params'], $contextSiteData);

        // UTM if from search
        $this->setUtmIfFromSearch($ref);

        if (!$User) {
            $this->User = new SessionUser();
            $this->User->import(array(
                'id'           => $this->data['userId'],
                'login_id'     => $login_id,
                'ref'          => $this->data['ref'],
                'ref_domain'   => $ref['host'],
                'url'          => $this->data['url'],
                'utm'          => $this->data['utm'],
                'data'         => $this->data['params'],
                'ip'           => $ip,
                'browser'      => $browser['name'],
                'browser_version' => $browser['version'],
                'os'           => $browser['platform'],
                'geo'          => $geo,
                'last_request' => time(),
                'created'      => time(),
                'screen'       => array(
                    'width'  => $this->data['screen']['width'],
                    'height' => $this->data['screen']['height'],
                    'size'   => $this->data['screen']['width'] . 'x' . $this->data['screen']['height'],
                ),
            ));
        } else {
            $this->User = $User;

            // Считаем историю изменений UTM
            $this->User = $this->setUtmHistory($this->User);

            // Тут импортируем не все данные, только те, что нужно обновить
            $this->User->import(array(
                'ref'          => $this->data['ref'],
                'login_id'     => $login_id ? $login_id : $this->User->login_id,
                'ref_domain'   => $ref['host'],
                'url'          => $this->data['url'],
                'utm'          => $this->data['utm'],
                'data'         => $this->data['params'],
                'ip'           => $ip,
                'browser'      => $browser['name'],
                'browser_version' => $browser['version'],
                'os'           => $browser['platform'],
                'geo'          => $geo,
                'last_request' => time(),
                'created'      => $this->User->created ? $this->User->created : time(),
                'screen'       => array(
                    'width'  => $this->data['screen']['width'],
                    'height' => $this->data['screen']['height'],
                    'size'   => $this->data['screen']['width'] . 'x' . $this->data['screen']['height'],
                ),
            ));
        }

        $Storage->save($this->User);
    }

    /**
     * Отдает гео данные, если это возможно
     * @param $server
     * @return array
     */
    protected function getGeoData($server) {
        $countryCode = array_key_exists('GEOIP_COUNTRY_CODE', $server) ? $server['GEOIP_COUNTRY_CODE'] : '';
        $countryName = array_key_exists('GEOIP_COUNTRY_NAME', $server) ? $server['GEOIP_COUNTRY_NAME'] : '';
        $cityName    = array_key_exists('GEOIP_CITY_NAME', $server)    ? $server['GEOIP_CITY_NAME']    : '';

        $geo = array(
            'county_code' => $countryCode,
            'county_name' => $countryName,
            'city_name'   => $cityName
        );

        return $geo;
    }

    /**
     * Отдает ip адрес юзера
     * @param $server
     * @return string
     */
    protected function getUserIp($server) {
        if (array_key_exists('REMOTE_ADDR', $server)) {
            return $server['REMOTE_ADDR'];
        }

        return '';
    }

    /**
     * @return SessionUser
     */
    public function getUser() {
        return SessionUserStorage::getInstance()->getById($this->data['userId']);
    }

    /**
     * @return UserFlags
     */
    protected function getFLags() {
        return $this->getUser()->getFlags();
    }

    /**
     * @param SessionUser $User
     * @return SessionUser
     */
    protected function setUtmHistory(SessionUser $User) {
        // Запись истории изменений UTM source
        $utm_source_new = isset($this->data['utm']) && isset($this->data['utm']['utm_source']) ? $this->data['utm']['utm_source'] : null;
        $utm_source_old = isset($User->utm['utm_source']) ? $User->utm['utm_source'] : null;

        if (!is_null($utm_source_new) && $utm_source_old != $utm_source_new) {
            $User->utm_history[] = array(
                'date_change' => date(DateTime::ISO8601),
                'utm_source'  => $utm_source_new,
            );
        }

        return $User;
    }

    /**
     * @param array $ref
     */
    protected function setUtmIfFromSearch($ref) {
        if (!array_key_exists('utm', $this->data) || !$this->data['utm']) {
            preg_match("#.*yandex.*#", $ref['host'], $matches);
            if (count($matches) > 0) {
                $this->data['utm'] = array(
                    'utm_source' => 'yandex',
                );
            }

            preg_match("#.*google.*#", $ref['host'], $matches);
            if (count($matches) > 0) {
                $this->data['utm'] = array(
                    'utm_source' => 'google',
                );
            }
        }
    }

    /**
     * Определяем браузер
     * @param $server
     * @return array
     */
    protected function getBrowser($server) {
        if (!isset($server['HTTP_USER_AGENT'])) {
            return array(
                'userAgent' => '',
                'name'      => '',
                'version'   => '',
                'platform'  => '',
                'pattern'   => ''
            );
        }

        $u_agent  = $server['HTTP_USER_AGENT'];
        $bname    = 'Unknown';
        $platform = 'Unknown';
        $version  = "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif(preg_match('/Firefox/i',$u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif(preg_match('/Chrome/i',$u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif(preg_match('/Safari/i',$u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif(preg_match('/Opera/i',$u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif(preg_match('/Netscape/i',$u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        $ub = isset($ub) ? $ub : '';

        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        $matches['version'][0] = isset($matches['version'][0]) ? $matches['version'][0] : '';
        $matches['version'][1] = isset($matches['version'][1]) ? $matches['version'][1] : '';

        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            } else {
                $version= $matches['version'][1];
            }
        } else {
            $version= $matches['version'][0];
        }

        // check if we have a number
        if ($version==null || $version=="") {$version="?";}

        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern
        );
    }
} 