<?php
namespace Event;

use Event\Handler\DownloadClientEventHandler;
use Event\Handler\Ingame\GameBeginEventHandler;
use Event\Handler\Ingame\IngamePingEventHandler;
use Event\Handler\Ingame\UserMathmakingEventHandler;
use Event\Handler\Ingame\UsersLooseEventHandler;
use Event\Handler\Ingame\UsersWinsEventHandler;
use Event\Handler\LoginEventHandler;
use Event\Handler\PageHit\PageHitEvent;
use Event\Handler\PingEventHandler;
use Event\Handler\RegisterEventHandler;
use Event\Handler\Serverside\AssociateLoginIdEventHandler;
use Event\Handler\Serverside\ServersideStatisticsEventHandler;
use Event\Handler\StatisticsEventHandler;
use Event\Handler\UniqueStatisticsEventHandler;
use Exception;

/**
 * ОЗапускает обработку событий
 * @author akiselev
 */
class EventRunner {
    /**
     * Возвращает все события
     * @return array
     */
    protected function getEvents() {
        return array(
            /**
             * ClientSide
             */
            'view_page'               => PageHitEvent::getClass(),
            'write_statistics'        => StatisticsEventHandler::getClass(),
            'write_unique_statistics' => UniqueStatisticsEventHandler::getClass(),
            'user_register'           => RegisterEventHandler::getClass(),
            'user_login'              => LoginEventHandler::getClass(),
            'ping'                    => PingEventHandler::getClass(),

            /**
             * InGame
             */
            'ingame_ping'      => IngamePingEventHandler::getClass(),
            'user_mathcmaking' => UserMathmakingEventHandler::getClass(),
            'game_begin'       => GameBeginEventHandler::getClass(),
            'users_wins'       => UsersWinsEventHandler::getClass(),
            'users_loose'      => UsersLooseEventHandler::getClass(),
            'download_client'  => DownloadClientEventHandler::getClass(),

            /**
             * ServerSide API
             */
            'associate_login_id'                   => AssociateLoginIdEventHandler::getClass(),
            'Event.Handler.Serverside.Write'       => ServersideStatisticsEventHandler::getClass(),
            'Event.Handler.Serverside.WriteUnique' => ServersideStatisticsEventHandler::getClass(),
        );
    }

    /**
     * Вызывает событие
     * @param mixed $event событие
     * @param mixed $data данные передаваемые событию
     * @throws Exception
     */
    public function fire($event, $data) {
        $events = $this->getEvents();

        if (!array_key_exists($event, $events)) {
            throw new Exception("Event \"{$event}\" not found", 400);
        }

        /** @var AbstractEventHandler $Event */
        $Event = new $events[$event]();
        $Event->setData($data);

        return $Event->run();
    }
}