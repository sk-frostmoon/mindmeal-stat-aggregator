<?php
namespace Event;

/**
 * Абстрактный класс конкретного обработчика события
 * @author akiselev
 */
abstract class AbstractEventHandler {
    /**
     * Данные переданные обработчику
     * @var mixed
     */
    protected $data = null;

    /**
     * Запускает обработку события
     */
    abstract public function run();

    /**
     * Добавляет данные в контекст события
     * @param mixed $data
     */
    public function setData($data) {
        $this->data = $data;
    }

    /**
     * Возвращает данные переданные в контекст события
     * @return mixed
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Возвращает класс события
     * @return string
     */
    public static function getClass() {
        return get_called_class();
    }
} 