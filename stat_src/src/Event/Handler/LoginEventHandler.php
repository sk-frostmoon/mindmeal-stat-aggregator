<?php
namespace Event\Handler;

use Event\AbstractUserEventHandler;
use Statistics\StatisticsContext;
use Statistics\StatisticsFacade;
use Statistics\StatisticsKeys;
use User\SessionUserStorage;

/**
 * Событие логина юзера
 * @author akiselev
 */
class LoginEventHandler extends AbstractUserEventHandler {
    /**
     * Запускает обработку события
     */
    public function run() {
        $Storage = SessionUserStorage::getInstance();
        $User = $this->getUser();
        if (!$User->first_login_time) {
            $User->first_login_time = time();
        }
        $User->last_login_time = time();
        $Storage->save($User);


        StatisticsFacade::write(StatisticsKeys::KEY_USER_LOGIN, array(
            StatisticsContext::USER        => $User,
            StatisticsContext::USER_FLAGS  => $User->getFlags(),
        ));

        StatisticsFacade::writeUnique(StatisticsKeys::KEY_USER_LOGIN_UNIQUE, array(
            StatisticsContext::USER        => $User,
            StatisticsContext::USER_FLAGS  => $User->getFlags(),
        ));
    }
}