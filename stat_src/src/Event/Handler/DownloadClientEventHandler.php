<?php
/**
 * Created by PhpStorm.
 * User: yakud
 * Date: 9/26/14
 * Time: 8:52 PM
 */

namespace Event\Handler;


use Event\AbstractEventHandler;
use Statistics\StatisticsFacade;
use Statistics\StatisticsKeys;

class DownloadClientEventHandler extends AbstractEventHandler {

    /**
     * Запускает обработку события
     */
    public function run() {
        StatisticsFacade::write('download_fd_client');
    }
}