<?php
namespace Event\Handler;

use Event\AbstractUserEventHandler;
use Exception;
use Statistics\StatisticsContext;
use Statistics\StatisticsFacade;

/**
 * Обработчик события "написать статистику"
 * @author akiselev
 */
class StatisticsEventHandler extends AbstractUserEventHandler {

    /**
     * Запускает обработку события
     */
    public function run() {
        if (!array_key_exists('key', $this->data['params'])) {
            throw new Exception('Ключ статистики не передан');
        }

        // todo Убрать из data ключ, что бы не писался
        StatisticsFacade::write($this->data['params']['key'], array(
            StatisticsContext::USER        => $this->getUser(),
            StatisticsContext::USER_FLAGS  => $this->getUser()->getFlags(),
        ));
    }
}