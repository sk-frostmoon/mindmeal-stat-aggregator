<?php
namespace Event\Handler\PageHit;

use Event\AbstractUserEventHandler;
use Statistics\StatisticsContext;
use Statistics\StatisticsFacade;
use Statistics\StatisticsKeys;

/**
 * Событие просмотра страницы пользователем.
 * Парсит url откуда пришел запрос и вызывает нужный обработчик, если для него есть правило.
 * Иначе, делает все сам.
 *
 * @author akiselev
 */
class PageHitEvent extends AbstractUserEventHandler {

    /**
     * Запускает обработку события
     */
    public function run() {
        $User         = $this->getUser();
        $RulesBuilder = new PageHitRulesBuilder();

        $host   = $RulesBuilder->normalizeHost($User->getDataByKey('host'));
        $path   = $User->getDataByKey('path');
        $path   = $RulesBuilder->normalizePath($path);
        $search = $User->getDataByKey('search');

        $contextData = $RulesBuilder->getContextData($host, $path, $search);

        // Сформированные данные кладем в юзера
        $User->data = array_merge($User->data, $contextData);

        $context = array(
            StatisticsContext::USER       => $User,
            StatisticsContext::USER_FLAGS => $User->getFlags(),
        );

        StatisticsFacade::write(StatisticsKeys::KEY_PAGE_HIT, $context);
        StatisticsFacade::writeUnique(StatisticsKeys::KEY_PAGE_HIT_USER_UNIQUE, $context + array(
            StatisticsContext::FLAG_NAME => sha1($host.$path),
        ));
        StatisticsFacade::writeUnique(StatisticsKeys::KEY_PAGE_HIT_DAILY_UNIQUE, $context + array(
            StatisticsContext::FLAG_NAME => sha1($host.$path.date('md', time())),
        ));
        StatisticsFacade::writeUnique(StatisticsKeys::KEY_SITE_HIT_USER_UNIQUE, $context + array(
            StatisticsContext::FLAG_NAME => sha1($host),
        ));
        StatisticsFacade::writeUnique(StatisticsKeys::KEY_SITE_HIT_DAILY_UNIQUE, $context + array(
            StatisticsContext::FLAG_NAME => sha1($host.date('md', time())),
        ));
    }
}