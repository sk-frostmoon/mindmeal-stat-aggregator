<?php
namespace Event\Handler\PageHit;

use Closure;
use Utils\Config;

/**
 * Обработчик для событий просмотра страниц.
 * Строит данные по правилам для статистики по просмотрам страниц.
 *
 * @author akiselev
 */
class PageHitRulesBuilder {
    /**
     * Возвращает контекст для статистики
     * @param string $host
     * @param string $path
     * @param string $search
     * @return array
     */
    public function getContextData($host, $path, $search) {
        // Распарсим GET запрос страницы
        $searchArray = array();

        if (is_array($search)) {
            $search = implode('', $search);
        }

        $search = str_replace('?', '', $search);
        $search = array_filter(explode('&', $search));
        foreach ($search as $token) {
            $token = explode('=', $token);
            if (isset($token[1])) {
                $searchArray[$token[0]] = $token[1];
            } else {
                $searchArray[] = $token[0];
            }
        }
        $search = $searchArray;

        // Начнем формировать контекст
        $contextData = array();

        // Контекст для сайта
        $site = $this->getSiteForPickStatistics($host);
        $contextData['site'] = $this->getSiteAlias($host);

        // Контекст для страницы
        if (!array_key_exists('pages', $site)) {
            // Нет правил для страниц, берем по умолчанию
            $contextData['page'] = $this->makeDefaultAlias($path);
        } else {
            $isFindRule = false;

            // Бежим по правилам и смотрим, какое подходит
            foreach ($site['pages'] as $rule => $ruleData) {
                $pageData = $this->makePageDataByRule($path, $rule, $ruleData);
                if (is_null($pageData)) {
                    // Правило не подходит
                    continue;
                }
                $isFindRule = true;

                if (array_key_exists('page', $pageData)) {
                    // В конфиге правила нет названия (алиаса) странички, генерим сами
                    $data['page'] = $this->makeDefaultAlias($path);
                }

                $contextData = array_merge($contextData, $pageData);
                break;
            }

            if (!$isFindRule) {
                // В итоге не нашли правило, генерим сами алиас
                $contextData['page'] = $this->makeDefaultAlias($path);
            }
        }

        return $contextData;
    }

    /**
     * @param string $host
     * @return array
     */
    protected function getSiteForPickStatistics($host) {
        $rules = Config::get('page_hit_rules');

        if (array_key_exists($host, $rules)) {
            return $rules[$host];
        }

        return array(
            'alias' => $this->makeDefaultAlias($host),
        );
    }

    /**
     * Формирует данные страницы по правилам или генерирует на основе url
     * @param string $url
     * @param array $rule
     * @param array $ruleParams
     * @return array
     */
    protected function makePageDataByRule($url, $rule, $ruleParams) {
        $rule = stripslashes($rule);
        preg_match("#^{$rule}$#", $url, $matches);

        $isMatches = count($matches) > 0;
        if (!$isMatches) {
            return null;
        }

        if (array_key_exists('alias', $ruleParams)) {
            return array(
                'page' => $ruleParams['alias']
            );
        }

        if (array_key_exists('handler', $ruleParams)) {
            if ($ruleParams['handler'] instanceof Closure) {
                if (count($matches) == 1) {
                    return call_user_func($ruleParams['handler']);
                }

                unset($matches[0]);
                $matches = array_values($matches);
                return call_user_func_array($ruleParams['handler'], $matches);
            }
        }

        return array(
            'page' => $this->makeDefaultAlias($url),
        );
    }

    /**
     * Нормализует путь
     *
     * '/super/puper/path'  => 'super/puper/path'
     * '/super/puper/path/' => 'super/puper/path'
     * ''                   => '/'
     *
     * @param string $path
     * @return string
     */
    public function normalizePath($path) {
        $path = explode('/', $path); // Разбираем путь
        $path = array_filter($path); // Убираем из массива пустые значения
        $path = implode('/', $path); // Собираем обратно

        if (empty($path)) {
            $path = '/';
        }

        return $path;
    }

    /**
     * @param string $host
     * @return mixed
     */
    public function normalizeHost($host) {
        $host = str_replace('www.', '', $host);

        return $host;
    }

    /**
     * Отдает алиас для сайта
     * @param string $host
     * @return string
     */
    protected function getSiteAlias($host) {
        $site = $this->getSiteForPickStatistics($host);
        if (is_null($site) || !array_key_exists('alias', $site)) {
            return $this->makeDefaultAlias($site);
        }

        return $site['alias'];
    }

    /**
     * Формирует алиас для строки
     *
     * cool.site.ru     => cool_site_ru
     * cool.big-site.ru => cool_big_site_ru
     * site.ru/this/url => site_ru_this_url
     * this/is/any/url  => this_is_any_url
     *
     * @param string $host
     * @return string
     */
    public function makeDefaultAlias($host) {
        $host = implode('_', array_filter(explode('.', $host)));
        $host = implode('_', array_filter(explode('-', $host)));
        $host = implode('_', array_filter(explode('/', $host)));

        return $host;
    }
} 