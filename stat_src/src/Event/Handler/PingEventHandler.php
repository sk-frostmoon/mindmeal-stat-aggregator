<?php
namespace Event\Handler;

use Event\AbstractEventHandler;

class PingEventHandler extends AbstractEventHandler {

    /**
     * Запускает обработку события
     */
    public function run() {
        $data = $this->getData();

        return array(
            'response' => 'pong',
            'request'  => $data,
        );
    }
}