<?php
namespace Event\Handler\Ingame\Exception;

use Exception;

/**
 * Юзер не авторизован
 * @author akiselev
 */
class UserNotAuthException extends Exception {
    protected $message = "Unauthorized user";
    protected $code = 401;
} 