<?php
namespace Event\Handler\Ingame\Exception;

use Exception;

/**
 * Юзер не авторизован
 * @author akiselev
 */
class UserNotFoundException extends Exception {
    protected $message = "User not found";
    protected $code = 600;
} 