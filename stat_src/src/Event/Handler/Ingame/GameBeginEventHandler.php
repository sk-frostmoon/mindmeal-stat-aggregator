<?php
namespace Event\Handler\Ingame;

use Event\AbstractEventHandler;
use User\SessionUser;

class GameBeginEventHandler extends AbstractEventHandler {

    /**
     * Запускает обработку события
     */
    public function run() {
        return array(
            'event' => 'game begin'
        );
    }
}