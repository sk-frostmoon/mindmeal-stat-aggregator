<?php
namespace Event\Handler\Ingame;

use Event\AbstractEventHandler;
use Event\Handler\Ingame\Exception\UserNotAuthException;
use Event\Handler\Ingame\Exception\UserNotFoundException;
use Exception;
use User\IdentificationService;
use User\SessionUser;
use User\SessionUserStorage;

/**
 * Событие "in game" статистики в частности FD
 * @author akiselev
 */
abstract class AbstractIngameEventHandler extends AbstractEventHandler {
    /**
     * @var SessionUser
     */
    protected $UserContainer = null;

    /**
     * Добавляет данные в контекст события
     * @param mixed $data
     * @throws Exception
     */
    public function setData($data) {
        parent::setData($data);

        if (!array_key_exists('loginId', $this->data) || !$this->data['loginId']) {
            throw new UserNotAuthException;
        }

        if (!array_key_exists('loginIdHash', $this->data) || !$this->data['loginIdHash']) {
            throw new UserNotAuthException;
        }

        $IdentificationService = new IdentificationService();
        if (!$IdentificationService->isAuth($this->data['loginId'], $this->data['loginIdHash'])) {
            throw new UserNotAuthException;
        }

        $session_id = $IdentificationService->getStatId($this->data['loginId'], null, $this->data['loginIdHash']);
        $Storage = SessionUserStorage::getInstance();
        $User    = $Storage->getById($session_id);

        if (!$User) {
//            throw new UserNotFoundException();
        }

        $this->UserContainer = $User;
    }
}