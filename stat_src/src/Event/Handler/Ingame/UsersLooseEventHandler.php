<?php
namespace Event\Handler\Ingame;

use Event\AbstractEventHandler;
use User\SessionUser;

class UsersLooseEventHandler extends AbstractEventHandler {

    /**
     * Запускает обработку события
     */
    public function run() {
        return array(
            'event' => 'Users loose =('
        );
    }
}