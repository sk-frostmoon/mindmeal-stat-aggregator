<?php
namespace Event\Handler\Ingame;

use Event\AbstractEventHandler;
use User\SessionUser;

class UsersWinsEventHandler extends AbstractEventHandler {

    /**
     * Запускает обработку события
     */
    public function run() {
        return array(
            'event' => 'Users wins!'
        );
    }
}