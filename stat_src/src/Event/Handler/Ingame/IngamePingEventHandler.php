<?php
namespace Event\Handler\Ingame;

use User\SessionUser;

class IngamePingEventHandler extends AbstractIngameEventHandler {

    /**
     * Запускает обработку события
     */
    public function run() {
        $data = $this->getData();
        $User = $this->UserContainer;

        if (!$User) {
            $User = new SessionUser();
        }

        return array(
            'hello' => 'world',
            'world' => 'hello'
        );
    }
}