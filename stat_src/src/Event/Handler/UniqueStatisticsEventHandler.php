<?php
namespace Event\Handler;

use Event\AbstractUserEventHandler;
use Exception;
use Statistics\StatisticsContext;
use Statistics\StatisticsFacade;

/**
 * Обработчик события "написать уникальную статистику"
 * @author akiselev
 */
class  UniqueStatisticsEventHandler extends AbstractUserEventHandler {

    /**
     * Запускает обработку события
     */
    public function run() {
        // Уникальная статистика
        if (array_key_exists('key_unique', $this->data['params'])) {
            if (array_key_exists('site', $this->data['params'])) {
                $site = $this->data['params']['site'];
            } else {
                $site = '';
            }

            if (array_key_exists('page', $this->data['params'])) {
                $page = $this->data['params']['page'];
            } else {
                $page = '';
            }

            // todo remove
            if ($site == 'solgame' && $page == 'index') {
                return;
            }

            // todo Убрать из data ключ, что бы не писался

            StatisticsFacade::writeUnique($this->data['params']['key_unique'], array(
                StatisticsContext::USER        => $this->getUser(),
                StatisticsContext::USER_FLAGS  => $this->getUser()->getFlags(),
                StatisticsContext::FLAG_NAME   => sha1($site.$page.date('md', time())),
            ));
        }

        // Не уникальная статистика
        if (array_key_exists('key', $this->data['params'])) {
            // todo Убрать из data ключ, что бы не писался

            StatisticsFacade::write($this->data['params']['key'], array(
                StatisticsContext::USER        => $this->getUser(),
                StatisticsContext::USER_FLAGS  => $this->getUser()->getFlags(),
            ));
        }
    }
}