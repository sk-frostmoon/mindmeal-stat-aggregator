<?php
namespace Event\Handler;

use Event\AbstractUserEventHandler;
use Statistics\StatisticsContext;
use Statistics\StatisticsFacade;
use Statistics\StatisticsKeys;

/**
 * Событие регистрации юзера
 * @author akiselev
 */
class RegisterEventHandler extends AbstractUserEventHandler {
    /**
     * Запускает обработку события
     */
    public function run() {
        StatisticsFacade::write(StatisticsKeys::KEY_USER_REGISTER, array(
            StatisticsContext::USER        => $this->getUser(),
            StatisticsContext::USER_FLAGS  => $this->getUser()->getFlags(),
        ));

        StatisticsFacade::writeUnique(StatisticsKeys::KEY_USER_REGISTER_UNIQUE, array(
            StatisticsContext::USER        => $this->getUser(),
            StatisticsContext::USER_FLAGS  => $this->getUser()->getFlags(),
            StatisticsContext::FLAG_NAME   => $this->getUser()->id,
        ));
    }
}