<?php
namespace Event\Handler\Serverside;

use Event\AbstractEventHandler;
use Exception;
use Statistics\StatisticsContext;
use Statistics\StatisticsFacade;
use Statistics\StatisticsKeys;
use User\IdentificationService;
use User\SessionUser;
use User\SessionUserStorage;

/**
 * Class AssociateLoginIdEventHandler
 * @package Event\Handler\Serverside
 */
class AssociateLoginIdEventHandler extends AbstractEventHandler {
    /**
     * Возвращает данные переданные в контекст события
     * @throws Exception
     * @return mixed
     */
    public function getData() {
        if (!is_array($this->data)) {
            $this->data = array();
        }

        if (!array_key_exists('loginId', $this->data)
         || !array_key_exists('statId', $this->data)
         || !array_key_exists('loginIdHash', $this->data)
        ) {
            throw new Exception("не передан loginId или statId", 401);
        }

        return $this->data;
    }

    /**
     * Запускает обработку события
     */
    public function run() {
        $data = $this->getData();

        $IdentificationService = new IdentificationService();
        $statId  = $IdentificationService->getStatId($data['loginId'], $data['statId'], $data['loginIdHash']);
        $Storage = SessionUserStorage::getInstance();
        $User    = $Storage->getById($statId);

        if (!isset($data['username']) || !isset($data['email'])) {
            $data['username'] = '';
            $data['email']    = '';
            $this->log('Не нашел username или email:' . PHP_EOL . var_export($data, true));
        }

        if (!$User) {
            $User = new SessionUser();
            $User->created = time();
        }

        $User->import(array(
            'id'           => $statId,
            'login_id'     => $data['loginId'],
            'username'     => $data['username'],
            'email'        => $data['email'],
            'last_request' => time(),
        ));

        $Storage->save($User);


        StatisticsFacade::write(StatisticsKeys::KEY_USER_REGISTER, array(
            StatisticsContext::USER => $User,
        ));
        StatisticsFacade::writeUnique(StatisticsKeys::KEY_USER_REGISTER_UNIQUE, array(
            StatisticsContext::USER      => $User,
            StatisticsContext::FLAG_NAME => $User->getId(),
        ));

        return array(
            'isAssociate' => true,
        );
    }

    protected function log($data) {
        $path = PATH_LOGS . '/test.log';
        file_put_contents($path, $data . PHP_EOL, FILE_APPEND);
    }
}