<?php
namespace Event\Handler\Serverside;

use Event\AbstractEventHandler;
use Exception;
use User\IdentificationService;
use User\SessionUser;
use User\SessionUserStorage;

/**
 * Абстрактное серверсайд юзер-событие
 * @author akiselev
 */
abstract class AbstractServersideUserEventHandler extends AbstractEventHandler {
    /**
     * @var SessionUser
     */
    protected $UserContainer = null;

    /**
     * Добавляет данные в контекст события
     * @param mixed $data
     * @throws Exception
     */
    public function setData($data) {
        $this->data = $data;

        if (!array_key_exists('loginId', $data) || !array_key_exists('loginIdHash', $data)) {
            throw new Exception("Не передан loginId или loginIdHash", 401);
        }

        $IdentificationService = new IdentificationService();
        $statId  = $IdentificationService->getStatId($data['loginId'], null, $data['loginIdHash']);
        $Storage = SessionUserStorage::getInstance();
        $User    = $Storage->getById($statId);

        if (!$User) {
            throw new Exception("Не верный loginId или loginIdHash", 401);
        }

        $this->UserContainer = $User;
    }
}