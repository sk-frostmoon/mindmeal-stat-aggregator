<?php
namespace Event\Handler\Serverside;

use Event\AbstractEventHandler;
use Exception;
use Statistics\StatisticsContext;
use Statistics\StatisticsFacade;

/**
 * Обработчик события "написать статистику сервера"
 * @author akiselev
 */
class ServersideStatisticsEventHandler extends AbstractEventHandler {
    /**
     * Запускает обработку события
     */
    public function run() {
        $context = $this->data;

        if (!array_key_exists('key', $context)) {
            throw new Exception('Ключ статистики не передан');
        }

        if (array_key_exists('context', $context)) {
            $contextData = $context['context'];
        } else {
            $contextData = [];
        }

        if (array_key_exists('uniqueContext', $context)) {
            StatisticsFacade::writeUnique($context['key'], array(
                StatisticsContext::FLAG_NAME => $context['uniqueContext'],
                StatisticsContext::CONTEXT   => $contextData,
            ));
        } else {
            StatisticsFacade::write($context['key'], [
                StatisticsContext::CONTEXT => $contextData,
            ]);
        }
    }
}