<?php
namespace Statistics;

use DateTime;
use Elasticsearch\Client;
use Storage\AbstractElasticSearchStorage;
use Utils\Config;

/**
 * Хранилище статистики в ElasticSearch
 * @author akiselev
 */
class StatisticsElasticStorage extends AbstractElasticSearchStorage {
    const INDEX = 'statistics';
    const INDEX_DEV = 'statistics_dev';
    const TYPE  = 'event';

    public $isDev = false;

    /**
     * @var null
     */
    protected $bulkData = array();

    public function __construct() {
        if (isset($_SERVER['dev']) && $_SERVER['dev']) {
            $this->isDev = true;
        }
    }

    /**
     * Вставляет запись в elasticsearch
     * @param array $context
     * @return string
     */
    public function write(array $context = array()) {
        $context = $this->normalizeContext($context);

        $params = array();
        if ($this->isDev) {
            $params['index'] = static::INDEX;
        } else {
            $params['index'] = static::INDEX;
        }

        $params['type']  = static::TYPE;
        $params['body']  = $context;

        // Faster execute command
        // todo Only for testing
//        {
//            $config = Config::get('elasticsearch');
//            $context = json_encode($context);
//            $url = 'http://' . $config['host'] . ':' . $config['port'];
//            return exec("curl -XPOST '" . $url . "/" . self::INDEX .  "/" . self::TYPE.  "/' -d '" . $context .  "'");
//        }

        $res = $this->getClient()->index($params);
        return $res;
    }

    /**
     * Нормализует данные для индекса. Например, изменяет формат времени
     * @param array $context
     * @return array
     */
    protected function normalizeContext(array $context) {
        $context['@timestamp'] = date(DateTime::ISO8601, $context['@timestamp']);

        if (array_key_exists('user', $context)) {
            if (array_key_exists('last_request', $context['user'])) {
                $context['user']['last_request'] = date(DateTime::ISO8601, $context['user']['last_request']);
            }

            if (array_key_exists('created', $context['user'])) {
                $context['user']['created'] = date(DateTime::ISO8601, $context['user']['created']);
            }

            if (array_key_exists('first_login_time', $context['user'])) {
                $context['user']['first_login_time'] = date(DateTime::ISO8601, $context['user']['first_login_time']);
            }

            if (array_key_exists('last_login_time', $context['user'])) {
                $context['user']['last_login_time'] = date(DateTime::ISO8601, $context['user']['last_login_time']);
            }
            
            if (array_key_exists('sol_first_play_game', $context['user'])) {
                $context['user']['sol_first_play_game'] = date(DateTime::ISO8601, $context['user']['sol_first_play_game']);
            }

            if (array_key_exists('sol_last_play_game', $context['user'])) {
                $context['user']['sol_last_play_game'] = date(DateTime::ISO8601, $context['user']['sol_last_play_game']);
            }
        }

        return $context;
    }

    /**
     * Добавляет данные в bulk запрос. Нужно для множественной вставки
     * @param array $context
     */
    public function appendBulk(array $context) {
        $context = $this->normalizeContext($context);

        $this->bulkData[] = array(
            'context' => $context,
        );
    }

    /**
     * Выполняет bulk запрос, вставляет пачку данных в elasticsearch
     * @return array
     */
    public function execBulk() {
        $params = array();

        if ($this->isDev) {
            $params['index'] = static::INDEX_DEV;
        } else {
            $params['index'] = static::INDEX;
        }

        $params['type']  = static::TYPE;
        $params['body']  = array();

        foreach ($this->bulkData as $data) {
            $params['body'][] = array(
                'index' => array()
            );

            $params['body'][] = $data['context'];
        }

        $this->bulkData = array();
        $res = $this->getClient()->bulk($params);

        return $res;
    }

    /**
     * Обновляет маппинг elasticsearch
     * ВНИМАНИЕ!!! Использовать с осторожностью, т.к. после обновления маппинга, данные чистятся.
     * Для переиндексирования данных воспользуйся готовым скриптом.
     */
    public function makeMapping() {
        if ($this->isDev) {
            $indexParams['index'] = self::INDEX_DEV;
        } else {
            $indexParams['index'] = self::INDEX;
        }

        $indexParams['body']['settings']['number_of_shards']   = 4;
        $indexParams['body']['settings']['number_of_replicas'] = 0;

        $indexParams['body']['mappings'][self::TYPE] = array(
            '_source' => array(
                'enabled' => true
            ),
            'properties' => array(
                '@key' => array(
                    'type' => 'string',
                    'index' => 'not_analyzed',
                ),
                '@count' => array(
                    'type' => 'float',
                    'index' => 'not_analyzed',
                ),
                '@timestamp' => array(
                    'type' => 'date',
                ),
                'user' => array(
                    'type' => 'object',
                    'properties' => array(
                        'id' => array(
                            'type' => 'string',
                            'index' => 'not_analyzed',
                        ),
                        'last_request' => array(
                            'type' => 'date',
                        ),
                        'created' => array(
                            'type' => 'date',
                        ),
                        'ref' => array(
                            'type' => 'string',
                            'index' => 'not_analyzed',
                        ),
                        'ref_domain' => array(
                            'type' => 'string',
                            'index' => 'not_analyzed',
                        ),
                        'url' => array(
                            'type' => 'string',
                            'index' => 'not_analyzed',
                        ),
                        'screen' => array(
                            'type' => 'object',
                            'properties' => array(
                                'width' => array(
                                    'type' => 'integer'
                                ),
                                'height' => array(
                                    'type' => 'integer'
                                ),
                                'size' => array(
                                    'type' => 'string',
                                    'index' => 'not_analyzed',
                                ),
                            ),
                        ),
                        'ip' => array(
                            'type' => 'ip',
                            'index' => 'no',
                        ),
                        'browser' => array(
                            'type' => 'string',
                            'index' => 'not_analyzed',
                        ),
                        'browser_version' => array(
                            'type' => 'string',
                            'index' => 'not_analyzed',
                        ),
                        'os' => array(
                            'type' => 'string',
                            'index' => 'not_analyzed',
                        ),
                        'data' => array(
                            'type' => 'object',
                            'dynamic' => true,
                        ),
                        'utm' => array(
                            'type' => 'object',
                            'properties' => array(
                                'utm_campaign' => array(
                                    'type' => 'string',
                                    'index' => 'not_analyzed',
                                ),
                                'utm_medium' => array(
                                    'type' => 'string',
                                    'index' => 'not_analyzed',
                                ),
                                'utm_source' => array(
                                    'type' => 'string',
                                    'index' => 'not_analyzed',
                                ),
                                'utm_term' => array(
                                    'type' => 'string',
                                    'index' => 'not_analyzed',
                                ),
                            ),
                        ),
                        'geo' => array(
                            'type' => 'object',
                            'properties' => array(
                                'city_name' => array(
                                    'type' => 'string',
                                    'index' => 'not_analyzed',
                                ),
                                'county_code' => array(
                                    'type' => 'string',
                                    'index' => 'not_analyzed',
                                ),
                                'county_name' => array(
                                    'type' => 'string',
                                    'index' => 'not_analyzed',
                                ),
                            ),
                        ),
                    ),
                ),
                'userFlag' => array(
                    'type' => 'object',
                    'dynamic' => true,
                ),
            ),
        );

//        $this->getClient()->
        try {
            if ($this->isDev) {
                $this->getClient()->indices()->delete(array('index' => self::INDEX_DEV));
            } else {
                $this->getClient()->indices()->delete(array('index' => self::INDEX));
            }
        } catch (\Exception $Ex) {

        }

        $this->getClient()->indices()->create($indexParams);
    }
} 