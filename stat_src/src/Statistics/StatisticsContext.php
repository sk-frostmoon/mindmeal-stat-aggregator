<?php
namespace Statistics;

/**
 * Константы для контекста статистики
 * @author akiselev
 */
class StatisticsContext {
    const USER       = 'user';
    const USER_FLAGS = 'user_flags';
    const FLAG_NAME  = 'flag_name';
    const CONTEXT    = 'context';
    const TIMESTAMP  = '@timestamp';
}