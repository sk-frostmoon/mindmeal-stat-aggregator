<?php
namespace Statistics\Reader;


class StatisticsReaderFilter {
    /**
     * Ключ "@key" событие которого запрашиваем из статистики
     * @var string
     */
    protected $key = null;

    /**
     * Время с которого запрашиваем события
     * @var int
     */
    protected $fromTime = null;

    /**
     * Время до которого запрашиваем события
     * @var int
     */
    protected $ещTime = null;
} 