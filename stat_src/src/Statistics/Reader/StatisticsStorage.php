<?php
namespace Statistics\Reader;
use Report\Query\ResultQuery;
use Report\Query\StatisticsQueryBuilder;

/**
 * Хранилище статистики. Делает запросы в хранилище и возвращает "сырые данные"
 * @author akiselev
 */
class StatisticsStorage {
    const LOGIN    = 'admin';
    const PASSWORD = 'theAdminPassword';

    /**
     * URL для обращения в elasticsearch
     * @var string
     */
    protected $urlElastic = 'http://95.213.129.108';

    /**
     * Запрашивает данные в es по запросу "query"
     * @param StatisticsQueryBuilder[] $Queries
     * @return ResultQuery[]
     */
    public function exec($Queries) {
        $Results = array();
        foreach ($Queries as $key => $Query) {
            $resultQuery = $this->queryExec($Query->makeQueryString());

            $Result = $Query->createResult()
                            ->setResult($resultQuery)
                            ->normalizeData()
            ;

            $Results[$key] = $Result->getResult();
        }

        return $Results;
    }

    /**
     * Выполняет запрос
     * @param string $query
     * @return array
     */
    public function queryExec($query) {
        $user  = static::LOGIN . ':' . static::PASSWORD;

        if (isset($_SERVER['DEV']) && $_SERVER['DEV']) {
            $index = 'statistics';
//            $index = 'statistics_dev';
        } else {
            $index = 'statistics';
        }

        $url = <<<EOF
curl --user {$user} -XGET '{$this->urlElastic}/{$index}/_search?pretty' -d '{$query}';
EOF;


        exec($url, $out);
        $result = json_decode(implode("\n", $out), true);

        return $result;
    }
}