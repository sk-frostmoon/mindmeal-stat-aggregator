<?php
namespace Statistics\Reader;

use DateTime;
use Storage\AbstractElasticSearchStorage;

class StatisticsReader extends AbstractElasticSearchStorage {
    /**
     * @param $offset
     * @param $count
     * @return array
     */
    public function get($offset, $count) {
        $Client = $this->getClient();

        $searchParams = array();
        $searchParams['index']  = 'statistics';
        $searchParams['type']   = 'event';
        $searchParams['from']   = $offset;  // offset
        $searchParams['size']   = $count;  // count
        $searchParams['body']['query']  = [
            'filtered' => [
                'query' => [
                    'bool' => [
                        'should' => [
                            [
                                'query_string' => [
                                    'query' => '@key:user_register',
                                ],
                            ],
                        ],
                    ],
                ],

                'filter' => [
                    'bool' => [
                        'must' => [
                            [
                                'terms' => [
                                    '_type' => [
                                        'event'
                                    ],
                                ],
                            ],
                            [
                                'range' => [
                                    '@timestamp' => [
                                        'from' => date(DateTime::ISO8601, $this->startTime),
                                        'to'   => date(DateTime::ISO8601, $this->endTime),
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $data = $Client->search($searchParams);

        return $data;
    }
} 