<?php
namespace Statistics;
use Exception;
use Profiler\Profiler;
use User\SessionUser;
use User\UserFlags;

/**
 * Фасад для работы со статистикой
 * @author akiselev
 */
class StatisticsFacade {
    // Текущая версия данных статистики
    const CURRENT_DATA_VERSION = 1;

    protected static $Instance = null;

    protected static $isDev = false;

    /**
     * Инстанс фасада статистики
     * @return StatisticsFacade
     */
    public static function getInstance() {
        if (is_null(static::$Instance)) {
            static::$Instance = new static();
        }

        return static::$Instance;
    }

    /**
     * @param string $key в данный момент - это имя модели для вызова
     * @param array $context
     * @param int $count
     * @param bool $writeStat
     * @return bool
     */
    public static function write($key, array $context = array(), $count = 1, $writeStat = true) {
        $startTime = Profiler::microtime_float();
        $Instance  = self::getInstance();

        // Подготовим контекст статистики
        $preparedContext = $Instance->prepareContext($context);
        $preparedContext['@key'] = $key;
        $preparedContext['@count'] = $count;

        if (!isset($preparedContext[StatisticsContext::TIMESTAMP])) {
            $preparedContext[StatisticsContext::TIMESTAMP] = time();
        }

        // Пишем в персистентное хранилище
        $startRedisTime = Profiler::microtime_float();
        $PersistentStorage = $Instance->getPersistentStorage();
        $PersistentStorage->write($preparedContext);
        $resultRedisTime = Profiler::microtime_float() - $startRedisTime;

        // Пишем в ElasticSearch
        $startElasticTime = Profiler::microtime_float();
        $ElasticStorage = $Instance->getElasticStorage();
        $res = $ElasticStorage->write($preparedContext);
        $resultElasticTime = Profiler::microtime_float() - $startElasticTime;

        $resultTime = Profiler::microtime_float() - $startTime;
        if ($writeStat && rand(0, 5) == 1) {
            // Писать такаую статистику каждый раз, нет смысла
            StatisticsFacade::write('execution_time', array(
                StatisticsContext::CONTEXT => array(
                    'redis_time'   => $resultRedisTime,
                    'elastic_time' => $resultElasticTime,
                ),
            ), $resultTime, false);
        }

        return true;
    }

    /**
     * Обязательно в контексте передать юзера StatisticsContext::USER
     * Для уникальной статистики передать дату StatisticsContext::DATE
     * По умолчанию, пишется раз в день.
     *
     * @param string $key
     * @param array $context
     * @param int $count
     * @return bool
     * @throws \Exception
     */
    public static function writeUnique($key, array $context = array(), $count = 1) {
        if (!array_key_exists(StatisticsContext::USER, $context)) {
            throw new Exception('Не передан юзер для обработки!');
        }

        if (array_key_exists(StatisticsContext::FLAG_NAME, $context)) {
            $flagName = $context[StatisticsContext::FLAG_NAME];
        } else {
            $flagName = date('md', time());
        }

        /** @var SessionUser $User */
        $User  = $context[StatisticsContext::USER];
        $Flags = $User->getFlags();
        $flag  = $key . ':' . $flagName;

        if (!$Flags->isChecked($flag)) {
            static::write($key, $context, $count);
            $Flags->setFlag($flag);
            return true;
        }

        return false;
    }

    /**
     * Подготавливает контекст для статистики
     * @param array $context
     * @return array
     */
    protected function prepareContext($context) {
        $preparedContext = array();

        if (array_key_exists(StatisticsContext::USER, $context)) {
            /** @var SessionUser $User */
            $User = $context[StatisticsContext::USER];
            $preparedContext['user'] = $User->export();
            $preparedContext['user']['data'] = array_filter($preparedContext['user']['data']);
            $preparedContext['user']['geo'] = array_filter($preparedContext['user']['geo']);
            $preparedContext['user'] = array_filter($preparedContext['user']);
        }

        if (array_key_exists(StatisticsContext::USER_FLAGS, $context)) {
            /** @var UserFlags $Flags */
            $Flags = $context[StatisticsContext::USER_FLAGS];
            $preparedContext['userFlag'] = $Flags->exportFlagWithChecked();
        }

        if (array_key_exists(StatisticsContext::CONTEXT, $context)) {
            $preparedContext['context_data'] = $context[StatisticsContext::CONTEXT];
        }

        if (array_key_exists(StatisticsContext::TIMESTAMP, $context)) {
            $preparedContext[StatisticsContext::TIMESTAMP] = $context[StatisticsContext::TIMESTAMP];
        }

        $preparedContext = array_filter($preparedContext);

        return $preparedContext;
    }

    /**
     * @var StatisticsRedisStorage
     */
    protected $PersistentStorage = null;

    /**
     * @return StatisticsRedisStorage
     */
    protected function getPersistentStorage() {
        if (is_null($this->PersistentStorage)) {
            if (static::$isDev) {
                $_SERVER['dev'] = 1;
            }

            $this->PersistentStorage = new StatisticsRedisStorage();
        }

        return $this->PersistentStorage;
    }

    /**
     * @var StatisticsElasticStorage
     */
    protected $ElasticStorage = null;

    /**
     * @return StatisticsElasticStorage
     */
    protected function getElasticStorage() {
        if (is_null($this->ElasticStorage)) {
            if (static::$isDev) {
                $_SERVER['dev'] = 1;
            }

            $this->ElasticStorage = new StatisticsElasticStorage();
        }

        return $this->ElasticStorage;
    }
}