<?php
namespace Statistics;

use Storage\AbstractRedisStorage;

/**
 * Хранилище данных статистики в Redis
 * @author akiselev
 */
class StatisticsRedisStorage extends AbstractRedisStorage {
    /**
     * Текущая версия статистики
     */
    const CURRENT_VERSION = 'v1';

    /**
     * Вставляет запись
     * @param array $context
     * @return int
     */
    public function write(array $context = array()) {
        $data = json_encode($context);

        return $this->getConnection()->executeRaw(array('RPUSH', $this->makeKey(), $data));
    }

    /**
     * Вставляет много записей
     * @param array $eventsContext
     */
    public function writeMany(array $eventsContext) {
        $eventsData = array_map(function($context) {
            return json_encode($context);
        }, $eventsContext);

        $arguments = array('RPUSH', $this->makeKey());
        foreach ($eventsData as $data) {
            $arguments[] = $data;
        }

        $this->getConnection()->executeRaw($arguments);
    }

    /**
     * Возвращает записи из базы
     * @param int $start сколько запиcей вытянуть
     * @param int $length смещение
     * @return array
     */
    public function get($start, $length) {
        return $this->getConnection()->lrange($this->makeKey(), $start, $start + $length - 1);
    }

    /**
     * @param $index
     * @param $value
     * @return mixed
     */
    public function lset($index, $value) {
        $data = json_encode($value);

        return $this->getConnection()->executeRaw(array('LSET', $this->makeKey(), $index, $data));
    }

    /**
     * Формирует ключ для статистики
     * @return string
     */
    protected function  makeKey() {
        return 'statistics:events:' . self::CURRENT_VERSION;
    }
}