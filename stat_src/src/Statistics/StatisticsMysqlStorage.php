<?php
namespace Statistics;

use PDO;
use PDOException;
use Storage\AbstractPersistentStorage;

/**
 * Хранилище данных статистики в MySQL
 * @author akiselev
 */
class StatisticsMysqlStorage extends AbstractPersistentStorage {
    /**
     * Массив схемы таблицы
     * @return array
     */
    public function getDataScheme() {
        return array(
            'id'      => PDO::PARAM_INT,
            'key'     => PDO::PARAM_STR,
            'time'    => PDO::PARAM_INT,
            'context' => PDO::PARAM_STR
        );
    }

    /**
     * Имя таблицы
     * @return string
     */
    public function getTableName() {
        return 'eventsStatistic';
    }

    /**
     * Вставляет запись
     * @param array $context
     * @internal param string $key
     * @return int
     */
    public function write(array $context = array()) {
        $data = array(
            'context' => json_encode($context),
        );

        try {
            $this->insert($data);
        } catch (PDOException $Ex) {
            return false;
        }

        return true;
    }

    public function get($offset, $limit) {

    }
}