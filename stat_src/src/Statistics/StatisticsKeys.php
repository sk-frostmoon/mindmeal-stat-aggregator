<?php
namespace Statistics;
use ReflectionClass;

/**
 * Ключи для статистики
 * @author akiselev
 */
class StatisticsKeys {
    /**
     * Ключи статистики
     */
    const KEY_USER_ENTER           = 'user_enter';
    const KEY_USER_REGISTER        = 'user_register';
    const KEY_USER_REGISTER_UNIQUE = 'user_register_unique';
    const KEY_USER_LOGIN           = 'user_login';
    const KEY_USER_LOGIN_UNIQUE    = 'user_login_unique';
    const KEY_USER_ENTER_GAME      = 'user_enter_game';
    const KEY_USER_FIELD_FORM      = 'user_field_form';
    const KEY_USER_CLICK_DOWNLOAD  = 'user_click_download';

    /**
     * Просмотр странички сайта
     */
    const KEY_PAGE_HIT              = 'page_hit';              // Каждый хит на страницу + сайт
    const KEY_PAGE_HIT_USER_UNIQUE  = 'page_hit_unique';       // Уникальные для юзера хиты страницы
    const KEY_PAGE_HIT_DAILY_UNIQUE = 'page_hit_daily_unique'; // Уникальный за текущий день хит страницы
    const KEY_SITE_HIT_USER_UNIQUE  = 'site_hit_unique';       // Уникальных для юзера хит на сайте
    const KEY_SITE_HIT_DAILY_UNIQUE = 'site_hit_daily_unique'; // Уникальный за текущий день хит сайта

    /**
     * Ключи статистики по SOL
     */
    const KEY_SOL_FIRST_GAME_ENTER = 'sol_first_begin_play_game';
    const KEY_SOL_LEVEL_UP         = 'sol_level_up';

    /**
     * Возвращает массив констант
     * @return array
     */
    public static function getConstants() {
        $oClass = new ReflectionClass(get_called_class());
        return $oClass->getConstants();
    }
} 