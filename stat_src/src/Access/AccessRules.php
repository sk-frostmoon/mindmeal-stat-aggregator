<?php
namespace Access;

use Utils\Config;

class AccessRules {
    /**
     * Действия
     */
    const ACTION_VIEW      = 'view';
    const ACTION_VIEW_ITEM = 'view_item';
    const ACTION_CREATE    = 'create';
    const ACTION_EDIT      = 'edit';

    /**
     * Уровни доступа
     */
    const LEVEL_ALL       = 'all';
    const LEVEL_ONLY_SELF = 'only_self';

    /**
     * Правила доступа
     */
    const RULE_USERS_PAGE           = 'users_page';           // Страничка администрирования пользователей
    const RULE_AD_CAMPAIGN_PAGE     = 'ad_campaign_page';     // Страничка рекламных кампаний
    const RULE_AD_CAMPAIGN_CATEGORY = 'ad_campaign_category'; // Страничка рекламных кампаний - категории
    const RULE_REPORT_PAGE          = 'report_page';          // Страничка отчета
    const RULE_ADVANCED_REPORT_PAGE = 'advanced_report_page'; // Страничка подробного отчета

    /**
     * Конфигурация правил доступа
     * @var array
     */
    static protected $rulesConfig = array();

    /**
     * Хранилище групп
     * @var AccessGroupStorage
     */
    static protected $GroupStorage = null;

    /**
     * Имеет ли юзер доступ к динамическому контенту
     * @param int      $userId        Идентификатор юзера
     * @param string   $rule          Правило
     * @param string   $action        Действие
     * @param int|null $contentUserId Идентификатор юзера, создавшего контент
     * @return bool
     */
    public static function hasAccess($userId, $rule, $action, $contentUserId = null) {
        $group       = AccessRules::getGroupForUser($userId);
        $accessLevel = AccessRules::getAccessLevel($group, $rule, $action);

        switch ($accessLevel) {
            // Уровень "только свои записи"
            case AccessRules::LEVEL_ONLY_SELF:
                return $contentUserId == $userId;

            // Уровень "все"
            case AccessRules::LEVEL_ALL:
                return true;

            // Неизвестный уровень прав
            default:
                return false;
        }
    }

    /**
     * Возвращет группу для юзера
     * @param int $userId
     * @return string
     */
    public static function getGroupForUser($userId) {
        $group = AccessRules::getGroupStorage()->getGroupForUser($userId);
        if (!$group) {
            $group = AccessGroupConfig::DEFAULT_GROUP;
        }

        return $group;
    }

    /**
     * Возвращает уровень доступа группы для действия по указанному правилу
     * @param string $group
     * @param string $rule
     * @param string $action
     * @return string|null
     */
    public static function getAccessLevel($group, $rule, $action) {
        $rules = AccessRules::getRulesConfig();

        if (!array_key_exists($rule, $rules)) {
            // Не найдено правило
            return null;
        }

        if (!array_key_exists($action, $rules[$rule])) {
            // Не найдено действие
            return null;
        }

        if (!array_key_exists($group, $rules[$rule][$action])) {
            // Не найдена группа
            return null;
        }

        return $rules[$rule][$action][$group];
    }

    /**
     * Возвращает хранилище групп
     * @return AccessGroupStorage
     */
    protected static function getGroupStorage() {
        if (is_null(static::$GroupStorage)) {
            static::$GroupStorage = new AccessGroupStorage();
        }

        return static::$GroupStorage;
    }

    /**
     * Возвращает конфиг правил доступа
     * @return array
     */
    protected static function getRulesConfig() {
        if (!static::$rulesConfig) {
            static::$rulesConfig = Config::get('access/rules');
        }

        return static::$rulesConfig;
    }
}