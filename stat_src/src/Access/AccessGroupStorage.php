<?php
namespace Access;

use Storage\AbstractRedisStorage;

/**
 * Хранилище групп юзеров
 * @author akiselev
 */
class AccessGroupStorage extends AbstractRedisStorage {
    /**
     * Сохраняет группу для юзера
     * @param int $userId
     * @param string $group
     * @return bool
     */
    public function saveGroupForUserId($userId, $group) {
        $key = $this->makeKey($userId);
        return (bool) $this->execute(['SET', $key, $group]);
    }

    /**
     * Возвращает группу для юзера
     * @param int $userId
     * @return string
     */
    public function getGroupForUser($userId) {
        $key = $this->makeKey($userId);
        return $this->execute(['GET', $key]);
    }

    /**
     * Строит ключ для сохранения в базу
     * @param int $userId
     * @return string
     */
    protected function makeKey($userId) {
        return "user:group:{$userId}";
    }
} 