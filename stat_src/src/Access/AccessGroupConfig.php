<?php
namespace Access;

/**
 * Хранилище групп юзеров
 * @author akiselev
 */
class AccessGroupConfig {
    const KIRILL   = 'admin';
    const MISHA    = 'manager';
    const INVESTOR = 'developer';
    const WATCHER  = 'guest';

    const DEFAULT_GROUP = self::WATCHER;

    /**
     * Возвращает массив групп для юзера
     * @return array
     */
    public function getGroups() {
        return [
            self::KIRILL => [
                'title' => 'Кирилл'
            ],
            self::MISHA => [
                'title' => 'Миша'
            ],
            self::INVESTOR => [
                'title' => 'Инвестор'
            ],
            self::WATCHER => [
                'title' => 'Наблюдатель'
            ],
        ];
    }
}