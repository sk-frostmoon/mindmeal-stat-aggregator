<?php
use User\IdentificationService;

/**
 * Отдает overview.js файл с указанием идентификатора юзера
 * @author akiselev
 */
class OverviewJsMaker {
    /**
     * Шаблон для отдачи
     */
    const FILE_TEMPLATE = '/overview.js.php';

    /**
     * Подставляет в файл идентификатор и возвращает данные файла
     * @return string
     */
    public function make() {
        $loginId     = null;
        $loginIdHash = null;

        if (array_key_exists('userId', $_GET) && array_key_exists('hash', $_GET)) {
            // Зашел зарегистрированный пользователь!
            $loginId     = $_GET['userId'];
            $loginIdHash = $_GET['hash'];
        }

        if (array_key_exists('sessionId', $_GET)) {
            $userId = $_GET['sessionId'];
        } else {
            $userId = $this->getIdentificationService()->getStatId($loginId, null, $loginIdHash);
        }

        ob_start();
            // Внутри проставляется $userId в объект юзера
            include PATH_JS . self::FILE_TEMPLATE;
        return ob_get_clean();
    }

    /**
     * Возвращает сервис идентификации
     * @return IdentificationService
     */
    protected function getIdentificationService() {
        return new IdentificationService();
    }
} 