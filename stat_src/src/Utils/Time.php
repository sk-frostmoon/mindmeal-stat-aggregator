<?php
namespace Utils;

/**
 * Класс для работы со временем
 * @author akiselev
 */
class Time {
    const TIME_MINUTE    = 60;
    const TIME_HOUR      = 3600;
    const TIME_DAY       = 86400;
    const TIME_WEEK      = 604800;
    const TIME_MONTH_30  = 2592000; // 30 дней
    const TIME_MONTH_31  = 2678400; // 31 день
}