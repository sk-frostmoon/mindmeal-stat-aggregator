<?php
namespace Utils;

/**
 * Класс для работы с конфигами
 * @author akiselev
 */
class Config {
    /**
     * @var array
     */
    protected static $cache = array();

    /**
     * Возвращает данные
     * @param string $name
     * @param string $connection
     * @return array
     */
    public static function get($name, $connection = 'default') {
        if (array_key_exists($name, static::$cache)) {
            if (isset(static::$cache[$name][$connection])) {
                return static::$cache[$name][$connection];
            }

            return static::$cache[$name];
        }

        $config = include PATH_CONFIG . '/' . $name . '.php';
        static::$cache[$name] = $config;

        if (isset($config[$connection])) {
            return $config[$connection];
        }

        return $config;
    }
} 