<?php
namespace Utils;
use Memcache;

/**
 * Соединение с memcache
 * @author akiselev
 */
class MemcacheConnection {
    /**
     * @var Memcache
     */
    protected static $Connection = null;

    /**
     * Создает подключение. Не кэширует
     * @return Memcache
     */
    public static function create() {
        if (is_null(static::$Connection)) {
            $config = Config::get('memcache');

            static::$Connection = new Memcache();
            static::$Connection->addServer($config['host'], $config['port']);

        }

        return static::$Connection;
    }
}