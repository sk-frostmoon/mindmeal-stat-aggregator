<?php
namespace Storage;

use PDO;
use PDOException;
use PDOStatement;
use Utils\Config;

/**
 * Абстрактное персистентное хранилище
 * @author akiselev
 */
abstract class AbstractPersistentStorage {
    const INSERT_QUERY = 'INSERT INTO `%s` (%s) VALUES (%s);';

    /**
     * @var PDO
     */
    protected $PDO = null;

    /**
     * Массив схемы таблицы
     * @return array
     */
    abstract public function getDataScheme();

    /**
     * Имя таблицы
     * @return string
     */
    abstract public function getTableName();

    protected function makeBindFields(array $attr) {
        $bindFields = array();
        $scheme     = $this->getDataScheme();

        foreach ($attr as $key => $value) {
            if (array_key_exists($key, $scheme)) {
                $bindFields[':' . $key] = "`{$key}`";
            }
        }

        return $bindFields;
    }

    /**
     * @param array $attr массив параметров
     * @return array
     */
    protected function makeSaveFields(array $attr) {
        $saveFields = array();
        $scheme     = $this->getDataScheme();

        foreach ($attr as $key => $value) {
            if (array_key_exists($key, $scheme)) {
                $saveFields[':' . $key] = $value;
            }
        }

        return $saveFields;
    }

    /**
     * Вставляет запись в таблицу
     * @param array $data
     * @throws PDOException
     * @return int
     */
    public function insert(array $data) {
        $bindFields = $this->makeBindFields($data);
        $saveFields = $this->makeSaveFields($data);

        $Statement = $this->makeInsertSqlQuery($bindFields, $saveFields);
        if ($Statement->execute()) {
            return $this->getPDO()->lastInsertId();
        }

        throw new PDOException(implode(' ', $Statement->errorInfo()), $Statement->errorCode());
    }

    public function select($limit, $offset) {
        $table = $this->getTableName();
        $sql = "SELECT * FROM {$table} WHERE 1 LIMIT {$offset}, {$limit}";

        $Statement = $this->getPDO()->prepare($sql);
        $Statement->execute();

        return $Statement->fetchAll();
    }

    /**
     * @param string $field
     * @return int
     */
    public function getFieldType($field) {
        $scheme = $this->getDataScheme();
        if (array_key_exists($field, $scheme)) {
            return $scheme[$field];
        }

        return PDO::PARAM_STR;
    }

    /**
     * @param PDOStatement $Statement
     * @param array $saveFields
     * @return PDOStatement
     */
    protected function bindStatementValues(PDOStatement $Statement, $saveFields) {
        foreach ($saveFields as $param => $value) {
            $Statement->bindValue($param, $value, $this->getFieldType($param));
        }

        return $Statement;
    }

    /**
     * Формирует запрос на insert в БД
     * @param array $bindFields
     * @param array $saveFields
     * @return PDOStatement
     */
    protected function makeInsertSqlQuery(array $bindFields, array $saveFields ) {
        $sql = sprintf(
            static::INSERT_QUERY,
            $this->getTableName(),
            implode(', ', array_values($bindFields)),
            implode(', ', array_keys($saveFields))
        );

        $Statement = $this->getPDO()->prepare($sql);
        $Statement = $this->bindStatementValues($Statement, $saveFields);

        return $Statement;
    }

    /**
     * @param string $configName
     * @return PDO
     */
    public function getPDO($configName = 'default') {
        if (is_null($this->PDO)) {
            $config = Config::get('mysql', $configName);

            $port = "";
            if (isset($config['port'])) {
                $port = "port={$config['port']};";
            }

            $dsn = "mysql:host={$config['host']};dbname={$config['dbname']};" . $port;

            $this->PDO = new PDO($dsn, $config['user'], $config['password'], array(PDO::ATTR_PERSISTENT => true));
        }

        return $this->PDO;
    }
}