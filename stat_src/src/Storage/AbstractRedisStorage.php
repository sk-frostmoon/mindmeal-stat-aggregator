<?php
namespace Storage;

use Exception;
use Predis\Client;
use Utils\Config;

/**
 * Абстрактное хранилище в Redis
 * @author akiselev
 */
abstract class AbstractRedisStorage {
    /**
     * @var Client
     */
    protected $Connection = null;

    /**
     * Возвращает клиент для Redis
     * @param string $connectionName
     * @return Client
     * @throws Exception
     */
    public function getConnection($connectionName = 'default') {
        if (is_null($this->Connection)) {
//            if (isset($_SERVER['DEV']) && $_SERVER['DEV']) {
//                $connectionName = 'dev';
//            }

            $config = Config::get('redis/redis', $connectionName);
            if (!$config) {
                throw new Exception('Not found config for redis: "' . $connectionName . '"');
            }

            $this->Connection = new Client($config);
        }

        return $this->Connection;
    }

    /**
     * @param array $arguments
     * @param bool $error
     * @return mixed
     */
    public function execute(array $arguments, &$error = null) {
        return $this->getConnection()->executeRaw($arguments, $error);
    }
} 