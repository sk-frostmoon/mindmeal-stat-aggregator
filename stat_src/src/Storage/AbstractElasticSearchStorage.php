<?php
namespace Storage;

use Elasticsearch\Client;
use Utils\Config;

/**
 * Class AbstractElasticSearchStorage
 * @package Storage
 */
class AbstractElasticSearchStorage {
    /**
     * @var Client
     */
    private $Client = null;

    /**
     * Возвращает клиент для работы с elasticsearch
     * @param string $connectionName
     * @return Client
     */
    public function getClient($connectionName = 'default') {
        if (is_null($this->Client)) {
            $config = Config::get('elasticsearch', $connectionName);

            $params = array(
                'hosts' => array(
                    $config['host'] . ':' . $config['port'],
                )
            );
            $this->Client = new Client($params);
        }

        return $this->Client;
    }
} 