<?php
namespace User;

use Storage\AbstractRedisStorage;

/**
 * Флаги пользователя. Хранятся в Memcached
 * @author akiselev
 */
class UserFlags extends AbstractRedisStorage {
    /**
     * @var string
     */
    protected $userId = null;

    /**
     * Флаги пользователя
     */
    const FLAG_USER_ENTER           = 'user_enter'; // Юзер зашел
    const FLAG_FIELD_FORM           = 'field_form';  // Юзер Заполнил форму и нажал отправить
    const FLAG_IS_REGISTER          = 'is_register'; // Юзер зарегистрировался
    const FLAG_FORUM_ENTER          = 'forum_enter'; // Зашел на форум todo ?? нужно ли ??
    const FLAG_CLICK_DOWNLOAD_GAME  = 'click_download_game'; // Юзер нажал скачать игру
    const FLAG_FIRST_ENTER_GAME     = 'first_game_enter'; // Юзер зашел в игру

    const FLAG_SOL_FIRST_PLAY_GAME  = 'sol_first_play_game'; // Юзер зашел в игру
    const FLAG_SOL_LEVEL_UP         = 'sol_level_up';        // Юзер получил уровень

    /**
     * Возвращает флаги пользователя
     * @return array
     */
    public function getFlags() {
        return array(
            self::FLAG_USER_ENTER,
            self::FLAG_FIELD_FORM,
            self::FLAG_IS_REGISTER,
            self::FLAG_FORUM_ENTER,
            self::FLAG_CLICK_DOWNLOAD_GAME,
            self::FLAG_FIRST_ENTER_GAME,
            self::FLAG_SOL_FIRST_PLAY_GAME,
        );
    }

    /**
     * @param $userId
     */
    public function __construct($userId = null) {
        $this->userId = $userId;
    }

    /**
     * Проверяет, стоит ли флаг у юзера
     * @param string $flag
     * @param string $userId
     * @return bool
     */
    public function isChecked($flag, $userId = null) {
        $key       = $this->makeFlagKey($userId);
        $isSetFlag = (bool) $this->getConnection()->executeRaw(array('HGET', $key, $flag));
        return $isSetFlag;
    }

    /**
     * Устанвливает флаг
     * @param string $flag
     * @param string $userId
     * @return $this
     */
    public function setFlag($flag, $userId = null) {
        $key = $this->makeFlagKey($userId);
        $this->getConnection()->executeRaw(array('HSET', $key, $flag, true));
        return $this;
    }

    /**
     * Убирает флаг
     * @param string $flag
     * @param string $userId
     * @return $this
     */
    public function unsetFlag($flag, $userId = null) {
        $key = $this->makeFlagKey($userId);
        $this->getConnection()->executeRaw(array('HDEL', $key, $flag));
        return $this;
    }

    /**
     * Отдает массив флагов ключ=>значение: имя_флага => true|false
     * @param string $userId
     * @return array
     */
    public function exportFlagWithChecked($userId = null) {
        $key      = $this->makeFlagKey($userId);
        $result   = $this->getConnection()->executeRaw(array('HKEYS', $key));
        $flags    = array_flip($this->getFlags());
        $response = array();

        foreach ($result as $flag) {
            if (array_key_exists($flag, $flags)) {
                $response[$flag] = true;
            }
        }

        return $response;
    }


    /**
     * Строит ключ для флага
     * @param string $userId
     * @return string
     */
    protected function  makeFlagKey($userId = null) {
        $userId = $userId ? $userId : $this->userId;

        return "user:flags:{$userId}";
    }
} 