<?php
namespace User;

use Exception;
use Memcache;
use Utils\MemcacheConnection;

/**
 * Хранилище сессионных юзеров
 * @author akiselev
 */
class SessionUserMemcacheStorage implements UserStorageInterface {
    /**
     * Кэш юзеров
     * @var SessionUser[]
     */
    protected $UserCache = array();

    /**
     * Сохраняет юзера в сторадж
     * @param SessionUser $User
     */
    public function save(SessionUser $User) {
        $Connection = $this->getMemcache();
        $key = $this->makeKey($User->getId());

        $userData = json_encode($User->export());
        if ($Connection->set($key, $userData)) {
            // Обновим кэш
            $this->UserCache[$User->getId()] = $User;
        }
    }

    /**
     * Получает юзера по id
     * @param int $id
     * @return SessionUser
     * @throws Exception
     */
    public function getById($id) {
        if (array_key_exists($id, $this->UserCache)) {
            // Нашли юзера в кэше
            return $this->UserCache[$id];
        }

        $Connection = $this->getMemcache();
        $key = $this->makeKey($id);

        $userData = $Connection->get($key);
        if (!$userData) {
            return null;
        }
        $userData = json_decode($userData, true);
        $User = new SessionUser();
        $User->import($userData);

        $this->UserCache[$id] = $User;

        return $User;
    }

    /**
     * Отдает клиент Memcache
     * @return Memcache
     */
    protected function getMemcache() {
        return MemcacheConnection::create();
    }

    /**
     * Строит ключ для хранилища
     * @param int $id
     * @return string
     */
    protected function makeKey($id) {
        return "user:data:{$id}";
    }
} 