<?php
namespace User;

/**
 * Юзер в сессии
 * @author akiselev
 */
class SessionUser {
    /**
     * Идентификатор юзера (session id)
     * @var int
     */
    public $id = null;

    /**
     * Время последнего запроса в сервис статистики
     * @var int
     */
    public $last_request = 0;

    /**
     * Время создания контейнера
     * @var int
     */
    public $created = 0;

    /**
     * ГЕО данные юзера
     * @var array
     */
    public $geo = array();

    /**
     * Реферал юзера
     * @var string
     */
    public $ref = '';

    /**
     * Домен реферальной ссылки юзера
     * @var string
     */
    public $ref_domain = '';

    /**
     * URL страницы, с которой пришел запрос на статистику
     * @var string
     */
    public $url = '';

    /**
     * Размер экрана юзера
     * @var array
     */
    public $screen = array(
        'width'  => 0,
        'height' => 0,
        'size'   => '0x0',
    );

    /**
     * IP юзера
     * @var string
     */
    public $ip = '';

    /**
     * Браузер юзера
     * @var string
     */
    public $browser = '';

    /**
     * Версия браузера юзера
     * @var string
     */
    public $browser_version = '';

    /**
     * Операционная система юзера
     * @var string
     */
    public $os = '';

    /**
     * UTM метки с которыми пришел юзер
     * @var array
     */
    public $utm = array();

    /**
     * Любые другие динамические данные
     * @var array
     */
    public $data = array();

    /**
     * Id из сервиса login
     * @var int
     */
    public $login_id = 0;

    /**
     * Время первого логина на сайте
     * @var int
     */
    public $first_login_time = 0;

    /**
     * Время последнего логина на сайте
     * @var int
     */
    public $last_login_time = 0;

    /**
     * Имя юзера в login'е
     * @var string
     */
    public $username = '';

    /**
     * Email юзера в логине
     * @var string
     */
    public $email = '';

    /**
     * Первый вход в игру
     * @var int
     */
    public $sol_first_play_game = 0;

    /**
     * Послединй вход в игру
     * @var int
     */
    public $sol_last_play_game = 0;

    /**
     * История изменений UTM'ов
     * @var array
     */
    public $utm_history = array();

    /**
     * Объект для работы с флагами юзера
     * @var UserFlags
     */
    protected $Flags = null;

    /**
     * @return UserFlags
     */
    public function getFlags() {
        if (is_null($this->Flags)) {
            $this->Flags = new UserFlags($this->getId());
        }

        return $this->Flags;
    }

    /**
     * @return int
     */
    public function getId() {
        if ($this->id) {
            return $this->id;
        }

        $IdentificationService = new IdentificationService();
        return $IdentificationService->getStatId();
    }

    /**
     * Экспортирует данные
     * @return array
     */
    public function export() {
        return array(
            'id'           => $this->getId(),
            'last_request' => $this->last_request,
            'login_id'     => $this->login_id,
            'created'      => $this->created,
            'geo'          => $this->geo,
            'ref'          => $this->ref,
            'ref_domain'   => $this->ref_domain,
            'url'          => $this->url,
            'screen'       => $this->screen,
            'ip'           => $this->ip,
            'browser'      => $this->browser,
            'browser_version' => $this->browser_version,
            'os'           => $this->os,
            'data'         => $this->data,
            'utm'          => $this->utm,
            'first_login_time'  => $this->first_login_time,
            'last_login_time'   => $this->last_login_time,
            'username'   => $this->username,
            'email'   => $this->email,
            'sol_first_play_game'  => $this->sol_first_play_game,
            'sol_last_play_game'   => $this->sol_last_play_game,
            'utm_history'   => $this->utm_history,
        );
    }

    /**
     * Импортируем данные в модель
     * @param array $data
     */
    public function import(array $data) {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }

        if (array_key_exists('last_request', $data)) {
            $this->last_request = $data['last_request'];
        }

        if (array_key_exists('created', $data)) {
            $this->created = $data['created'];
        }

        if (array_key_exists('login_id', $data)) {
            $this->login_id = $data['login_id'];
        }

        if (array_key_exists('geo', $data)) {
            $this->geo = $data['geo'];
        }

        if (array_key_exists('ref', $data)) {
            $this->ref = $data['ref'];
        }

        if (array_key_exists('ref_domain', $data)) {
            $this->ref_domain = $data['ref_domain'];
        }

        if (array_key_exists('url', $data)) {
            $this->url = $data['url'];
        }

        if (array_key_exists('screen', $data)) {
            $this->screen = $data['screen'];
        }

        if (array_key_exists('browser', $data)) {
            $this->browser = $data['browser'];
        }

        if (array_key_exists('browser_version', $data)) {
            $this->browser_version = $data['browser_version'];
        }

        if (array_key_exists('os', $data)) {
            $this->os = $data['os'];
        }

        if (array_key_exists('ip', $data)) {
            $this->ip = $data['ip'];
        }

        if (array_key_exists('first_login_time', $data)) {
            $this->first_login_time = $data['first_login_time'];
        }

        if (array_key_exists('last_login_time', $data)) {
            $this->last_login_time = $data['last_login_time'];
        }

        if (array_key_exists('sol_first_play_game', $data)) {
            $this->sol_first_play_game = $data['sol_first_play_game'];
        }

        if (array_key_exists('sol_last_play_game', $data)) {
            $this->sol_last_play_game = $data['sol_last_play_game'];
        }

        if (array_key_exists('data', $data)) {
            $this->data = $data['data'];
        }

        if (array_key_exists('utm', $data) && $data['utm']) {
            $this->utm = $data['utm'];
        }

        if (array_key_exists('username', $data) && $data['username']) {
            $this->username = $data['username'];
        }

        if (array_key_exists('email', $data) && $data['email']) {
            $this->email = $data['email'];
        }

        if (array_key_exists('utm_history', $data) && $data['utm_history']) {
            $this->utm_history = $data['utm_history'];
        }
    }

    /**
     * Отдает данные по ключу
     * @param string $key
     * @return mixed
     */
    public function getDataByKey($key) {
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }

        return null;
    }
}