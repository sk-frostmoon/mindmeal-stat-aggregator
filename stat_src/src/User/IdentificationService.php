<?php
namespace User;

use Event\Handler\Ingame\Exception\UserNotAuthException;
use Storage\AbstractRedisStorage;

/**
 * Сервис идентификации юзера
 * @author akiselev
 */
class IdentificationService extends AbstractRedisStorage {
    /**
     * Соль для валидации id-ка
     */
    const SALT = 'N*(#@N@#N*(#J@*(@#FJ*(HJ@*#(RYU*(N@#J()J@C#)R(';

    /**
     * Возвращает идентификатор
     * Если в сессии его нету, то выдаем новый и пишем в сессию.
     * @param int $loginId
     * @param int $_statId
     * @param string $loginIdHash
     * @return int
     */
    public function getStatId($loginId = null, $_statId = null, $loginIdHash = '') {
        $Connection = $this->getConnection();
        $isFindUser = false;

        if (!is_null($_statId) && SessionUserStorage::getInstance()->getById($_statId)) {
            $statId = $_statId;
            $isFindUser = true;
        } else {
            $statId = $this->getCurrentSessionId();
        }

        if (!is_null($loginId) && $loginId && $this->isAuth($loginId, $loginIdHash)) {
            $statId = $Connection->executeRaw(array('GET', $this->makeKey($loginId)));
            if ($statId) {
                // Достали сессию по идентификатору юзера
                return $statId;
            } else {
                if ($isFindUser) {
                    $statId = $_statId;
                } else {
                    $statId = $this->getCurrentSessionId();
                }
            }

            // Не нашли в базе такого идентификатора, создаем связь:
            $Connection->executeRaw(array(
                'SET',  $this->makeKey($loginId), $statId,
            ));
            $Connection->executeRaw(array(
                'SET', $this->makeKey($statId), $loginId,
            ));
        }

        return $statId;
    }

    /**
     * Возвращает statId по loginId
     * @param int $loginId
     * @param string $loginIdHash
     * @return string|null
     * @throws UserNotAuthException
     */
    public function getStatIdByLoginId($loginId, $loginIdHash) {
        $Connection = $this->getConnection();

        if (!$this->isAuth($loginId, $loginIdHash)) {
            throw new UserNotAuthException();
        }

        $statId = $Connection->executeRaw(array('GET', $this->makeKey($loginId)));
        if ($statId) {
            return $statId;
        }

        return null;
    }

    /**
     * Возвращает текущий идентификатор сессииЫ
     * @return string
     */
    public function getCurrentSessionId() {
        return session_id();
    }

    /**
     * Создает ключ по id
     * @param int $realUserId
     * @return string
     */
    protected function makeKey($realUserId) {
        return 'user:session:' . $realUserId;
    }

    /**
     * Возвращает, действительно ли авторизован пользователь
     * @param int $loginId
     * @param string $loginIdHash
     * @return bool
     */
    public function isAuth($loginId, $loginIdHash) {
        return strcmp(sha1($loginId . static::SALT), $loginIdHash) === 0;
    }
}