<?php
namespace User;

/**
 * Интерфейс хранилища юзеров
 * @author akiselev
 */
interface UserStorageInterface {
    /**
     * Сохраняет юзера в сторадж
     * @param SessionUser $User
     */
    public function save(SessionUser $User);

    /**
     * Получает юзера по id
     * @param int $id
     * @return SessionUser|null
     */
    public function getById($id);
} 