<?php
namespace User;

/**
 * Хранилище сессионных юзеров
 * @author akiselev
 */
class SessionUserStorage {
    const REDIS_STORAGE    = 'redis';
    const MEMCACHE_STORAGE = 'memcache';

    /**
     * Текущее храналище
     */
    const CURRENT_STORAGE = self::REDIS_STORAGE;

    /**
     * @var SessionUserStorage
     */
    protected static $Instance = null;

    /**
     * Отдает инстанс фасада
     * @return UserStorageInterface
     */
    public static function getInstance() {
        if (is_null(self::$Instance)) {
            switch (static::CURRENT_STORAGE) {
                case static::REDIS_STORAGE:
                    self::$Instance = new SessionUserRedisStorage();
                break;

                case static::MEMCACHE_STORAGE:
                    self::$Instance = new SessionUserMemcacheStorage();
                    break;

                default:
                    self::$Instance = new SessionUserRedisStorage();
            }

        }

        return self::$Instance;
    }
} 