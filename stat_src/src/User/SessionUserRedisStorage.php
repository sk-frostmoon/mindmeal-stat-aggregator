<?php
namespace User;

use Exception;
use Storage\AbstractRedisStorage;

/**
 * Хранилище сессионных юзеров
 * @author akiselev
 */
class SessionUserRedisStorage extends AbstractRedisStorage implements UserStorageInterface {
    /**
     * Кэш юзеров
     * @var SessionUser[]
     */
    protected $UserCache = array();

    /**
     * Сохраняет юзера в сторадж
     * @param SessionUser $User
     */
    public function save(SessionUser $User) {
        $Connection = $this->getConnection();
        $key = $this->makeKey($User->getId());

        $userData = json_encode($User->export());
        if ($Connection->executeRaw(array('SET', $key, $userData))) {
            // Обновим кэш
            $this->UserCache[$User->getId()] = $User;
        }
    }

    /**
     * Получает юзера по id
     * @param int $id
     * @return SessionUser|null
     * @throws Exception
     */
    public function getById($id) {
        if (array_key_exists($id, $this->UserCache)) {
            // Нашли юзера в кэше
            return $this->UserCache[$id];
        }

        $Connection = $this->getConnection();
        $key = $this->makeKey($id);

        $userData = $Connection->executeRaw(array('GET', $key));
        if (!$userData) {
            return null;
        }
        $userData = json_decode($userData, true);
        $User = new SessionUser();
        $User->import($userData);

        $this->UserCache[$id] = $User;

        return $User;
    }

    /**
     * Строит ключ для хранилища
     * @param int $id
     * @return string
     */
    protected function makeKey($id) {
        return "user:data:{$id}";
    }
} 