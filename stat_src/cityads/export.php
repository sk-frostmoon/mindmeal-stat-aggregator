<?php


use Ad\Cityads\CityadsReportConverter;
use Ad\Cityads\CityadsRequestHandler;
use Statistics\Reader\StatisticsStorage;

require_once __DIR__ . '/../bootstrap/bootstrap.php';

header('Content-Type: text/xml');

try {
    $RequestHandler = new CityadsRequestHandler();
} catch (Exception $Ex) {
    echo '<?xml version="1.0"?><error>' . $Ex->getCode() . ' ' . $Ex->getMessage() . '</error>';
    return;
}

$request  = $RequestHandler->getRequest();
$dateFrom = date(DateTime::ISO8601, $request['xml']['date_from']);
$dateTo   = date(DateTime::ISO8601, $request['xml']['date_to']);


$E = new StatisticsStorage();

$query = <<<EOF
{
  "query": {
    "filtered": {
      "query": {
        "bool": {
          "should": [
            {
              "query_string": {
                "query": "@key:sol_level_up"
              }
            }
          ]
        }
      },
      "filter": {
        "bool": {
          "must": [
            {
              "terms": {
                "_type": [
                  "event"
                ]
              }
            },
            {
              "range": {
                "context_data.lvl_time": {
                  "from": "{$dateFrom}",
                  "to": "{$dateTo}"
                }
              }
            },
            {
              "fquery": {
                "query": {
                  "query_string": {
                    "query": "_type:event"
                  }
                },
                "_cache": true
              }
            }
          ]
        }
      }
    }
  },
  "size": 2500,
  "sort": [
    {
      "context_data.lvl_time": {
        "order": "asc",
        "ignore_unmapped": true
      }
    }
  ]
}
EOF;

$result = $E->queryExec($query);

if (isset($result['hits']) && isset($result['hits']['hits'])) {
    $usersTemp = $result['hits']['hits'];
} else {
    exit;
}


$users = [];
foreach ($usersTemp as $event) {
    $event = $event['_source'];

    if (!isset($event['user']['login_id'])) {
        continue;
    }

    $canWrite = false;
    if (isset($users[$event['user']['login_id']])) {
        if ($users[$event['user']['login_id']]['level'] < $event['context_data']['lvl']) {
            $canWrite = true;
        }
    } else {
        $canWrite = true;
    }

    if (!$canWrite) {
        continue;
    }

    $users[$event['user']['login_id']] = [
        'account_id' => $event['user']['login_id'],
        'char_id'  => $event['context_data']['char_id'],
        'level'    => $event['context_data']['lvl'],
        'lvl_time' => $event['context_data']['lvl_time'],
        'first_game_login_time' => $event['user']['sol_first_play_game'],
        'utm' => isset($event['user']['utm']['utm_click_id']) ? $event['user']['utm']['utm_click_id'] : 0,
    ];
}

$ReportConverter = new CityadsReportConverter($users);
echo $ReportConverter->toXML();