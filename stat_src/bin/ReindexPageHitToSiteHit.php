<?php
use Statistics\StatisticsElasticStorage;
use Statistics\StatisticsRedisStorage;

require_once __DIR__ . '/../bootstrap/bootstrap.php';

$Storage           = new StatisticsElasticStorage();
$StoragePersistent = new StatisticsRedisStorage();

$limit  = 1000;
$offset = 0;
$total  = 0;
$alreadyWrite = array();
$totalWrite = 0;

while (true) {
    $result = $StoragePersistent->get($limit, $offset);
    if (!$result) {
        break;
    }

    $total += sizeof($result);

    foreach ($result as $row) {
        $event = json_decode($row, true);

        if ($event['@timestamp'] > strtotime('2014-08-22T16:35:00+0400')) {
            // свежие записи не нужны
            continue;
        }

        if ($event['@key'] != 'page_hit_unique') {
            continue;
        }

        if (!isset($event['user'])) {
            continue;
        }

        if (!isset($event['user']['data']['site']) || $event['user']['data']['site'] != 'solgame') {
            // Нужен только solgame
            continue;
        }

        if (array_key_exists($event['user']['id'], $alreadyWrite)) {
            // Уже записали такого чувака
            continue;
        }

        if (isset($event['user']['data']['search'])) {
            unset($event['user']['data']['search']);
        }

        $event['@key'] = 'site_hit_unique';
        $alreadyWrite[$event['user']['id']] = 1;

        $totalWrite++;

        try {
            $Storage->write($event);
            $StoragePersistent->write($event);
        } catch (Exception $Ex) {
            var_export($event);
            var_export($Ex->getMessage());
            return;
        }
//        $Storage->appendBulk($event);
    }


//    $Storage->execBulk();

    $offset += $limit;

    echo "-----Total " . ($totalWrite) . " keys write\n";
    echo "Total " . ($total) . " keys reindex\n";
}