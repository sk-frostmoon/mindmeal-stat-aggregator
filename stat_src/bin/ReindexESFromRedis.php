<?php
use Statistics\StatisticsElasticStorage;
use Statistics\StatisticsRedisStorage;

require_once __DIR__ . '/../bootstrap/bootstrap.php';

$Storage           = new StatisticsElasticStorage();
$StoragePersistent = new StatisticsRedisStorage();

$limit  = 1000;
$offset = 0;
$total  = 0;

while (true) {
    $result = $StoragePersistent->get($limit, $offset);
    if (!$result) {
        break;
    }

    $total += sizeof($result);

    foreach ($result as $row) {
        $event = json_decode($row, true);

        if (isset($event['user']['data']['search'])) {
            unset($event['user']['data']['search']);
        }

        $Storage->appendBulk($event);
    }

    $Storage->execBulk();
    $offset += $limit;

    echo "Total " . ($total) . " keys reindex\n";
}