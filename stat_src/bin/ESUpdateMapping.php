<?php
use Statistics\StatisticsElasticStorage;
use Statistics\StatisticsRedisStorage;

require_once __DIR__ . '/../bootstrap/bootstrap.php';

$Storage = new StatisticsElasticStorage();

if (isset($argv[1]) && $argv[1] == 'dev') {
    $Storage->isDev = true;
}

$Storage->makeMapping();

