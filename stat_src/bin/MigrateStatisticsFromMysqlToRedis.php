<?php
use Statistics\StatisticsElasticStorage;
use Statistics\StatisticsFacade;
use Statistics\StatisticsMysqlStorage;
use Statistics\StatisticsRedisStorage;
use Utils\Time;

require_once __DIR__ . '/../bootstrap/bootstrap.php';

$StorageRedis = new StatisticsRedisStorage();
$StorageMysql = new StatisticsMysqlStorage();

$limit  = 1000;
$offset = 0;
$total  = 0;

while (true) {
    if (!($result = $StorageMysql->select($limit, $offset))) {
        break;
    }

    $total += sizeof($result);

    $eventsContext = array();
    foreach ($result as $row) {
        $context = json_decode($row['context'], true);

        if ($row['key']) {
            $context['@key'] = $row['key'];
        }

        if (!isset($context['@timestamp'])) {
            $context['@timestamp'] = strtotime($row['time'])  + Time::TIME_HOUR * 4;
        }

        $eventsContext[] = $context;
    }

    $StorageRedis->writeMany($eventsContext);

    $offset += $limit;

    echo "Total " . ($total) . " keys migrate\n";
}