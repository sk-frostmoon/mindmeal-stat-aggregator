<?php
use Statistics\StatisticsElasticStorage;
use Statistics\StatisticsRedisStorage;

require_once __DIR__ . '/../../bootstrap/bootstrap.php';

$Storage           = new StatisticsElasticStorage();
$StoragePersistent = new StatisticsRedisStorage();

$limit  = 1000;
$offset = 0;
$total  = 0;
$alreadyWrite = array();
$totalWrite = 0;

while (true) {
    $result = $StoragePersistent->get($limit, $offset);
    if (!$result) {
        break;
    }

    $total += sizeof($result);

    foreach ($result as $key => $row) {
        $event = json_decode($row, true);

        if (!isset($event['user']) || !isset($event['user']['data']) || !isset($event['user']['data']['landing_theme'])) {
            continue;
        }

        $event['user']['data']['landing_theme'] = array_merge(array(
            'withPasswdConfirmField' => false,
            'textRegisterOrJustDo'   => false,
            'btnRegisterOrPlayFree'  => false,
        ), $event['user']['data']['landing_theme']);


        if (isset($event['user']['data']['search'])) {
            unset($event['user']['data']['search']);
        }

        $totalWrite++;

//        var_export($event['user']['data']['landing_theme']);

        try {
            $StoragePersistent->lset($offset + $key, $event);
//            $Storage->write($event);
//            $StoragePersistent->write($event);
        } catch (Exception $Ex) {
            var_export($event);
            var_export($Ex->getMessage());
            return;
        }
//        $Storage->appendBulk($event);
    }


//    $Storage->execBulk();

    $offset += $limit;

    echo "-----Total " . ($totalWrite) . " keys write\n";
    echo "Total " . ($total) . " keys reindex\n";
}