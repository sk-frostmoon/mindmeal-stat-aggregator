<?php
use SOL\EventsGenerator\EventGeneratorRedisStorage;
use SOL\EventsGenerator\LevelUpEventGenerator;
use Statistics\StatisticsContext;
use Statistics\StatisticsFacade;
use Statistics\StatisticsKeys;
use User\IdentificationService;
use User\SessionUserStorage;
use User\UserFlags;

require_once __DIR__ . '/../../../bootstrap/bootstrap.php';

// Достать время последней генерации событий
$IdentificationService = new IdentificationService();
$UserFlags             = new UserFlags();
$Storage               = SessionUserStorage::getInstance();
$EventGeneratorStorage = new EventGeneratorRedisStorage();
$LevelUpEventGenerator = new LevelUpEventGenerator();

$lastGenerateReport = date('Y-m-d H:i:s', $EventGeneratorStorage->lastTime());
$users = $LevelUpEventGenerator->getUsers($lastGenerateReport);

$EventGeneratorStorage->setTime(time());

$totalWriteEvents = 0;

foreach ($users as $user) {
    $loginId     = $user['user_index'];
    $timestamp   = strtotime($user['char_updatestamp']);
    $loginIdHash = sha1($loginId . IdentificationService::SALT);

    $statId = $IdentificationService->getStatId($loginId, null, $loginIdHash);
    $User   = $Storage->getById($statId);

    if (!$User) {
        // Странно, что не нашли такого юзера. Хотя, может быть и не странно.
        // Возможно, что у него был отключен JS или он был зарегистрирован до создания сервиса статистики
        $User = new \User\SessionUser();
        $User->created             = time();
        $User->last_request        = time();
        $User->login_id            = $loginId;
        $User->sol_first_play_game = strtotime($user['char_datestamp']);
        $User->sol_last_play_game  = $timestamp;

        session_destroy();
        session_start();
        $Storage->save($User);
    } elseif (!$User->sol_first_play_game) {
        $User->sol_first_play_game = strtotime($user['char_datestamp']);
        $User->sol_last_play_game  = $timestamp;
        $Storage->save($User);
    }

    $flag = UserFlags::FLAG_SOL_LEVEL_UP . '_' . $user['index'] . '_' . $user['char_level'];
    if (!$UserFlags->isChecked($flag, $User->id)) {
        // Пишем стату
        StatisticsFacade::write(StatisticsKeys::KEY_SOL_LEVEL_UP, [
            StatisticsContext::USER    => $User,
            StatisticsContext::CONTEXT => [
                'char_id'      => $user['index'],
                'lvl'          => $user['char_level'],
                'lvl_up_time'  => $user['char_leveluptime'],
                'lvl_time'     => date(DateTime::ISO8601, $timestamp - $user['char_leveluptime']),
            ],
        ]);

        $UserFlags->setFlag($flag, $User->id);
        $totalWriteEvents++;
    }
}

echo "Total write events: ", $totalWriteEvents, PHP_EOL;
echo sizeof($users), PHP_EOL;