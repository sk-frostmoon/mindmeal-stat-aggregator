<?php
require_once __DIR__ . '/../../bootstrap/bootstrap.php';

use SOL\Report\ReportBuilder;
use SOL\Report\ReportRedisStorage;
use Statistics\StatisticsContext;
use Statistics\StatisticsFacade;
use Statistics\StatisticsKeys;
use User\IdentificationService;
use User\SessionUserStorage;
use User\UserFlags;

$ReportStorage         = new ReportRedisStorage();
$ReportBuilder         = new ReportBuilder();
$IdentificationService = new IdentificationService();
$UserFlags             = new UserFlags();
$Storage               = SessionUserStorage::getInstance();

$lastGeneratedTime = $ReportStorage->getLastGeneratedTime();
$chars = $ReportBuilder->getCharacters($lastGeneratedTime);
$ReportStorage->setLastGeneratedTime(strtotime("2014-11-08 22:00:00"));

$users = [];

foreach ($chars as $char) {
    // Генерируем события для статистики
    $loginId = $char['user_index'];
    if (!isset($users[$loginId])) {
        $users[$loginId] = $char;
    } else {
        if (strtotime($users[$loginId]['char_datestamp']) > strtotime($char['char_datestamp'])) {
            $users[$loginId] = $char;
        }
    }
}

$total_ = 0;

foreach ($users as $user) {
    $loginId = $user['user_index'];
    $loginIdHash = sha1($loginId . IdentificationService::SALT);
    $statId = $IdentificationService->getStatId($loginId, null, $loginIdHash);
    $User   = $Storage->getById($statId);

    if (!$User) {
        // Странно, что не нашли такого юзера. Хотя, может быть и не странно.
        // Возможно, что у него был отключен JS или он был зарегистрирован до создания сервиса статистики
        $User = new \User\SessionUser();
        $User->created             = time();
        $User->last_request        = time();
        $User->login_id            = $loginId;

        session_destroy();
        session_start();

        $Storage->save($User);
        $IdentificationService->getStatId($loginId, null, $loginIdHash);
    } elseif ($User->sol_first_play_game) {
        continue;
    }

    if (!$User->login_id) {
        $User->login_id = $loginId;
        $Storage->save($User);
        $IdentificationService->getStatId($loginId, null, $loginIdHash);
    }

    $User->sol_first_play_game = strtotime($user['char_datestamp']);
    $User->sol_last_play_game  = strtotime($user['char_updatestamp']);
    $Storage->save($User);

    // Событие первого входа в игру
    if (!$UserFlags->isChecked(UserFlags::FLAG_SOL_FIRST_PLAY_GAME, $User->login_id)) {
        $total_ ++;

        StatisticsFacade::write(StatisticsKeys::KEY_SOL_FIRST_GAME_ENTER, array(
            StatisticsContext::USER      => $User,
            StatisticsContext::TIMESTAMP => $User->sol_first_play_game,
        ));

        $UserFlags->setFlag(UserFlags::FLAG_SOL_FIRST_PLAY_GAME, $User->id);
    }
}

$size = sizeof($chars);
echo "Find {$size} new chars \n";
echo "Write {$total_} events \n";

//echo "Start build report\n";
//$ReportBuilder->build();
//echo "End build report\n";

