<?php
use Statistics\StatisticsElasticStorage;
use Statistics\StatisticsMysqlStorage;
use Utils\Time;

require_once __DIR__ . '/../bootstrap/bootstrap.php';

$Storage           = new StatisticsElasticStorage();
$StoragePersistent = new StatisticsMysqlStorage();

$limit  = 1000;
$offset = 0;
$total  = 0;

while (true) {
    if (!($result = $StoragePersistent->select($limit, $offset))) {
        break;
    }

    $total += sizeof($result);

    foreach ($result as $row) {
        $context = json_decode($row['context'], true);

        if ($row['key']) {
            $context['@key'] = $row['key'];
        }

        echo $row['time'] . "\n";
        $context['@timestamp'] = strtotime($row['time']) + Time::TIME_HOUR * 4;
        $Storage->appendBulk($context);
    }

    $Storage->execBulk();
    $offset += $limit;

    echo "Total " . ($total) . " keys reindex\n";
}