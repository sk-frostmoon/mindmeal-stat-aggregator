<?php
define('PATH_ROOT', __DIR__ . '/..');
define('PATH_SRC', PATH_ROOT . '/src');
define('PATH_JS', PATH_ROOT . '/js');
define('PATH_CONFIG', PATH_ROOT . '/config');
define('PATH_LOGS', PATH_ROOT . '/log');
