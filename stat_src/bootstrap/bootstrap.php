<?php
require_once __DIR__ . '/path.php';

ini_set('session.gc_maxlifetime', 51840000);
ini_set('session.cookie_lifetime', 51840000);
$save_handler = ini_set('session.save_handler', 'redis');
$save_path = ini_set('session.save_path', 'tcp://127.0.0.1:6379');

session_start();

error_reporting(-1);
ini_set('display_errors', 'On');

header('Content-Type: text/html; charset=utf-8');

require_once PATH_ROOT . '/../vendor/autoload.php';
