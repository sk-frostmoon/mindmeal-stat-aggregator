var mmServer = {
    getXmlHttp: function () {
        var xmlhttp;
        try {
            xmlhttp = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (e) {
                xmlhttp = false;
            }
        }

        if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
            xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
    },
    sendRequest: function(url, data, callback) {
        try {
            var xmlhttp = mmServer.getXmlHttp();
            if (xmlhttp) {
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == xmlhttp.DONE) {
                        if (callback) {
                            callback(xmlhttp.responseText);
                        }
                    }
                };

                xmlhttp.open("POST", url);
                xmlhttp.send(JSON.stringify(data));
            }
        } catch (e) {
            new_mmStatOverview._error(e.toString());
        }
    }
};

var mmCookie = {
    setCookie: function (name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires == "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires*1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for(var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie + '; mmStatId=' + user.id + '; path=/;';
    },

    getCookie: function (name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));

        return matches ? decodeURIComponent(matches[1]) : undefined
    },

    deleteCookie: function(name) {
        mmCookie.setCookie(name, null, { expires: -1 })
    }
};



/**
 * Обработчик событий с сервера
 * Действия приходят в куки: push, writeStatistics, writeUniqueStatistics, writeStatisticsWithUnique
 * В формате:
 * [
 *   {
 *      action_1: {a: 1, b:2}
 *   },
 *   {
 *      action_2: {a: 1, b:2}
 *   },
 * ]
 *
 * todo отправлять не несколько запросов, а все события в одном
 */
var mmEventHandler = {
    // Проверка событий и вызов
    fire: function () {
        var events = {
            stat_push: new_mmStatOverview.push,
            stat_writeStatistics: new_mmStatOverview.push,
            stat_writeUniqueStatistics: new_mmStatOverview.push,
            stat_writeStatisticsWithUnique: new_mmStatOverview.push
        };

        var eventData;
        // Все типы событий
        for (var event in events) {
            eventData = mmEventHandler.getEvents(event);

            if (eventData === null) {
                continue;
            }

            // Все события конкретного типа
            for (var eventFromData in eventData) {
                for (var key in eventData[eventFromData]) {
                    events[event](key, eventData[eventFromData][key]);
                }
            }

            mmCookie.deleteCookie(event);
        }
    },

    // Получает JSON объект событий
    getEvents: function(eventName) {
        var cookie = mmCookie.getCookie(eventName);

        if (cookie) {
            return JSON.parse(cookie);
        }

        return null;
    },

    // Добавляет событие
    applyEvent: function(eventName, event, data) {
        var eventData = mmEventHandler.getEvents(eventName);
        if (eventData === null) {
            eventData = [];
        }

        var obj = {};
        obj[event] = data;
        eventData.push(obj);

        mmCookie.setCookie(eventName, JSON.stringify(eventData));
    }
};

var new_mmStatOverview = {
    _log:   function (text) { if (console && console.log)   { console.log('new_mmStatOverview[log] ' + text); } },
    _error: function (text) { if (console && console.error) { console.error('new_mmStatOverview[error] ' + text); } },

    getReferer: function () {
        return ((typeof document.referrer) == 'undefined') ? 'none' : document.referrer;
    },

    getScreenResolution: function () {
        return {
            width: window.screen.width,
            height: window.screen.height
        };
    },

    getUtmValues: function () {
        // Берем из кук
        var ccs = document.cookie.split(/;[ ]+/);
        var utmValues = {};
        for (var i=0; i<ccs.length; i++) {
            if (/^utm_.*/.test(ccs[i])) {
                var equalIdx = ccs[i].indexOf('=');
                var name  = ccs[i].substring(0, equalIdx);
                var value = ccs[i].substring(equalIdx+1);
                utmValues[name] = value;
            }
        }
        return utmValues;
    },

    saveUtmFromHrefToCookie: function() {
        var parser = document.createElement('a');
        parser.href = location.href;

        var search = parser.search.substring(1, parser.search.length).split('&');

        for (var token in search) {
            token = search[token].split('=');


            if (token[0] == 'click_id' && token[1]) {
                token[0] = 'utm_click_id';
            }

            if (token[1] && /^utm_.*/.test(token[0])) {
                mmCookie.setCookie(token[0], token[1]);
            }
        }
    },

    push: function (action, params, callback) {
//        var url  = 'http://statistics.local/stat_src/event.php';
        var url  = 'https://stat.mindmeal.ru/stat_src/event.php';

        // Вытаскиваем данные статистики для конкретной странички
        if (window._get_stat_init_params !== undefined) {
            var init_params = window._get_stat_init_params();

            for (var i in init_params) {
                params[i] = init_params[i];
            }
        }

        var login_id = 0, login_hash = '';
        if (window._get_login_id !== undefined) {
            login_id   = window._get_login_id()[0];
            login_hash = window._get_login_id()[1];
        }

        var data = {};
        data['action'] = action;
        data['data']   = {};
        data['data']['params']  = params;
        data['data']['loginId'] = login_id;
        data['data']['loginHash'] = login_hash;
        data['data']['utm']     = new_mmStatOverview.getUtmValues();
        data['data']['ref']     = new_mmStatOverview.getReferer();
        data['data']['screen']  = new_mmStatOverview.getScreenResolution();
        data['data']['url']     = document.URL;
        data['data']['userId']  = user.id;

        mmServer.sendRequest(url, data, callback);
    },

    /**
     * Пишет статиситку
     * @param statisticKey
     * @param params
     * @param callback
     */
    writeStatistics: function(statisticKey, params, callback) {
        params['key'] = statisticKey;
        new_mmStatOverview.push('write_statistics', params, callback);
    },

    /**
     * Пишет только уникальную статистику
     * @param statisticKey
     * @param params
     * @param callback
     */
    writeUniqueStatistics: function(statisticKey, params, callback) {
        params['key_unique'] = statisticKey;
        new_mmStatOverview.push('write_unique_statistics', params, callback);
    },

    /**
     * Пишет "обычную" и уникальную статистику
     * @param statisticKey
     * @param params
     * @param callback
     */
    writeStatisticsWithUnique: function(statisticKey, params, callback) {
        params['key'] = statisticKey;
        params['key_unique'] = statisticKey + '_unique';
        new_mmStatOverview.push('write_unique_statistics', params, callback);
    },

    /**
     * Просмотр страницы.
     * Действие по умолчанию.
     */
    viewPage: function() {
        var parser = document.createElement('a');
        parser.href = location.href;

        var params = {};
        params.host = parser.hostname;
        params.path = parser.pathname;
        params.search = parser.search;

        new_mmStatOverview.push('view_page', params, function(response){});
    },

    /**
     * Инициализация статистики
     */
    init: function() {
        if (!_new_mm.canInit) {
            _new_mm.canInit = true;
            return;
        }

        // Преребросим utm_* метки в куки
        new_mmStatOverview.saveUtmFromHrefToCookie();

        // Отправим стату просмотра страниц
        new_mmStatOverview.viewPage();

        // Выполним запланированные действия
        mmEventHandler.fire();
    }
};

var _new_mm = {
    canInit: (window._stat_can_init === undefined) ? true : window._stat_can_init,

    init: new_mmStatOverview.init,
    push: new_mmStatOverview.push, // 1
    writeStatistics: new_mmStatOverview.writeStatistics, // 2
    writeUniqueStatistics: new_mmStatOverview.writeUniqueStatistics, // 3
    writeStatisticsWithUnique: new_mmStatOverview.writeStatisticsWithUnique, // 4

    fireEvents: mmEventHandler.fire
};

// Инициализируем объект статистики
_new_mm.init();
