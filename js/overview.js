var mmStatOverview = {
    _log:   function (text) { if (console && console.log)   { console.log('new_mmStatOverview[log] ' + text); } },
    _trace: function (text) { if (console && console.trace) { console.trace('new_mmStatOverview[trace] ' + text); } },
    _error: function (text) { if (console && console.error) { console.error('new_mmStatOverview[error] ' + text); } },

    getReferer: function () {
        return ((typeof document.referrer) == 'undefined') ? 'none' : document.referrer;
    },

    getXmlHttp: function () {
        var xmlhttp;
        try {
            xmlhttp = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (e) {
                xmlhttp = false;
            }
        }

        if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
            xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
    },

    screenResolution: function () {
        try {
            var s = window.screen;
            var resol = s.width + "x" + s.height;
            xmlhttp = mmStatOverview.getXmlHttp();
            if (xmlhttp) {
                xmlhttp.open("GET", "https://stat.mindmeal.ru/overview");
                xmlhttp.setRequestHeader('X-mmStat-Referer', mmStatOverview.getReferer());
                xmlhttp.setRequestHeader('X-mmStat-Resolution', resol);
                /*
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == xmlhttp.DONE) {
                        //console.log(xmlhttp);
                    }
                };
                */
                xmlhttp.send(null);
            }
        } catch (e) {
            mmStatOverview._error(e.toString());
        }
    },

    getUtmValues: function () {
        var ccs = document.cookie.split(/;[ ]+/);
        var utmValues = {};
        for (var i=0; i<ccs.length; i++) {
            if (/^utm_.*/.test(ccs[i])) {
                var equalIdx = ccs[i].indexOf('=');
                var name  = ccs[i].substring(0, equalIdx);
                var value = ccs[i].substring(equalIdx+1);
                utmValues[name] = value;
            }
        }
        return utmValues;
    },

    push: function (action, params) {
        try {
            var data = mmStatOverview.getUtmValues();
            data['action'] = action;
            data['data']   = params;
            var xmlhttp = mmStatOverview.getXmlHttp();
            if (xmlhttp) {
                xmlhttp.open("POST", "https://stat.mindmeal.ru/p/");
                xmlhttp.send(JSON.stringify(data));
            }
        } catch (e) {
            mmStatOverview._error(e.toString());
        }
    },

    init: function () {
        mmStatOverview.screenResolution();
    },
};

var _mm = {
    push: mmStatOverview.push
};

// Shortcall for push
mmStatOverview.init();
