/**
 * @owner       Mind Meal LLC
 * @author      Alexander Kondratev
 * @authorSite  http://theg4sh.ru
 **/

/*${companyId}*/
var rndName = 'mmStatAdv' + (('' + Math.random()).substr(2));
document.currentScript.setAttribute('data-name', rndName);
document.currentScript.mmStatAdv = {
    id: null,
    version: null,
    type: null,
    companyId: null,
//    statWebRoot: 'http://stat.mindmeal.ru',
    statWebRoot: 'http://statistics.local',
    scriptName: '/js/stat.js',
    scriptVersion: '0',
    scriptTag: null,
    hasListener: false,

    _log:   function (text) { if (console && console.log)   { console.log('mmStatAdv[log] ', text); } },
    _trace: function (text) { if (console && console.trace) { console.trace('mmStatAdv[trace] ', text); } },
    _error: function (text) { if (console && console.error) { console.error('mmStatAdv[error] ', text); } },

    checkVisible: function () {
        var element = document.getElementById(this.id).children[0];
        if ((!element) || element.offsetWidth === 0 || element.offsetHeight === 0) return false;
        var height = document.documentElement.clientHeight,
            rects = element.getClientRects(),
            on_top = function(r) {
                var x = (r.left + r.right)/2, y = (r.top + r.bottom)/2;
                return (document.elementFromPoint(x, y) === element);
            };
        for (var i = 0, l = rects.length; i < l; i++) {
            var r = rects[i],
                in_viewport = r.top > 0 ? r.top <= height : (r.bottom > 0 && r.bottom <= height);
            //if (in_viewport && on_top(r)) return true;
            if (on_top(r)) return true;
        }
        return false;
    },

    bind: function (eventName, obj, listener) {
        if (obj.addEventListener) {
            obj.addEventListener(eventName, listener, false);
            return true;
        }
        if (obj.attachEvent) {
            obj.attachEvent('on'+eventName, listener, false);
            return true;
        }

        return true;
    },

    getCompanyId: function () {
        var scripts = document.getElementsByTagName('script');
        if (document.currentScript) {
            this.scriptTag = document.currentScript;
            return this.scriptTag.src.replace(/^.*\?/, '');
        }
        for( var i in scripts ) {
            if (/^[0-9]+$/.test(i)) {
                if (scripts[i].src && (scripts[i].src.indexOf(this.statWebRoot + this.scriptName) == 0)) {
                    this.scriptTag = scripts[i];
                    return this.scriptTag.src.replace(/^.*\?/, '');
                }
            }
        }

        return false;
    },

    getXmlHttp: function () {
        var xmlhttp;
        try {
            xmlhttp = new ActiveXObject('Msxml2.XMLHTTP');
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (e) {
                xmlhttp = false;
            }
        }

        if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
            xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
    },

    sendViewed: function () {
        var self = this.mmStatAdv;

        window.setTimeout(function () {
            var xmlhttp = self.getXmlHttp();
            if (xmlhttp != false) {
                if (!self.checkVisible()) {
                    self._error('Invisible element');
                    return;
                }
                var url = self.statWebRoot + '/v/' + self.companyId;

                try {
                    xmlhttp.open("GET", url + '?r=' + Math.random(), true);
                    xmlhttp.setRequestHeader('X-mmStat-Referer', ((typeof document.referrer) == 'undefined') ? "none" : document.referrer);
                    xmlhttp.onreadystatechange = function () { if (xmlhttp.readyState == xmlhttp.DONE) { return; } };
                    xmlhttp.send(null);
                } catch (e) {
                    self._error('XMLHttRequest has status ' + xmlhttp.status);
                    console.log(e);
                }
            } else {
                self._error('XMLHttRequest not initialized');
            }
        }, 1000);
    },

    render: {
        'v0': {
        },
    },


    init: function () {
        var self = this;
        if (document.currentScript) {
            this.statWebRoot = document.currentScript.src.replace(/^(https?:\/\/[^\/]+)\/.*/, '$1');
        }

        var id = 'mmStatAdvLink' + (''+Math.random()).substr(2);
        var companyId = this.getCompanyId();
        this.id = id;
        this.companyId = companyId;
        if (companyId === false) {
            this._error('Company id not found!');
            return;
        }
        this.version = 'v' + this.companyId.substr(0,1);
        this.type    = 't' + this.companyId.substr(1,1);


        try {
            var xmlhttp = this.getXmlHttp();
            var url     = this.statWebRoot + '/js/stat.'+this.version+'.json';
            xmlhttp.open("GET", url + '?r=' + Math.random(), true);
            //xmlhttp.setRequestHeader('X-mmStat-Referer', ((typeof document.referrer) == 'undefined') ? "none" : document.referrer);
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == xmlhttp.DONE) {
                    if (xmlhttp.status == 200) {
                        var code = eval(xmlhttp.responseText);
                        for(var t in code) {
                            if (/^t[a-zA-Z0-9]$/.test(t) && (typeof(code[t]) == 'function')) {
                                self.render[self.version][t] = code[t];
                            }
                        }
                    }
                    self.initContinue();
                }
            };
            xmlhttp.send(null);
        } catch (e) {
            this._error(e.toString());
            this.initContinue();
        }
    },

    initContinue: function () {
        try {
            if (/^.+$/.test(this.version) && /^.+/.test(this.type)) {
                var renderMethod = this.render[this.version][this.type];
                if (typeof (renderMethod) == 'function') {
                    renderMethod(this);
                }
            } else {
                window.mm = this;
        } catch (e) {
            this._error(e.toString());
        }
    },
};

// Test git environments
document.currentScript.mmStatAdv.init();
