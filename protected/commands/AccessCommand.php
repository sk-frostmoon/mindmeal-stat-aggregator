<?php 

class AccessCommand extends CConsoleCommand
{
    public function actionFlushAuth()
    {
        $db = Yii::app()->db;
        $db->createCommand("DELETE FROM AuthItem")->execute();
        $db->createCommand("DELETE FROM AuthItemChild")->execute();
        $db->createCommand("DELETE FROM AuthAssignment")->execute();
        $db->createCommand("DELETE FROM users")->execute();
    }

    public function actionAddRules()
    {
        $auth = Yii::app()->authManager;

        // Atom operations for Campaigns
        $auth->createOperation('addCampaign', 'Add new campaign');
        $auth->createOperation('viewCampaign', 'View campaign');
        $auth->createOperation('updateCampaign', 'Update campaign');
        //$auth->createOperation('deleteCampaign', 'Delete campaign');
        $auth->createOperation('viewCampaignList', 'View all campaigns');

        // Atom oprations for Users
        $auth->createOperation('addUser', 'Create new users');
        $auth->createOperation('viewUser', 'View user');
        $auth->createOperation('updateUser', 'Update user');
        $auth->createOperation('deleteUser', 'Delete user');
        $auth->createOperation('viewUserList', 'View all users');

        $auth->createOperation('viewDevelopersParts', 'Developers parts view');


        $role=$auth->createRole('user', 'User can view and detail view of him accessed campaign');
        $role->addChild('viewCampaign');
        $role->addChild('viewCampaignList');

        $role=$auth->createRole('moderator', 'User can view, detail view and update accessed campaigns');
        $role->addChild('user');
        $role->addChild('updateCampaign');


        $role=$auth->createRole('admin');
        $role->addChild('moderator');
        // User actions
        $role->addChild('addUser');
        $role->addChild('viewUser');
        $role->addChild('updateUser');
        $role->addChild('deleteUser');
        // Campaigns actions
        $role->addChild('addCampaign');
        //$role->addChild('viewCampaign');
        //$role->addChild('updateCampaign');
        //$role->addChild('viewCampaignList');

        // Developer role
        $role=$auth->createRole('developer');
        $role->addChild('viewDevelopersParts');
    }

    public function actionAddAdminUser()
    {
        $auth = Yii::app()->authManager;

        $user = new User();
        $user->username = 'villicus'; // @ak lat. Управитель имения
        $user->password = 'sucilliv';
        $user->confirmPassword = $user->password;
        $user->email    = 'admin@stat.mindmeal.ru';
        $user->activated = 1;
        if (!$user->save()) {
            echo "Возникли ошибки при создании пользователя:\n";
            var_dump($user->getErrors());
        }
        $user->refresh();
        $auth->assign('admin', $user->id);

        echo "You can login as \n";
        echo "    Username:  villicus\n";
        echo "    Password:  sucilliv\n";
    }

    public function actionAddTestUsers()
    {
        $user = new User();
        $user->username = 'ak'; // @ak lat. Управитель имения
        $user->password = 'm1Ndm3@l';
        $user->email    = 'ak@mindmeal.ru';
        $user->save();
        $auth->assign('developer', $user->id);

        $user = new User();
        $user->username = 'moderator'; // @ak lat. Управитель имения
        $user->password = 'moderator';
        $user->email    = 'moderator.test@stat.mindmeal.ru';
        $user->save();
        $auth->assign('moderator', $user->id, User::ASSIGNMENT_BIZRULE);

        $user = new User();
        $user->username = 'user'; // @ak lat. Управитель имения
        $user->password = 'user';
        $user->email    = 'user.test@stat.mindmeal.ru';
        $user->save();
        $auth->assign('user', $user->id, User::ASSIGNMENT_BIZRULE);
    }
}
