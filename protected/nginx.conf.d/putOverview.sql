DROP PROCEDURE IF EXISTS putOverview;
    
DELIMITER $$
CREATE PROCEDURE putOverview(host CHAR(128), uri CHAR(255), ip CHAR(15), referer CHAR(255), resolution CHAR(128), os CHAR(128), browser TEXT(512), country_code CHAR(128), country_name CHAR(128), city CHAR(128))
BEGIN
    DECLARE screen_id INT(11);
    DECLARE os_id INT(11);
    DECLARE browser_id INT(11);
    DECLARE geo_id INT(11);

    SELECT id INTO geo_id FROM ov_geo ovg WHERE ovg.country_code = country_code AND ovg.city_name = city LIMIT 1;
    IF geo_id IS NULL THEN
        INSERT INTO ov_geo (`country_code`, `country_name`, `city_name`, `count`) VALUES (country_code, country_name, city, 1);
        SET geo_id = LAST_INSERT_ID();
    ELSE
        UPDATE ov_geo SET count = count+1 WHERE id = geo_id;
    END IF;

    -- Save screen resolution and get the screen_id
    SELECT id INTO screen_id FROM ov_screen ovs WHERE ovs.screen = resolution LIMIT 1;
    IF screen_id IS NULL THEN
        INSERT INTO ov_screen (`screen`, `count`) VALUES (resolution, 1);
        SET screen_id = LAST_INSERT_ID();
    ELSE
        UPDATE ov_screen SET count = count+1 WHERE id = screen_id;
    END IF;

    -- Save user os 
    SELECT id INTO os_id FROM ov_os ovo WHERE ovo.os = os LIMIT 1;
    IF os_id IS NULL THEN
        INSERT INTO ov_os (`os`, `count`) VALUES (os, 1);
        SET os_id = LAST_INSERT_ID();
    ELSE
        UPDATE ov_os SET count = count+1 WHERE id = os_id;
    END IF;

    -- Save user os 
    SELECT id INTO browser_id FROM ov_browser ovb WHERE ovb.browser = browser LIMIT 1;
    IF browser_id IS NULL THEN
        INSERT INTO ov_browser (`browser`, `count`) VALUES (browser, 1);
        SET browser_id = LAST_INSERT_ID();
    ELSE
        UPDATE ov_browser SET count = count+1 WHERE id = browser_id;
    END IF;

    INSERT INTO views (
            `user_id`, `time`, `host`, `uri`, `referer`,
            `ip`, `geo_id`, `browser_id`, `screen_id`, `os_id`
    )
    VALUES (
            NULL, UNIX_TIMESTAMP(), host, uri, referer,
            INET_ATON(ip), geo_id, browser_id, screen_id, os_id
    );
END$$
DELIMITER ;
