package mmStat;

use base;
use nginx;
use DBI;
use Parse::HTTP::UserAgent;
use Geo::IP;

our $debug = 0;
our $connect       = DBI->connect("dbi:mysql:database=mm_stat;host=stat.mindmeal.ru;port=3306", "mm_stat", "ssx5i5uJ9IG7") || return "";
our $campaignSth   = $connect->prepare("SELECT id as company_id, target_url FROM adv_campaign WHERE code = ? LIMIT 1");
our $advViewedSth  = $connect->prepare("INSERT INTO adv_views (user_id, time, host, uri, referer, campaign_id, ip) VALUES (NULL, UNIX_TIMESTAMP(), ?, ?, ?, ?, INET_ATON(?))");
our $advClickedSth = $connect->prepare("INSERT INTO adv_click (user_id, time, host, uri, campaign_id, ip) VALUES (NULL, UNIX_TIMESTAMP(), ?, ?, ?, INET_ATON(?))");
our $overviewSth   = $connect->prepare("CALL putOverview(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
our $gi = Geo::IP->open("/usr/share/GeoIP/GeoIPCity.dat", GEOIP_STANDARD);


#
# Do not call this function from nginx
#
sub getCampaignByCode {
	my $hash = shift;	

	# ATTENSION: Check the hash outter this function
    my @values = (
        $hash
    );
    my $campagnId = undef;
    my $targetUrl = undef;
	my %campaign = (
		id => undef,
		targetUrl => undef,
	);
    $campaignSth->execute(@values) || return "";
    if ($campaignSth->rows > 0) {
        my @row = $campaignSth->fetchrow_array;

		$campaign{id}        = @row[0];
		$campaign{targetUrl} = @row[1];
    }
	
	return %campaign;
}

sub getReferer {
	my $r = shift;

	my %referer = (
		host => "none",
		uri  => "none"
	);
    my $refererUrl = $r->variable('http_referer');
    $r->header_out("X-Debug-Referer", $referer) if ($debug == 1);
    if ($refererUrl) {
		$referer{host} = $refererUrl;
		$referer{uri}  = $refererUrl;
		$referer{host} =~ s/^(http:\/\/)?([^\/]+).*$/\1\2/ui;
		$referer{uri}  =~ s/^(http:\/\/)?[^\/]+(.*)$/\2/ui;
    }

	return %referer;
}

#
# Function allow send to us headers like X-Custom-Header
# https://developer.mozilla.org/ru/docs/HTTP/Access_control_CORS
#
sub allowCustomHeaders {
	my $r = shift;
    $r->header_out('Access-Control-Allow-Origin', '*');
    $r->header_out('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
    $r->header_out('Access-Control-Allow-Headers', 'X-mmStat-Referer, X-mmStat-Resolution');
    $r->header_out('Access-Control-Max-Age', 1728000);
	if ($r->request_method =~ m/^OPTIONS$/) {
		$r->status(200);
		$r->header_out("X-Debug-Options", "true") if ($debug == 1);
		# Here we can allow to access deny to any hosts :^)
		#my $origin = $r->header_in('Origin');
		$r->send_http_header('text/plain');
		return 1;
	}
	return 0;
}

sub avdRedirectUrl {
    my $r = shift;
    my $hash = $r->uri;
       $hash =~ s/^\/q\/([0-9a-zA-Z]+).*$/\1/i;

    $r->header_out("X-Debug-mmStat", "true") if ($debug == 1);
   	$r->header_out("Cache-Control", "no-cache; must-revalidate");
    $r->header_out("Content-Length", "0");

	if (allowCustomHeaders($r)) {
		return OK;
	}

    if ($hash =~ m/{$r->uri}/)
	{
		$r->status(400);
        $r->header_out("X-Debug-Match", "$hash - " . $r->uri) if ($debug == 1);
        $r->send_http_header('text/html');
        return OK;
    }

	my %campaign = getCampaignByCode($hash);
	if ($campaign{id} == undef) {
		$r->status(404);
        $r->header_out("X-Debug-Campaign", "not-found") if ($debug == 1);
        $r->send_http_header('text/html');
        return OK;
	}

	my %referer = getReferer($r);

    my @values = (
        #NULL,
        #"UNIX_TIMESTAMP()",
         $referer{host},
         $referer{uri},
         $campaign{id},
         $r->remote_addr
    );
    $r->header_out("X-Debug-Args", join(", ", @values)) if ($debug == 1);
    $res = $advClickedSth->execute(@values);

    my $targetUrl = $campaign{targetUrl};
    if ($targetUrl =~ m/\?/) {
        $targetUrl = $targetUrl . '&utm_code=' . $hash;
    } else {
        $targetUrl = $targetUrl . '?utm_code=' . $hash;
    }

	$r->status(307);
    $r->header_out("X-Debug-Result", "200") if ($debug == 1);
    $r->header_out("Location", $targetUrl);
    $r->send_http_header('text/html');
    return OK;
}

sub advViewed {
    my $r = shift;
    my $hash = $r->uri;
       $hash =~ s/^\/v\/([0-9a-zA-Z]+).*$/\1/i;

    $r->header_out("X-Debug-mmStat", "true") if ($debug == 1);
   	$r->header_out("Cache-Control", "no-cache; must-revalidate");
    $r->header_out("Content-Length", "0");

	if (allowCustomHeaders($r)){
		return OK;
	}

    if ($hash =~ m/{$r->uri}/)
	{
		$r->status(400);
        $r->header_out("X-Debug-Match", "$hash - " . $r->uri) if ($debug == 1);
        $r->send_http_header('text/plain');
        return OK;
    }

	my %referer = getReferer($r);
    my $refererUrl = $r->header_in('X-mmStat-Referer');
	if (!$refererUrl) {
		$refererUrl = "none";
	}


	my %campaign = getCampaignByCode($hash);
	if ($campaign{id} == undef) {
		$r->status(404);
        $r->header_out("X-Debug-Campaign", "not-found") if ($debug == 1);
        $r->send_http_header('text/plain');
        return OK;
	}

    my @values = (
        #NULL,
        #"UNIX_TIMESTAMP()",
         $referer{host},
         $referer{uri},
         $refererUrl,
         $campaign{id},
         $r->remote_addr
    );
    $r->header_out("X-Debug-Args", join(", ", @values)) if ($debug == 1);
    $res = $advViewedSth->execute(@values);

	$r->status(200);
    $r->header_out("X-Debug-Result", "200") if ($debug == 1);
    $r->send_http_header('text/plain');
    return OK;
}

sub statActivity {
	my $r = shift;
	$r->header_out("X-Debug-mmStat", "true") if ($debug == 1);

	if (allowCustomHeaders($r)){
		return OK;
	}

	my %additional = (
		referer    => $r->header_in('X-mmStat-Referer'),
		resolution => $r->header_in('X-mmStat-Resolution')
	);
	if ($additional{referer} == undef)    { $additional{referer}    = 'none'; }
	if ($additional{resolution} == undef) { $additional{resolution} = 'none'; }

	my $iprec = $gi->record_by_addr($r->variable('REMOTE_ADDR'));
	my %country = (
                code => ($iprec ? $iprec->country_code : 'none'),
                name => ($iprec ? $iprec->country_name : 'none'),
                city => ($iprec ? $iprec->city         : 'none')
	);
        if ($country{code} =~ m/^$/) { $country{code} = 'none'; }
        if ($country{name} =~ m/^$/) { $country{name} = 'none'; }
        if ($country{city} =~ m/^$/) { $country{city} = 'none'; }

        $r->header_out("X-Debug-Geo", $country{code} . ", ". $country{name} . ", ". $country{city} .", ". $r->variable('REMOTE_ADDR')) if ($debug == 1);

	my $useragent = Parse::HTTP::UserAgent->new( $r->header_in('User-Agent') );
	my $os      = $useragent->unknown ? 'none' : $useragent->os;
	my $browser = $useragent->unknown ? 'none' : ($useragent->name . ' ' . $useragent->version);

	my %referer = getReferer($r);
	my @values = (
		$referer{host},
		$referer{uri},
	        $r->remote_addr,
		$additional{referer},
		$additional{resolution},
		$os,
		$browser,
		$country{code},
		$country{name},
		$country{city}
	);
	$r->header_out('X-Debug-Args', join(", ", @values)) if ($debug == 1);
	$overviewSth->execute(@values);
	#our $advOverviewSth = $connect->prepare("CALL putOverview('http://localhost/', '/', '15.25.43.52', 'http://referer/', '1920x1080', 'Linux', 'Firefox 28', 'RU', 'Russian Federation', 'Saint-Petersburg')");

	$r->status(200);
	$r->send_http_header('text/plain');
	return OK;
}

1;
__END__
