<?php

use Access\AccessRules;
use Ad\AdCampaign;
use Ad\AdStorage;
use Ad\Category\AdCategory;
use Ad\Category\AdCategoryStorage;

class CampaignsController extends CpanelController {
    protected $model;
    public $defaultAction = 'list';

    public function init() {
        parent::init();

        $rootDir = __DIR__ . "/../../../../";
        $statSrc = $rootDir . "stat_src/src/";
        include_once $rootDir . "stat_src/bootstrap/path.php";

        Yii::setPathOfAlias('Predis',     $rootDir . "vendor/predis/predis/src");
        Yii::setPathOfAlias('User',       $statSrc . 'User');
        Yii::setPathOfAlias('Storage',    $statSrc . 'Storage');
        Yii::setPathOfAlias('Utils',      $statSrc . 'Utils');
        Yii::setPathOfAlias('Statistics', $statSrc . 'Statistics');
        Yii::setPathOfAlias('Event',      $statSrc . 'Event');
        Yii::setPathOfAlias('Ad',         $statSrc . 'Ad');
        Yii::setPathOfAlias('Report',     $statSrc . 'Report');
        Yii::setPathOfAlias('Access',     $statSrc . 'Access');
    }

    /*
    public function filters() {
        return array('accessControl');
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'roles' => array('admin'),
            ),
            array('allow',
                'actions' => array('more', 'update', 'addCategory'),
//                'roles' => array('moderator'=>array('campaign'=>$this->campaign)),
            ),
            array('allow',
                'actions' => array('list'),
                'roles' => array('user'),
            ),
            array('allow',
                'actions' => array('more'),
//                'roles' => array('user'=>array('campaign'=>$this->campaign)),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }*/

    // allow to access a $this->campaign to model
//    public function getCampaign() {
//        if (isset($this->actionParams['id'])) {
//            return $this->loadModel($this->actionParams['id']);
//        }
//        return NULL;
//    }
//
//    public function loadModel($id) {
//        if ($this->model === null) {
//            $this->model = AdStorage::getInstance()->getById($id);
//        }
//
//        if ($this->model === null) {
//            throw new CHttpException(404, 'The requested page does not exists');
//        }
//
//        return $this->model;
//    }

    public function actionList() {
        $userId = Yii::app()->user->getId();
        if (!AccessRules::hasAccess($userId, AccessRules::RULE_AD_CAMPAIGN_PAGE, AccessRules::ACTION_VIEW)) {
            echo "403 no access";
            return;
        }

        $AdCategoryStorage = AdCategoryStorage::getInstance();
        $CurrentCategory   = $this->getAdCategory();

        $Parents    = $AdCategoryStorage->getParents($CurrentCategory);
        $list       = $AdCategoryStorage->getList($CurrentCategory);
        $Categories = $AdCategoryStorage->getByIds($list);

        ///////////////////////////////////////////////////////////////

        $AdStorage = AdStorage::getInstance();

        if ($CurrentCategory->id) {
            $list = $AdStorage->getAdHashIdList($CurrentCategory);
        } else {
            $list = $AdStorage->getAdHashIdList(new AdCategory([
                'id' => 'all'
            ]));
            $list = array_merge($list, $AdStorage->getAdHashIdList($CurrentCategory));
        }


        $Campaigns = array();
        foreach ($list as $hashId) {
            $Campaigns[] = $AdStorage->getByHashId($hashId);
        }

        $this->render('list', array(
            'Campaigns' => $Campaigns,
            'CurrentCategory'  => $CurrentCategory,
            'ParentsCategory' => $Parents,
            'Categories'  => $Categories,
        ));
    }

    public function actionEdit() {
        $Category = $this->getAdCategory();
        $Ad       = $this->getAd();
        $adData   = isset($_POST['ad']) ? $_POST['ad'] : array();

        if ($adData) {
            $error = $this->uploadEvent();

            if (!$error) {
                $_POST['ad']['data_array'] = $this->getDataArray();

                $Ad->import($_POST['ad']);

                if ($Ad->isNew()) {
                    Yii::app()->user->setFlash('notice', 'Campaign created successfully');

                    $Ad->author = Yii::app()->user->id;
                    $Ad->createTime = time();
                } else {
                    Yii::app()->user->setFlash('notice', 'Campaign updated successfully');
                }

                AdStorage::getInstance()->save($Ad, $Category);
            }
        }

        $this->render('_form', array(
            'Ad' => $Ad,
            'Category' => $Category,
        ));
    }

    public function actionUpdate() {
        $Ad = $this->getAd();
        $userId = Yii::app()->user->getId();

        if (!AccessRules::hasAccess($userId, AccessRules::RULE_AD_CAMPAIGN_PAGE, AccessRules::ACTION_EDIT, $Ad->author)) {
            echo "403 no access";
            return;
        }

        $this->actionEdit();
    }

    public function actionCreate() {
        $Ad     = $this->getAd();
        $userId = Yii::app()->user->getId();

        if ($Ad->id) {
            if (!AccessRules::hasAccess($userId, AccessRules::RULE_AD_CAMPAIGN_PAGE, AccessRules::ACTION_EDIT, $Ad->author)) {
                echo "403 no access";
                return;
            }
        }

        if (!AccessRules::hasAccess($userId, AccessRules::RULE_AD_CAMPAIGN_PAGE, AccessRules::ACTION_CREATE)) {
            echo "403 no access";
            return;
        }

        $this->actionEdit();
    }

    public function actionMore() {
        $this->render('more', array('model' => $this->campaign));
    }

    public function actionAddCategory() {
        $userId = Yii::app()->user->getId();
        if (!AccessRules::hasAccess($userId, AccessRules::RULE_AD_CAMPAIGN_CATEGORY, AccessRules::ACTION_CREATE)) {
            echo "403 no access";
            return;
        }

        $title  = $_GET['title'];
        $parent = $_GET['category'];

        $Category = new AdCategory();
        $Category->parentId = $parent;
        $Category->title    = $title;

        AdCategoryStorage::getInstance()->save($Category);

        header('Location: /index.php?r=cpanel/campaigns&category=' . $Category->id);
        return;
    }

    /**
     * @return array
     */
    protected function getDataArray() {
        $arr_key    = null;
        $data_array = array();
        foreach ($_POST['ad']['data_array'] as $key => $value) {
            if (!$value) {continue;}

            if (is_null($arr_key)) {
                $arr_key = $value;
            } else {
                $data_array[$arr_key] = $value;
                $arr_key = null;
            }
        }

        return $data_array;
    }

    /**
     * @return AdCampaign
     */
    protected function getAd() {
        $adId = isset($_GET['adId']) ? $_GET['adId'] : null;
        if ($adId) {
            $Ad = AdStorage::getInstance()->getById($adId);
        } else {
            $Ad = new AdCampaign();
        }

        return $Ad;
    }

    /**
     * @return AdCategory
     */
    protected function getAdCategory() {
        $currentCategory = isset($_GET['category']) ? $_GET['category'] : null;
        $CurrentCategory = AdCategoryStorage::getInstance()->getById($currentCategory);
        if (!$CurrentCategory) {
            $CurrentCategory = new AdCategory();
        }

        return $CurrentCategory;
    }

    /**
     * @return bool
     */
    protected function uploadEvent() {
        $error = false;
        foreach (AdCampaign::getProperties() as $field => $options) {
            if ($options['type'] == 'file' && isset($_FILES['ad']['name']) && isset($_FILES['ad']['name'][$field]) && $_FILES['ad']['name'][$field]) {
                $filename = null;

                try {
                    $filename = $this->uploadFile($field);
                } catch (Exception $Ex) {
                    $error = true;
                    Yii::app()->user->setFlash('error', $Ex->getMessage());
                }

                if ($filename) {
                    $_POST['ad'][$field] = $filename;
                }
            }
        }

        return $error;
    }

    /**
     * @param string $field
     * @return bool
     * @throws Exception
     */
    protected function uploadFile($field) {
        //Проверяем, загружен ли файл во временную папку
        if (!is_uploaded_file($_FILES['ad']['tmp_name'][$field])) {
            throw new Exception('Error upload file');
        }

        $allowExt = array('jpg', 'jpeg', 'png', 'gif', 'swf');
        $filename = basename($_FILES['ad']['name'][$field]);
        $file_extension = strtolower(substr(strrchr($filename,"."),1));

        if (!in_array($file_extension, $allowExt)) {
            throw new Exception('Error file extension. Allow: ' . implode(', ', $allowExt));
        }

        $generateFolderName = substr(sha1(rand(0, 5000000) . sha1(rand(0, 5000000))), 0, 12);
        $uploadDir  = $_SERVER['DOCUMENT_ROOT'] . '/upload_files/';
        mkdir($uploadDir . $generateFolderName);

        $filename   = $generateFolderName . '/' . $_FILES['ad']['name'][$field];
        $fileUpload = $uploadDir . $filename;
        move_uploaded_file($_FILES['ad']['tmp_name'][$field], $fileUpload);

        file_get_contents('http://ad.mindmeal.ru/syncFile.php?filename=' . $filename);

        return $filename;
    }
}
