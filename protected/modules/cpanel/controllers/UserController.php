<?php

use Access\AccessRules;

class UserController extends CpanelController {
    protected $model;
    public $defaultAction = 'list';

    public function init() {
        parent::init();

        $rootDir = __DIR__ . "/../../../../";
        $statSrc = $rootDir . "stat_src/src/";
        include_once $rootDir . "stat_src/bootstrap/path.php";

        Yii::setPathOfAlias('Predis',     $rootDir . "vendor/predis/predis/src");
        Yii::setPathOfAlias('User',       $statSrc . 'User');
        Yii::setPathOfAlias('Storage',    $statSrc . 'Storage');
        Yii::setPathOfAlias('Utils',      $statSrc . 'Utils');
        Yii::setPathOfAlias('Statistics', $statSrc . 'Statistics');
        Yii::setPathOfAlias('Event',      $statSrc . 'Event');
        Yii::setPathOfAlias('Ad',         $statSrc . 'Ad');
        Yii::setPathOfAlias('Report',     $statSrc . 'Report');
        Yii::setPathOfAlias('Access',     $statSrc . 'Access');
    }

/*
    public function filters() {
        return array('accessControl');
    }

    public function accessRules() {
        return array(
                array('allow',
                    'roles' => array('admin'),
                ),
                array('allow',
                    'actions' => array('profile'),
                    'roles' => array('user'),
                ),
                array('deny',
                    'roles' => array('*'),
                ),
        );
    }
*/
    public function getUserModel() {
        if (isset($this->actionParams['id'])) {
            return $this->loadModel($this->actionParams['id']);
        }

        return null;
    }

    public function loadModel($id) {
        if ($this->model === null) {
            $this->model = User::model()->findByPk($id);
        }

        if ($this->model === null) {
            throw new CHttpException(404, 'The requested page does not exists');
        }

        return $this->model;
    }

    public function actionList() {
        $userId = Yii::app()->user->getId();
        if (!AccessRules::hasAccess($userId, AccessRules::RULE_USERS_PAGE, AccessRules::ACTION_VIEW)) {
            echo "403 no access";
            return;
        }

        $users = User::model()->findAll();
        $this->render('list', array('users'=>$users));
    }

    protected function actionEdit() {
        $userId = Yii::app()->user->getId();
        if (!AccessRules::hasAccess($userId, AccessRules::RULE_USERS_PAGE, AccessRules::ACTION_EDIT)) {
            echo "403 no access";
            return;
        }

        $AccessGroups        = new \Access\AccessGroupConfig();
        $AccessGroupsStorage = new \Access\AccessGroupStorage();
        $accessGroups = $AccessGroups->getGroups();

        if (isset($this->actionParams['id'])) {
            $model = $this->getUserModel();
            $model->password = '';
        } else {
            $model = new User();
            $model->activated = 1;
            $model->created = time();
        }

        if (isset($_POST[get_class($model)])) {
            $input = $_POST[get_class($model)];

            $model->attributes = $input;
            $attributes = array('username', 'email');

            $isNew = $model->getIsNewRecord();

            // todo РЕФАКТОРИ ЭТО ДЕРЬМО И ПОЗОР БЛЕАТЬ!!!!111
            if (isset($input['password']) && $input['password']) {
                if ($input['password'] != $input['confirmPassword']) {
                    Yii::app()->user->setFlash('error', "Пароли должны совпадать");

                    if ($isNew) {
                        $this->redirect( $this->createUrl('/cpanel/user/create') );
                    } else {
                        $this->redirect( $this->createUrl('/cpanel/user/update', array('id' => $model->id)) );
                    }
                } else {
                    $attributes[] = 'password';
                    $attributes[] = 'confirmPassword';

                    if (!$isNew) {
                        $model->password = crypt($input['password'], UserIdentity::blowfishSalt());
                    }
                }
            } else if ($isNew) {
                Yii::app()->user->setFlash('error', "Пароль не может быть пустым");
                $this->redirect( $this->createUrl('/cpanel/user/create') );
            }
            //////

            if (isset($input['activated'])) {
                $model->activated = $input['activated'] == 'on' ? 1 : 0;
                $attributes[] = 'activated';
            }

            if ($isNew) {
                $attributes[] = 'created';
            }

            if ($model->save(true, $attributes)) {
                if (isset($_POST['group'])) {
                    $AccessGroupsStorage->saveGroupForUserId($model->id, $_POST['group']);
                }

                Yii::app()->user->setFlash('notice', 'User succesfully saved');
            } else {
                Yii::app()->user->setFlash('error', CHtml::errorSummary($model));
            }
        }

        $this->render('_new_form', array(
            'model'        => $model,
//            'campaigns'    => $campaigns,
//            'assignments'  => $assignments,
            'accessGroups' => $accessGroups,
            'currentGroup' => $AccessGroupsStorage->getGroupForUser($model->id),
        ));

        return;
    }

    public function actionUpdate() {
        $userId = Yii::app()->user->getId();
        if (!AccessRules::hasAccess($userId, AccessRules::RULE_USERS_PAGE, AccessRules::ACTION_EDIT)) {
            echo "403 no access";
            return;
        }

        $this->actionEdit();
    }

    public function actionCreate() {
        $userId = Yii::app()->user->getId();
        if (!AccessRules::hasAccess($userId, AccessRules::RULE_USERS_PAGE, AccessRules::ACTION_CREATE)) {
            echo "403 no access";
            return;
        }

        $this->actionEdit();
    }

    protected function actionAjaxTellHTMLStatus($status) {
        if (isset($status) && $status == 'true') {
            header('HTTP/1.0 200 OK');
            echo '<small class="label label-success">' . Yii::t('cpanel.labels', 'Success') . '</small>';
        } else {
            header('HTTP/1.0 410 Gone');
            echo '<small class="label label-danger">' . Yii::t('cpanel.labels', 'Error') . '</small>';
        }
    }

    public function actionAjaxAssignModerate() {
        $campaign_id = Yii::app()->request->getParam('campaign_id', false);
        if (isset($this->actionParams['id']) && ($campaign_id !== false)) {
            $result = $this->userModel->assignCampaignRole('moderator', $campaign_id);
        }
        $this->actionAjaxTellHTMLStatus(isset($result) && $result);
    }

    public function actionAjaxRevokeModerate() {
        $campaign_id = Yii::app()->request->getParam('campaign_id', false);
        if (isset($this->actionParams['id']) && ($campaign_id !== false)) {
            $result = $this->userModel->revokeCampaignRole('moderator', $campaign_id);
        }
        $this->actionAjaxTellHTMLStatus(isset($result) && $result);
    }

    public function actionAjaxAssignView() {
        $campaign_id = Yii::app()->request->getParam('campaign_id', false);
        if (isset($this->actionParams['id']) && ($campaign_id !== false)) {
            $result = $this->userModel->assignCampaignRole('user', $campaign_id);
        }
        $this->actionAjaxTellHTMLStatus(isset($result) && $result);
    }

    public function actionAjaxRevokeView() {
        $campaign_id = Yii::app()->request->getParam('campaign_id', false);
        if (isset($this->actionParams['id']) && ($campaign_id !== false)) {
            $result = $this->userModel->revokeCampaignRole('user', $campaign_id);
        }
        $this->actionAjaxTellHTMLStatus(isset($result) && $result);
    }

    public function actionProfile() {
        $this->render('profile', array('model' => $this->userModel));
    }
}
