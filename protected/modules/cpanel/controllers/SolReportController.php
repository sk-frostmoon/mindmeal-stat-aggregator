<?php

use SOL\Report\ReportRedisStorage;

class SolReportController extends CpanelController {
    public $defaultAction = 'index';

    public function init() {
        parent::init();

        $rootDir = __DIR__ . "/../../../../";
        $statSrc = $rootDir . "stat_src/src/";
        include_once $rootDir . "stat_src/bootstrap/path.php";

        Yii::setPathOfAlias('Predis',     $rootDir . "vendor/predis/predis/src");
        Yii::setPathOfAlias('User',       $statSrc . 'User');
        Yii::setPathOfAlias('Storage',    $statSrc . 'Storage');
        Yii::setPathOfAlias('Utils',      $statSrc . 'Utils');
        Yii::setPathOfAlias('Statistics', $statSrc . 'Statistics');
        Yii::setPathOfAlias('Event',      $statSrc . 'Event');
        Yii::setPathOfAlias('Ad',         $statSrc . 'Ad');
        Yii::setPathOfAlias('Report',     $statSrc . 'Report');
        Yii::setPathOfAlias('Access',     $statSrc . 'Access');
    }

    /*public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array(
                'allow',
                'actions' => array('index', 'sourceMore', 'details'),
                'roles' => array('developer'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }*/

    public function actionIndex() {
        $ReportStorage = new ReportRedisStorage();
        $reports = $ReportStorage->getReportsList();

        $reportName = isset($_GET['report']) ? $_GET['report'] : 'test';

        $report  = $ReportStorage->getReport($reportName);
        $columns = array_keys($report[0]);
        $columnsData = array();
        foreach ($columns as $number => $key) {
            if ( $key == 'total_payments' ||$key == 'first_payment_date'||$key == 'avg_payment' || $key == 'avg_bill' ||$key == 'total_game_enter') {
                continue;
            }

            $columnsData[] = array(
                'title' => $key,
                'data' => $key,
            );
        }

        $this->render('application.modules.cpanel.views.solReport.index', array(
            'dataTable' => json_encode($report),
            'columnsTable' => json_encode($columnsData),
            'reportName' => $reportName,
            'reports' => $reports,
        ));
    }
}