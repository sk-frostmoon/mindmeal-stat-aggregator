<?php

use Access\AccessRules;
use Ad\AdStorage;
use Report\Filter\ReportFilter;
use Report\Filter\ReportFilterContent;
use Report\Filter\ReportFilterSelectedValues;
use Report\Query\Details\DetailsQueryBuilder;
use Report\Query\StatisticsHistogramQueryBuilder;
use Report\Query\StatisticsMultifieldTermQueryBuilder;
use Report\Query\StatisticsTermQueryBuilder;
use Report\StatisticsStorage;
use Report\TableColumnConfig;

/**
 * Контроллер для страницы с отчетами
 * @author akiselev
 */
class ReportsController extends CpanelController {
    public $defaultAction = 'index';

    public function init() {
        parent::init();

        $rootDir = __DIR__ . "/../../../../";
        $statSrc = $rootDir . "stat_src/src/";
        include_once $rootDir . "stat_src/bootstrap/path.php";

        Yii::setPathOfAlias('Predis',     $rootDir . "vendor/predis/predis/src");
        Yii::setPathOfAlias('User',       $statSrc . 'User');
        Yii::setPathOfAlias('Storage',    $statSrc . 'Storage');
        Yii::setPathOfAlias('Utils',      $statSrc . 'Utils');
        Yii::setPathOfAlias('Statistics', $statSrc . 'Statistics');
        Yii::setPathOfAlias('Event',      $statSrc . 'Event');
        Yii::setPathOfAlias('Ad',         $statSrc . 'Ad');
        Yii::setPathOfAlias('Access',     $statSrc . 'Access');

        Yii::setPathOfAlias('Report', __DIR__ . '/../models/Report');
    }


//    public function accessRules() {
//        return array(
////            array('deny',
////                'users' => array('*'),
////            ),
//        );
//    }

    public function actionDetails() {
        $userId = Yii::app()->user->getId();
        if (!AccessRules::hasAccess($userId, AccessRules::RULE_REPORT_PAGE, AccessRules::ACTION_VIEW)) {
            echo "403 no access";
            return;
        }

        $timeTo    = isset($_GET['to'])       && $_GET['to']       ? (int) $_GET['to']   : strtotime(date('Y-m-d'));
        $timeFrom  = isset($_GET['from'])     && $_GET['from']     ? (int) $_GET['from'] : strtotime('-1 day', $timeTo);
        $shownMode = isset($_GET['mode'])     && $_GET['mode']     ? $_GET['mode']       : null;

        // Готовим объекты для работы
        $filter = '';
        $StatisticsStorage = new StatisticsStorage();
        $TableConfig       = new TableColumnConfig();

        $QueryBuilder = new DetailsQueryBuilder();

        $columnQuery = $TableConfig->getQuery(TableColumnConfig::GOAL_REGISTER, $shownMode);
        $Query = $QueryBuilder->makeClone()
            ->setRange($timeFrom + 60 * 60 * 4, strtotime('+1 day', $timeTo + 60 * 60 * 4))
            ->setLucene($columnQuery);


        $results = $StatisticsStorage->exec([$Query]);

        $container = array(
            'login_id' => null,
            'ref_domain' => null,
            'last_request' => null,
            'utm_campaign' => null,
            'utm_source' => null,
            'utm_redirect' => null,
            'site' => null,
            'page' => null,
        );

        $columns = array(
            array(
                "title" => "Id юзера",
                "default" => null,
                "data" => "login_id",
                "canHide" => true
            ),array(
                "title" => "Реферал",
                "default" => null,
                "data" => "ref_domain",
                "canHide" => true
            ),array(
                "title" => "Время хита",
                "default" => null,
                "data" => "last_request",
                "canHide" => true
            ),array(
                "title" => "utm_campaign",
                "default" => null,
                "data" => "utm_campaign",
                "canHide" => true
            ),array(
                "title" => "utm_source",
                "default" => null,
                "data" => "utm_source",
                "canHide" => true
            ),array(
                "title" => "Рекламная кампания",
                "default" => null,
                "data" => "utm_redirect",
                "canHide" => true
            ),array(
                "title" => "Сайт",
                "default" => null,
                "data" => "site",
                "canHide" => true
            ),array(
                "title" => "Страница",
                "default" => null,
                "data" => "page",
                "canHide" => true
            ),array(
                "title" => "debug",
                "default" => null,
                "data" => "all",
                "canHide" => true,
            ),
        );

        $AdStorage = AdStorage::getInstance();

        $resultData = array();
        foreach ($results[0] as $key => $event) {
            if (!isset($event['user'])) {
                continue;
            }
            $data = $container;

            $user = $event['user'];
            $data['login_id']     = isset($user['login_id']) ? $user['login_id'] : null;
            $data['ref_domain']   = isset($user['ref_domain']) ? $user['ref_domain'] : null;
            $data['last_request'] = isset($user['last_request']) ? date('Y.m.d H:i:s', strtotime($user['last_request'])) : null;
            $data['utm_campaign'] = isset($user['utm']) && isset($user['utm']['utm_campaign']) ? $user['utm']['utm_campaign'] : null;
            $data['utm_source']   = isset($user['utm']) && isset($user['utm']['utm_source']) ? $user['utm']['utm_source'] : null;
            $data['utm_redirect'] = isset($user['utm']) && isset($user['utm']['utm_redirect']) ? $user['utm']['utm_redirect'] : null;
            $data['site']         = isset($user['data']) && isset($user['data']['site']) ? $user['data']['site'] : null;
            $data['page']         = isset($user['data']) && isset($user['data']['page']) ? $user['data']['page'] : null;

            if ($data['utm_redirect']) {
                $Ad = $AdStorage->getByHashId($data['utm_redirect']);
                $data['utm_redirect'] = "<a href='index.php?r=cpanel/campaigns/update&adId={$Ad->id}'>" . $Ad->name . "</a>";
            }

            $user = array_filter($user);
            unset($user['id']);
            $data['all'] = '<a href="javascript: void(0);" onclick="$(\'#data-' . $key . '\').toggle(100)">data</a><pre id="data-' . $key . '" style="display: none; width: 420px; text-align:left;">' . var_export($user, true) . '</pre>';

            $resultData[] = $data;
        }

        $dateFrom = date('d.m.Y', $timeFrom);
        $dateTo   = date('d.m.Y', $timeTo);

        $this->render('application.modules.cpanel.views.reports.details', array(
            'columnsTable'   => json_encode($columns),
            'columnsTableArray' => $columns,
            'queries'        => $TableConfig->getQueries(),
            'dataTable'      => json_encode($resultData),
            'totalRows'      => sizeof($resultData),
            'dateFrom'       => $dateFrom,
            'dateTo'         => $dateTo,
            'timeFrom'       => $timeFrom,
            'timeTo'         => $timeTo,
        ));
    }

    protected function expandArrayColsToRows(array $arr, $fieldRow, $fieldValue) {
        // Развернем сырые данные по колонкам -> по строкам
        $tableDataPerRows = array();
        foreach ($arr as $columnName => $tableDataPerColumn) {
            foreach ($tableDataPerColumn as $column) {
                if (!isset($column[$fieldRow])) {
                    continue;
                }

                $rowName = $column[$fieldRow];

                if (!isset($tableDataPerRows[$rowName])) {
                    $tableDataPerRows[$rowName] = array();
                }

                $tableDataPerRows[$rowName][$columnName] = $column[$fieldValue];
            }
        }

        return $tableDataPerRows;
    }

    public function actionIndex() {
        $userId = Yii::app()->user->getId();
        if (!AccessRules::hasAccess($userId, AccessRules::RULE_REPORT_PAGE, AccessRules::ACTION_VIEW)) {
            echo "403 no access";
            return;
        }

        // Создаем классы для работы
        $TableConfig          = new TableColumnConfig();
        $ReportFilterContent  = new ReportFilterContent($TableConfig);
        $ReportFilterSelected = new ReportFilterSelectedValues();
        $StatisticsStorage    = new StatisticsStorage();

        // Принимаем входные данные фильтров
        $timeTo           = $this->getInputValue('to', strtotime(date('Y-m-d')));
        $timeFrom         = $this->getInputValue('from', strtotime('-1 day', $timeTo));
        $site             = $this->getInputValue('site', TableColumnConfig::QUERY_DEFAULT);
        $showColumn       = $this->getInputValue('show-column', []);
        $utm_source       = $this->getInputValue('utm_source', []);
        $utm_medium       = $this->getInputValue('utm_medium', []);
        $utm_campaign     = $this->getInputValue('utm_campaign', []);
        $filter_column    = $this->getInputValue('filter-column', []);
        $filter_operation = $this->getInputValue('filter-operation', []);
        $filter_value     = $this->getInputValue('filter-value', []);
        $details          = $this->getInputValue('details', null);
        $interval         = $this->getInputValue('interval', '1d');

        // Формируем фильтры
        $ReportFilterContent->setInterval($timeFrom, $timeTo);

        $ReportFilterSelected->timeFrom        = $timeFrom;
        $ReportFilterSelected->timeTo          = $timeTo;
        $ReportFilterSelected->site            = $site;
        $ReportFilterSelected->hiddenColumns   = $showColumn;
        $ReportFilterSelected->utmSource       = is_array($utm_source) ? $utm_source : [$utm_source];
        $ReportFilterSelected->utmMedium       = is_array($utm_medium) ? $utm_medium : [$utm_medium];
        $ReportFilterSelected->utmCampaign     = is_array($utm_campaign) ? $utm_campaign : [$utm_campaign];
        $ReportFilterSelected->filterColumn    = $filter_column;
        $ReportFilterSelected->filterOperation = $filter_operation;
        $ReportFilterSelected->filterValue     = $filter_value;

        // Сделаем запрос по имеющимся фильтрам
        if ($details == null) {
            $QueryBuilder = new StatisticsMultifieldTermQueryBuilder();
            $ReportFilterSelected->term = array(
                'user.utm.utm_campaign',
                'user.utm.utm_source',
                'user.utm.utm_medium',
            );
        } else {
            $filterUTM = $this->normalizeFilter($details);

            $QueryBuilder = new StatisticsHistogramQueryBuilder();
            $QueryBuilder->setFilter($filterUTM)
                ->setInterval($interval);
        }

        $Queries = $QueryBuilder->makeQueries($TableConfig, $ReportFilterSelected);

        // Выполняем запросы, получаем сырые данные по колонкам
        $results = $StatisticsStorage->exec($Queries);
        $results = $this->expandArrayColsToRows($results, $QueryBuilder->getFieldRow(), $QueryBuilder->getFieldValue());

        // Смотрим какие UTM-ы мы имеем
        $utms = array_keys($results);
        $utms = $this->makeArrayUtm($utms);
        $ReportFilterContent->utmMedium   = isset($utms['utm_medium']) && is_array($utms['utm_medium']) ? $utms['utm_medium'] : [];
        $ReportFilterContent->utmSource   = isset($utms['utm_source']) && is_array($utms['utm_source']) ? $utms['utm_source'] : [];
        $ReportFilterContent->utmCampaign = isset($utms['utm_campaign']) && is_array($utms['utm_campaign']) ? $utms['utm_campaign'] : [];

        // Обрезаем отчет по фильтрам
        if (is_null($details)) {
            $results = $this->explodeResultsByFilter($results, $utms, $ReportFilterSelected);
        }

        // Вытаскиваем список колонок в массив "имя колонки" => 0
        // Начало формирования самого отчета
        $columnsConfig = $TableConfig->tableColumns();
        foreach ($columnsConfig as $columnName => $columnData) {
            $columnsConfig[$columnName] = $columnData['default'];
        }

        // Считаем Строку "всего"
        $total = $columnsConfig;
        $total['title'] = 'Total';
        foreach ($results as $rowName => $rowData) {
            $results[$rowName] = array_merge($columnsConfig, $results[$rowName]);

            // todo убрать как-нибудь потом
            if (urldecode($rowName) == '10..||100820||239501||7||127||63||3||0||0||-57845005||sspc_1411051434645||1:null:null') {
                unset($results[$rowName]);
                continue;
            }


            if (is_null($details)) {
                $url = $this->createUrl('index', array_filter(array(
                    'from'    => $timeFrom,
                    'to'      => $timeTo,
                    'details' => $rowName,
                    'site'    => $site,
                )));
                $results[$rowName]['title'] = '<a href="' . $url . '">' . urldecode($rowName) . '</a>';
            } else {
                $results[$rowName]['title'] = date('Y/m/d', $rowName / 1000);
            }


            foreach ($TableConfig->tableColumns() as $columnName => $columnData) {
                if (!array_key_exists($columnName, $columnsConfig)) {
                    continue;
                }

                // Ищем столбец, в котором нужно считать конверсию
                if (isset($columnData['isConversion'])) {
                    $results[$rowName][$columnName] = $this->calcPercent(
                        $results[$rowName][$columnData['isConversion']],
                        $results[$rowName][$columnData['conversionField']]
                    );
                } else {
                    if (!isset($columnData['notValue'])) {
                        $total[$columnName] += $results[$rowName][$columnName];
                    }
                }
            }
        }

        // Считаем проценты для total
        foreach ($TableConfig->tableColumns() as $columnName => $columnData) {
            // Ищем столбец, в котором нужно считать конверсию
            if (isset($columnData['isConversion'])) {
                $total[$columnName] = $this->calcPercent(
                    $total[$columnData['isConversion']],
                    $total[$columnData['conversionField']]
                );
            }
        }

        $results = array_values($results);
        $columns = array_values($TableConfig->tableColumns());

        $this->render('application.modules.cpanel.views.reports.test', [
            'ReportFilterContent' => $ReportFilterContent,
            'ReportFilterSelected' => $ReportFilterSelected,
            'results' => json_encode($results),
            'columns' => json_encode($columns),
            'total'   => json_encode($total),
        ]);
    }

    /**
     * Обрезаем отчет по фильтрам
     * @param array $results
     * @param array $utms
     * @param ReportFilterSelectedValues $ReportFilterSelected
     * @return array
     */
    public function explodeResultsByFilter(array $results, array $utms, ReportFilterSelectedValues $ReportFilterSelected) {
        foreach ($results as $rowName => $rowData) {
            $find        = 0;
            $filterIsset = 0;

            // Фильтруем данные по UTM-у
            if ($ReportFilterSelected->utmCampaign) {
                $filterIsset ++;
                foreach ($ReportFilterSelected->utmCampaign as $_utm_campaign) {
                    $pos = strpos($rowName, $_utm_campaign . ':');
                    if (($pos !== false && $pos == 0) || !in_array($_utm_campaign, $utms['utm_campaign'])) {
                        $find ++;
                    }
                }
            }
            if ($ReportFilterSelected->utmSource) {
                $filterIsset ++;
                foreach ($ReportFilterSelected->utmSource as $_utm_source) {
                    if (strpos($rowName, ':' . $_utm_source . ':') !== false || !in_array($_utm_source, $utms['utm_source'])) {
                        $find ++;
                    }
                }
            }
            if ($ReportFilterSelected->utmMedium) {
                $filterIsset ++;
                foreach ($ReportFilterSelected->utmMedium as $_utm_medium) {
                    if (strpos($rowName, ':' . $_utm_medium) !== false || !in_array($_utm_medium, $utms['utm_medium'])) {
                        $find ++;
                    }
                }
            }

            // Фильтруем данные по условиям
            foreach ($ReportFilterSelected->filterColumn as $i => $column) {
                if ($ReportFilterSelected->filterValue[$i] == '') {
                    continue;
                }

                $filterIsset ++;

                if (!isset($rowData[$column])) {
                    $rowData[$column] = 0;
                }

                if ($ReportFilterSelected->filterOperation[$i] == 'large') {
                    if ($rowData[$column] > $ReportFilterSelected->filterValue[$i]) {
                        $find ++;
                    }
                }

                if ($ReportFilterSelected->filterOperation[$i] == 'small') {
                    if ($rowData[$column] < $ReportFilterSelected->filterValue[$i]) {
                        $find ++;
                    }
                }
            }

            if ($filterIsset != $find) {
                // Не подходит под указанный фильтр
                unset($results[$rowName]);
            }
        }

        return $results;
    }

    /**
     * Возвращает значение входящих параметров ($_GET или $_POST)
     * @param string $field
     * @param mixed $default
     * @return mixed
     */
    protected function getInputValue($field, $default = null) {
        $input = array_merge($_GET, $_POST);
        if (isset($input[$field])) {
            return $input[$field];
        }

        return $default;
    }

    /**
     * Подсчет процентов
     * @param number $firstVar
     * @param number $secondVar
     * @return string
     */
    protected function calcPercent($firstVar, $secondVar) {
        if ($firstVar <= 0) {
            return 0 . '%';
        } else {
            return round(100 * $secondVar / $firstVar, 2) . '%';
        }
    }

    /**
     * @param string $filter
     * @return array
     */
    protected function normalizeFilter($filter) {
        $normalizedFilter = '';
        $campaign = '';
        $source   = '';
        $medium   = '';

        $splitFilter = explode(':', $filter);

        if (sizeof($splitFilter) == 3) {
            $campaign = $splitFilter[0];
            $source   = $splitFilter[1];
            $medium   = $splitFilter[2];
        }

        if (sizeof($splitFilter) > 3) {
            $campaign = $splitFilter[0];
            $medium   = $splitFilter[sizeof($splitFilter) - 1];

            $filter = str_replace($campaign . ':', '', $filter);
            $filter = str_replace(':' . $medium, '', $filter);

            $source = $filter;
        }

        if ($campaign && $campaign != 'null') {
            $normalizedFilter .= 'user.utm.utm_campaign:' . $campaign;
        } else {
            $normalizedFilter .= '_missing_:user.utm.utm_campaign';
        }

        if ($source && $source != 'null') {
            $normalizedFilter .= ' AND user.utm.utm_source:' . $source;
        } else {
            $normalizedFilter .= ' AND _missing_:user.utm.utm_source';
        }

        if ($medium && $medium != 'null') {
            $normalizedFilter .= ' AND user.utm.utm_medium:' . $medium;
        } else {
            $normalizedFilter .= ' AND _missing_:user.utm.utm_medium';
        }

        return $normalizedFilter;
    }

    protected function makeArrayUtm(array $filters) {
        $campaign = '';
        $source   = '';
        $medium   = '';

        $utms = [];
        foreach ($filters as $filter) {

            $splitFilter = explode(':', $filter);

            if (sizeof($splitFilter) == 3) {
                $campaign = $splitFilter[0];
                $source   = $splitFilter[1];
                $medium   = $splitFilter[2];
            }

            if (sizeof($splitFilter) > 3) {
                $campaign = $splitFilter[0];
                $medium   = $splitFilter[sizeof($splitFilter) - 1];

                $filter = str_replace($campaign . ':', '', $filter);
                $filter = str_replace(':' . $medium, '', $filter);

                $source = $filter;
            }

            $utms['utm_campaign'][$campaign] = $campaign;
            $utms['utm_source'][$source] = $source;
            $utms['utm_medium'][$medium] = $medium;
        }

        sort($utms['utm_campaign'], SORT_STRING);
        sort($utms['utm_source'], SORT_STRING);
        sort($utms['utm_medium'], SORT_STRING);

        return $utms;
    }
}