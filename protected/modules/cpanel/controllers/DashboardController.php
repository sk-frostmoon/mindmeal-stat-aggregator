<?php

class DashboardController extends CpanelController
{
    public $defaultAction = 'index';

    public function actionIndex()
    {
        $this->render('index'); 
    }
}
