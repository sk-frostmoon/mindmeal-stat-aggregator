<?php

class CpanelModule extends CWebModule
{
    public $defaultController = 'dashboard';

    public $assetsUrl;

    public $webRootStat = '';

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			//'cpanel.models.*',
			//'cpanel.models.CampaignBehaviors.*',
			'cpanel.components.*',
		));

        // @ak Theme will changed early than controller created
        Yii::app()->theme = 'classic';

        $am = Yii::app()->assetManager;
        $this->assetsUrl = $am->publish($this->basePath . '/assets', false, -1, defined('YII_DEBUG') and YII_DEBUG);

        $cs = Yii::app()->clientScript;
        $cs->registerCssFile($this->assetsUrl . '/css/cpanel.css');
        /*
        $cs->registerScriptFile('/js/min.js');
        $cs->registerScriptFile('/js/log.js');
        $cs->registerScriptFile('/js/jquery.min.js');
        $cs->registerScriptFile('/js/jquery-ui.min.js');
        $cs->registerScriptFile($this->assetsUrl . '/js/bootstrap.min.js');
        $cs->registerCssFile($this->assetsUrl . '/css/bootstrap.css');
        $cs->registerCssFile($this->assetsUrl . '/css/style.css');
        $cs->registerCssFile($this->assetsUrl . '/css/bones.css');
        */
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}

