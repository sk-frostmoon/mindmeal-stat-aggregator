<?php

class CpanelController extends Controller
{
    public function filters()
    {
        return array( 'accessControl' );
    }

    public function accessRules()
    {
        return array(
                array('allow',
                    'users' => array('@'),
                ),
                array('deny'),
        );
    }

    public function init()
    {
        parent::init();
        // @ak Check if user present in DB and WebUser restored without actual data from session
        if (!Yii::app()->user->getIsGuest() && (User::model()->findByPk(Yii::app()->user->id) === NULL)) {
            $this->redirect($this->createUrl('/site/logout'));
        }
    }

    public function renderFlashes()
    {
        $flashes = Yii::app()->user->getFlashes();
        if (!empty($flashes)) {
            foreach($flashes as $type => $text) {
                $this->renderPartial('application.views.site._flash', array(
                            'type' => $type,
                            'htmlText' => $text,
                ));
            }
        }
    }
}
