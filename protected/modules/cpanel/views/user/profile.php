<? /* @var Controller $this */ ?>
<div class="col-sm-6 col-sm-offset-3">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Профиль пользователя <b><?=CHtml::encode($model->getFullName());?></b></h3>
        </div>
        <div class="box-body">
            <dl class="dl-horizontal">
                <dt><?=Yii::t('yii', 'Зарегистрирован');?></dt>
                <dd><?=date('Y-m-d H:i:s', $model->created)?></dd>
            </dl>
        </div>
        <? if (Yii::app()->user->checkAccess('admin')) { ?>
            <div class="box-footer">
                <a class="btn btn-default" href="<?=$this->createUrl('/cpanel/user/update', array('id' => $model->id)); ?>"><?=Yii::t('yii', 'Update');?></a>
            </div>
        <? } ?>

    </div>
</div>
