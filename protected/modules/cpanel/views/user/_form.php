<? /* @var Controller $this; */
$this->pageTitle = $model->getIsNewRecord() ? "Создание пользователя" : "Редактирование пользователя";


$cs = Yii::app()->clientScript;
$cs->registerCoreScript('jquery');
$cs->registerScriptFile($this->getModule()->assetsUrl . '/js/users.js');
?>
<div class="col-sm-6">
    <form id="modelEditForm" action="<?=$this->createUrl('/cpanel/user/'.$this->getAction()->id, $model->getIsNewRecord() ? array() : array('id'=>$model->id));?>" data-model-id="<?=$model->id;?>" method="POST">
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-info-circle"></i><?=Yii::t('Cpanel.labels', 'User information');?></h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label for="User[username]"><?=$model->getAttributeLabel('username');?></label>
                    <input class="form-control" name="User[username]" type="text" value="<?=CHtml::encode($model->username);?>"/>
                </div>
                <div class="form-group">
                    <label for="User[email]"><?=$model->getAttributeLabel('email');?></label>
                    <input class="form-control" name="User[email]" type="text" value="<?=CHtml::encode($model->email);?>"/>
                </div>
                <? if ($model->getIsNewRecord()) { ?>
                    <div class="form-group">
                        <label for="User[password]"><?=$model->getAttributeLabel('password');?></label>
                        <input class="form-control" name="User[password]" type="password" value="<?=CHtml::encode($model->password);?>"/>
                            
                    </div>
                    <div class="form-group">
                        <label for="User[confirmPassword]"><?=$model->getAttributeLabel('confirmPassword');?></label>
                        <input class="form-control" name="User[confirmPassword]" type="password" value="<?=CHtml::encode($model->confirmPassword);?>"/>
                    </div>
                <? } ?>
                <div class="form-group">
                    <label>
                        <input type="hidden" name="User[activated]" value="<?=$model->activated?>"/>
                        <input class="flat-green" type="checkbox" data-name="User[activated]"
                            <? if ($model->activated == '1') { ?> checked="checked" <? } ?>/>
                        <?=Yii::t('cpanel', 'Пользователь активирован');?>
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <input class="flat-red" type="checkbox" name="AuthAssignments[admin]" value="<?=$model->id?>"
                            <? if (Yii::app()->authManager->checkAccess('admin', $model->id)) { ?> checked="checked" <? } ?>/>
                        <?=Yii::t('cpanel', 'Администратор пользователей');?>
                    </label>
                </div>
                <div class="form-group">
                    <label>
                        <input class="flat-red" type="checkbox" name="AuthAssignments[developer]" value="<?=$model->id?>"
                            <? if (Yii::app()->authManager->checkAccess('developer', $model->id)) { ?> checked="checked" <? } ?>/>
                        <?=Yii::t('cpanel', 'Разработчик');?>
                    </label>
                </div>
            </div>
            <div class="box-footer">
                <button class="btn btn-primary" type="summit"><?=Yii::t('yii', 'Submit');?></button>
            </div>
        </div>
    </form>

    <? if (!$model->getIsNewRecord()) { ?>
    <form id="modelChangePasswordForm" action="<?=$this->createUrl('/cpanel/user/changePassword', array('id'=>$model->id));?>" data-model-id="<?=$model->id;?>" method="POST">
        <div class="box box-solid box-warning collapsed-box">
            <div class="box-header">
                <h4 class="box-title"><i class="fa fa-fw fa-unlock-alt"></i>Изменить пароль</h4>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-default btn-sm" title="" data-toogle="tooltip" data-widget="collapse" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body" style="display: none">
                <div class="form-group">
                    <label for="User[password]"><?=$model->getAttributeLabel('password');?></label>
                    <input class="form-control" name="User[password]" type="password" value="<?=CHtml::encode($model->password);?>"/>
                        
                </div>
                <div class="form-group">
                    <label for="User[confirmPassword]"><?=$model->getAttributeLabel('confirmPassword');?></label>
                    <input class="form-control" name="User[confirmPassword]" type="password" value="<?=CHtml::encode($model->confirmPassword);?>"/>
                </div>
            </div>
            <div class="box-footer" style="display: none">
                <button class="btn btn-warning" type="summit"><?=Yii::t('yii', 'Submit');?></button>
            </div>
        </div>
    </form>
    <? } ?>
</div>

<? if ($model->getIsNewRecord()) { ?>
<div class="col-sm-6">
    <div class="box box-default">
        <div class="box box-body text-muted">
            <?=Yii::t('yii', 'Предоставление редактирования доступа к кампаниям будет доступно после успешного сохранения нового пользователя');?>
        </div>
    </div>
</div>
<? } else { ?>
    <? if (Yii::app()->authManager->checkAccess('admin', $model->id)) { ?>
    <div class="col-sm-6">
        <div class="box box-default view">
            <div class="box-body text-muted">
                <?=Yii::t('yii', 'Администратор пользователей так же является администратором рекламных кампаний.');?>
            </div>
        </div>
    </div>
    <? } else { ?>
    <div class="col-sm-6">
        <div class="box box-info view">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-eye"></i>Просмотр кампаний<i class="fa fa-fw fa-spinner hidden"></i></h3>
            </div>
            <div class="box-body" style="max-height: 150px; overflow: scroll;">
                <? foreach($campaigns as $c) { ?>
                <div class="checkbox">
                    <label>
                        <input class="form-control" name="AuthAssignments[user][]" type="checkbox"
                            value="<?=$c->id?>" <? if (isset($assignments['user']['campaigns'][$c->id])) { ?>checked="checked"<? } ?>/>
                        <?=$c->name?>
                    </label>
                </div>
                <? } ?>
            </div>
        </div>

        <div class="box box-info moderate">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-fw fa-edit"></i>Администрирование кампаний<i class="fa fa-fw fa-spinner hidden"></i></h3>
            </div>
            <div class="box-body" style="max-height: 150px; overflow: scroll;">
                <? foreach($campaigns as $c) { ?>
                <div class="checkbox">
                    <label>
                        <input class="form-control" name="AuthAssignments[moderator][]"
                            type="checkbox" value="<?=$c->id?>" <? if (isset($assignments['moderator']['campaigns'][$c->id])) { ?>checked="checked"<? } ?>/>
                        <?=$c->name?>
                    </label>
                </div>
                <? } ?>
            </div>
        </div>
    </div>
    <? } ?>
<? } ?>


