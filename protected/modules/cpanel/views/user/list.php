<? /* var Controller $this */ ?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-body">
                <a class="btn btn-success" href="<?=$this->createUrl('/cpanel/user/create');?>"><?=Yii::t('yii', 'Create');?></a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-default">
            <div class="box-body">
                <table class="table table-bordered table-hover datatable">
                    <thead>
                        <tr>
                            <th><?=User::model()->getAttributeLabel('id');?></th>
                            <th><?=User::model()->getAttributeLabel('username');?></th>
                            <th><?=User::model()->getAttributeLabel('email');?></th>
                            <th><?=User::model()->getAttributeLabel('created');?></th>
                            <!-- th><?=User::model()->getAttributeLabel('actions');?></th -->
                        </tr>
                    </thead>
                    <tbody>
                        <? foreach($users as $model) { ?>
                        <tr <?if ($model->activated == 0) { ?> class="text-muted" <? } ?>>
                            <td><?=CHtml::encode($model->id);?></td>
                            <td><a href="<?=$this->createUrl('/cpanel/user/update', array('id'=>$model->id));?>"><?=CHtml::encode($model->username);?></a></td>
                            <td><?=CHtml::encode($model->email);?></td>
                            <td>
                                <? if ($model->created) {
                                    echo date("Y-m-d H:i:s", $model->created);
                                } else {
                                    echo '-';
                                } ?>
                            </td>
                            <!-- td>
                                none
                            </td -->
                        </tr>
                        <? } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
