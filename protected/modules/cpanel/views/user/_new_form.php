<? /* @var Controller $this; */
$this->pageTitle = $model->getIsNewRecord() ? "Создание пользователя" : "Редактирование пользователя";


$cs = Yii::app()->clientScript;
$cs->registerCoreScript('jquery');
$cs->registerScriptFile($this->getModule()->assetsUrl . '/js/users.js');
?>

<style>
.checkbox input[type="checkbox"] {
    margin-left: 0;
    margin-right: 5px;
}

.box {
    min-height: 280px;
}

.select-user-group {
    margin-top: -20px;
}

.group-description,.group-description-title {
    display: none;
}

.group-description {
    font-size: 15px;
}
</style>
<form id="modelEditForm" action="<?=$this->createUrl('/cpanel/user/'.$this->getAction()->id, $model->getIsNewRecord() ? array() : array('id'=>$model->id));?>" data-model-id="<?=$model->id;?>" method="POST">
<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">
                    <i class="fa fa-fw fa-info-circle"></i>
                    Основная информация
                </h3>
            </div>

            <div class="box-body">
                <div class="form-group">
                    <label for="user-username">Имя пользователя</label>
                    <input class="form-control" id="user-username" name="User[username]" type="text" value="<?=CHtml::encode($model->username);?>"/>
                </div>
                <div class="form-group">
                    <label for="user-email">Email</label>
                    <input class="form-control" id="user-email" name="User[email]" type="text" value="<?=CHtml::encode($model->email);?>"/>
                </div>

                <div class="form-group">
                    <label>
                        <input type="hidden" name="User[activated]" value="<?=$model->activated?>"/>
                        <input class="flat-green" type="checkbox" name="User[activated]"
                            <? if ($model->activated == '1') { ?> checked="checked" <? } ?>/>
                        <?=Yii::t('cpanel', 'Пользователь активирован');?>
                    </label>
                </div>
            </div>

            <div class="box-footer">
                <button class="btn btn-primary" type="summit"><?=Yii::t('yii', 'Отправить');?></button>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="box box-warning ">
            <div class="box-header">
                <h4 class="box-title"><i class="fa fa-fw fa-unlock-alt"></i>Пароль</h4>
            </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="User[password]">Пароль</label>
                        <input class="form-control" name="User[password]" type="password"/>
                    </div>
                    <div class="form-group">
                        <label for="User[confirmPassword]">Повторите пароль</label>
                        <input class="form-control" name="User[confirmPassword]" type="password"/>
                    </div>
                </div>
        </div>
    </div>


    <div class="col-md-2">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <i class="fa fa-fw fa-group"></i>
                    Группа
                </h3>
            </div>

            <div class="box-body">
                <div class="form-group select-user-group">

                    <? foreach ($accessGroups as $group => $groupConfig): ?>
                        <div class="checkbox">
                            <label>
                                <input
                                       class="group-selector"
                                       type="radio"
                                       name="group"
                                       value="<?=$group?>"
                                       <?=$currentGroup == $group ? "checked" : "" ?>
                                    />
                                <?=$groupConfig['title']?>
                            </label>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <i class="fa fa-fw fa-group"></i>
                    <span class="group-description-title group-admin">Группа "Кирилл"</span>
                    <span class="group-description-title group-manager">Группа "Миша"</span>
                    <span class="group-description-title group-developer">Группа "Инвестор"</span>
                    <span class="group-description-title group-guest">Группа "Наблюдатель"</span>
                </h3>
            </div>

            <div class="box-body">
                <div class="group-description group-admin">
                    <ul>
                        <li>Просматривает все разделы;</li>
                        <li>Добавляет записи во всех разделах;</li>
                        <li>Редактирует все записи во всех разделах.</li>
                    </ul>
                </div>
                <div class="group-description group-manager">
                    <ul>
                        <li>Просматривает все разделы;</li>
                        <li>Добавляет записи во всех разделах;</li>
                        <li>Редактирует только свои записи во всех разделах.</li>
                    </ul>
                </div>
                <div class="group-description group-developer">
                    <ul>
                        <li>Просматривает все разделы отчетов.</li>
                    </ul>
                </div>
                <div class="group-description group-guest">
                    <ul>
                        <li>Просматривает все разделы отчетов, исключая отчеты, содержащие финансовую информацию.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</form>


<div class="row">

</div>

<script>
$(function() {
    $('input[name="group"]').change(function() {
        var t = $(this);
        var selector = '.group-' + t.val();

        $('.group-description').hide();
        $('.group-description-title').hide();

        $('.group-description' + selector).show();
        $('.group-description-title' + selector).show();
    });

    var selector = '.group-' + $('input[name="group"]:checked').val();

    $('.group-description').hide();
    $('.group-description-title').hide();
    $('.group-description' + selector).show();
    $('.group-description-title' + selector).show();
});
</script>


<!--
<? if ($model->getIsNewRecord()) { ?>
    <div class="col-sm-6">
        <div class="box box-default">
            <div class="box box-body text-muted">
                <?=Yii::t('yii', 'Предоставление редактирования доступа к кампаниям будет доступно после успешного сохранения нового пользователя');?>
            </div>
        </div>
    </div>
<? } else { ?>
    <? if (Yii::app()->authManager->checkAccess('admin', $model->id)) { ?>
        <div class="col-sm-6">
            <div class="box box-default view">
                <div class="box-body text-muted">
                    <?=Yii::t('yii', 'Администратор пользователей так же является администратором рекламных кампаний.');?>
                </div>
            </div>
        </div>
    <? } else { ?>
        <div class="col-sm-6">
            <div class="box box-info view">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-fw fa-eye"></i>Просмотр кампаний<i class="fa fa-fw fa-spinner hidden"></i></h3>
                </div>
                <div class="box-body" style="max-height: 150px; overflow: scroll;">
                    <? /*foreach($campaigns as $c) { ?>
                        <div class="checkbox">
                            <label>
                                <input class="form-control" name="AuthAssignments[user][]" type="checkbox"
                                       value="<?=$c->id?>" <? if (isset($assignments['user']['campaigns'][$c->id])) { ?>checked="checked"<? } ?>/>
                                <?=$c->name?>
                            </label>
                        </div>
                    <? }*/ ?>
                </div>
            </div>

            <div class="box box-info moderate">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-fw fa-edit"></i>Администрирование кампаний<i class="fa fa-fw fa-spinner hidden"></i></h3>
                </div>
                <div class="box-body" style="max-height: 150px; overflow: scroll;">
                    <? /*foreach($campaigns as $c) { ?>
                        <div class="checkbox">
                            <label>
                                <input class="form-control" name="AuthAssignments[moderator][]"
                                       type="checkbox" value="<?=$c->id?>" <? if (isset($assignments['moderator']['campaigns'][$c->id])) { ?>checked="checked"<? } ?>/>
                                <?=$c->name?>
                            </label>
                        </div>
                    <? }*/ ?>
                </div>
            </div>
        </div>
    <? } ?>
<? } ?>
-->

