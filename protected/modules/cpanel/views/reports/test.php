<?
use Report\Filter\ReportFilterContent;
use Report\Filter\ReportFilterSelectedValues;

/* @var Controller $this */
/* @var array $tableHeadTitle */
/* @var array $table */
/* @var array $bodyRowsTitle */
/* @var array $tableSumm */
/* @var array $results */
/* @var array $columns */
/* @var array $total */
/* @var ReportFilterContent $ReportFilterContent */
/* @var ReportFilterSelectedValues $ReportFilterSelected */

$cs = Yii::app()->getClientScript();
$cs->registerCssFile('/css/datatables/dataTables.bootstrap.css');
$cs->registerCoreScript('jquery');
$cs->registerScriptFile('/js/plugins/datatables/jquery.dataTables.js');
$cs->registerScriptFile('/js/plugins/datatables/dataTables.bootstrap.js');
$cs->registerScriptFile('/js/plugins/daterangepicker/daterangepicker.js');

if (isset($_GET['details'])) {
    $this->pageTitle = $_GET['details'];
}

?>
<style>
.content {
    padding: 8px;
}

.row {
    margin-right: 0;
    margin-left: 0;
}

.btn.btn-default.collapse-button {
    padding-bottom: 5px!important;
    /*margin: 5px!important;*/
    /*padding: 5px!important 13px!important;*/
}

.box-header {
    margin-left: -1px;
}

.box-body {
    overflow: auto;
}

.no-padding-col-xs {
    padding: 0 4px 0 0;
}

.padding-filter-row {
    padding-top: 5px;
}

.filter-rows {
    display: inline-block;
}

.add-filter-row {
    vertical-align: middle;
    display: inline-block;
    margin-top: -25px;
}

.big-right-border {
    border-right-width: 2px!important;
}

.big-left-border {
    border-right-width: 2px!important;
}

.text-center {
    text-align: center;
}

.multifield_utm {
    height: 230px!important;
}

input[type="checkbox"] {
    margin-left: 5px!important;
    margin-right: 5px!important;
}

.daterangepicker {
    left: 100px!important;
    right: auto!important;
}

.btn-controller {
    width: 170px;
    text-align: left;
    font-size: 12px;
}
</style>

<div class="row">
    <form action="" id="filter">
        <div class="col-md-12">
            <div class="nav-tabs-custom box box-solid" >
                <div class="box-header">
                    <div>
                        <ul class="nav nav-tabs">
                            <? if (!isset($_GET['details'])) : ?>
                                <li class="active">
                                    <a href="#tab_1" data-toggle="tab">
                                        <i class="fa fa-fw fa-book"></i>
                                        Базовые
                                    </a>
                                </li>
                                <li style="margin-left: -2px;">
                                    <a href="#tab_2" data-toggle="tab">
                                        <i class="fa fa-fw fa-plus"></i>
                                        Расширенные
                                    </a>
                                </li>
                            <? endif; ?>

                            <? if (isset($_GET['details'])) : ?>
                                <li class="active">
                                    <a href="#tab_1" data-toggle="tab">
                                        <i class="fa fa-fw fa-book"></i>
                                        Базовые
                                    </a>
                                </li>
                            <? endif; ?>

                            <li class="pull-right collapse-button box-tools">
                                <a href="javascript:void(0);"
                                   class="btn btn-default btn-sm"
                                   data-widget="collapse"
                                   style="border-left: 1px solid #EEEEEE;
                                          margin: 0;
                                          padding-bottom: 12px;
                                          padding-top: 13px;
                                          border-bottom: none;
                                          border-top: none;
                                          border-right: none;
                                          box-shadow: none;
                                    ">
                                    <i class="fa fa-minus"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="tab-content box-body">
                    <? if (!isset($_GET['details'])) : ?>
                    <div class="tab-pane active" id="tab_1" >
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="site-form">Сайт</label>
                                        <select id="site-form" class="form-control" name="site">
                                            <? foreach ($ReportFilterContent->site as $site => $siteTitle) : ?>
                                                <option value="<?=$site?>"
                                                        <?=$ReportFilterSelected->site == $site ? 'selected' : ''?>
                                                >
                                                <?=$siteTitle?>
                                                </option>
                                            <? endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label for="reservation">Дата отчета:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"
                                                   class="form-control pull-right"
                                                   id="reservation"
                                                   value="<?=$ReportFilterContent->getDateIntervalString()?>">
                                        </div><!-- /.input group -->
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 4px;">
                                    <a href="#" class="btn btn-success btn-apply-filter btn-controller">
                                        <i class="fa fa-fw fa-check"></i>
                                        Применить фильтр
                                    </a>
                                </div>

                                <div class="row" style="margin-top: 4px;">
                                    <a href="#" class="btn btn-primary export btn-controller">
                                        <i class="fa fa-fw fa-share"></i>
                                        Экспорт в csv
                                    </a>
                                </div>

                                <div class="row" style="margin-top: 4px;">
                                    <a href="?r=cpanel/reports" class="btn btn-danger btn-controller">
                                        <i class="fa fa-fw fa-times"></i>
                                        Очистить фильтры
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row">
                                    <div>
                                        <label for="reservation">Столбцы для отображения</label>
                                        <div class="form-group">
                                            <?
                                            $numberColumn = 0;
                                            foreach($ReportFilterContent->hiddenColumns as $column => $columnConfig) : ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox"
                                                               class="toggle-vis"
                                                               data-column="<?=$numberColumn++?>"
                                                               value="<?=$column?>"
                                                               name="show-column[]"
                                                               <?=isset($columnConfig['canHide']) && !$columnConfig['canHide'] ? 'disabled checked' : '' ?>
                                                               <?=$ReportFilterSelected->isCheckedHidden($column) ? "checked" : "" ?>

                                                            />
                                                        <?=$columnConfig['title']?>
                                                    </label>
                                                </div>
                                            <? endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="row">
                                    <div class="form-group">
                                        <label>utm_campaign</label>
                                        <select multiple class="form-control multifield_utm" id="m_campaign" name="utm_campaign[]">
                                            <? foreach ($ReportFilterContent->utmCampaign as $utm_campaign) : ?>
                                                <option value="<?=$utm_campaign?>"
                                                    <?=in_array($utm_campaign, $ReportFilterSelected->utmCampaign) ? "selected" : ""?>
                                                    >
                                                    <?=$utm_campaign?>
                                                </option>
                                            <? endforeach; ?>
                                        </select>
                                        <a href="javascript: $('#m_campaign').children('option:selected').prop('selected', false); void(0);">
                                            <i class="fa fa-fw fa-times"></i>
                                            Снять выделение
                                        </a>
                                        <a href="javascript: void(0);"
                                           class="pull-right "
                                           data-widget="collapse"
                                           data-toggle="tooltip"
                                           title=""
                                           data-original-title="Если не выбран ни один элемент, отображаются все записи">
                                            <i class="fa fa-fw fa-question-circle"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="row">
                                    <div class="form-group">
                                        <label>utm_source</label>

                                        <select multiple class="form-control multifield_utm" id="m_source" name="utm_source[]">
                                            <? foreach ($ReportFilterContent->utmSource as $utm_source) : ?>
                                                <option value="<?=$utm_source?>"
                                                        <?=in_array($utm_source, $ReportFilterSelected->utmSource) ? "selected" : ""?>
                                                    >
                                                    <?=$utm_source?>
                                                </option>
                                            <? endforeach; ?>
                                        </select>
                                        <a href="javascript: $('#m_source').children('option:selected').prop('selected', false); void(0);">
                                            <i class="fa fa-fw fa-times"></i>
                                            Снять выделение
                                        </a>
                                        <a href="javascript: void(0);"
                                           class="pull-right "
                                           data-widget="collapse"
                                           data-toggle="tooltip"
                                           title=""
                                           data-original-title="Отчет фильтруется по выделенным элементам списка">
                                            <i class="fa fa-fw fa-question-circle"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="row">
                                    <div class="form-group">
                                        <label>utm_medium</label>
                                        <select multiple class="form-control multifield_utm" id="m_medium" name="utm_medium[]">
                                            <? foreach ($ReportFilterContent->utmMedium as $utm_medium) : ?>
                                                <option value="<?=$utm_medium?>"
                                                    <?=in_array($utm_medium, $ReportFilterSelected->utmMedium) ? "selected" : ""?>
                                                    >
                                                    <?=$utm_medium?>
                                                </option>
                                            <? endforeach; ?>
                                        </select>
                                        <a href="javascript: $('#m_medium').children('option:selected').prop('selected', false); void(0);">
                                            <i class="fa fa-fw fa-times"></i>
                                            Снять выделение
                                        </a>
                                        <a href="javascript: void(0);"
                                           class="pull-right "
                                           data-widget="collapse"
                                           data-toggle="tooltip"
                                           title=""
                                           data-original-title="Если выбрать элементы из разных списков, то будут отображены все записи имеющие хотя бы одно вхождение из каждого списка">
                                            <i class="fa fa-fw fa-question-circle"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="tab-pane" id="tab_2">
                        <div class="col-md-8">
                            <div class="row">
                                <label>Показывать запись если:</label>
                                <div class="form-group">
                                    <div>
                                        <div class="filter-rows">
                                            <?
                                            $i = 0;
                                            do {
                                                $filterColumn    = isset($ReportFilterSelected->filterColumn[$i])    ? $ReportFilterSelected->filterColumn[$i] : null;
                                                $filterOperation = isset($ReportFilterSelected->filterOperation[$i]) ? $ReportFilterSelected->filterOperation[$i] : null;
                                                $filterValue     = isset($ReportFilterSelected->filterValue[$i])     ? $ReportFilterSelected->filterValue[$i] : null;

                                                if ($filterValue == null && $i > 0) {
                                                    continue;
                                                }
                                            ?>
                                                <div class="row padding-filter-row <?=$i == 0 ? 'first-example-row' : ''?>">
                                                <div class="col-xs-6 no-padding-col-xs">
                                                    <select class="form-control filter-column" name="filter-column[]">
                                                        <option value="">Выбрать событие</option>
                                                        <? foreach ($ReportFilterContent->rowValue as $site => $rowValueConfig) : ?>
                                                            <option value="<?=$site?>"
                                                                    <?=$filterColumn == $site ? 'selected' : '' ?>
                                                                >
                                                                <?=$rowValueConfig['title']?>
                                                            </option>
                                                        <? endforeach; ?>
                                                    </select>
                                                </div>

                                                <div class="col-xs-2 no-padding-col-xs">
                                                    <select class="form-control filter-operation" name="filter-operation[]">
                                                        <option value="large" <?=$filterOperation == 'large' ? 'selected' : '' ?>> > </option>
                                                        <option value="small" <?=$filterOperation == 'small' ? 'selected' : '' ?>> < </option>
                                                    </select>
                                                </div>

                                                <div class="col-xs-4 no-padding-col-xs">
                                                    <input class="form-control filter-value" type="text" value="<?=$filterValue?>" name="filter-value[]"/>
                                                </div>
                                            </div>
                                            <? } while(++$i < sizeof($ReportFilterSelected->filterColumn));  ?>
                                        </div>

                                        <div class="add-filter-row">
                                            <a href="javascript: void(0);" id="add-row-filter">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 4px;">
                                <a href="#" class="btn btn-success btn-apply-filter btn-controller">
                                    <i class="fa fa-fw fa-check"></i>
                                    Применить фильтр
                                </a>
                            </div>

                            <div class="row" style="margin-top: 4px;">
                                <a href="#" class="btn btn-primary export btn-controller">
                                    <i class="fa fa-fw fa-share"></i>
                                    Экспорт в csv
                                </a>
                            </div>

                            <div class="row" style="margin-top: 4px;">
                                <a href="?r=cpanel/reports" class="btn btn-danger btn-controller">
                                    <i class="fa fa-fw fa-times"></i>
                                    Очистить фильтры
                                </a>
                            </div>
                        </div>

                    </div>
                    <? endif; ?>

                    <? if (isset($_GET['details'])) : ?>
                    <input type="hidden" name="details" value="<?=$_GET['details']?>"/>
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="site-form">Сайт</label>
                                        <select id="site-form" class="form-control" name="site">
                                            <? foreach ($ReportFilterContent->site as $site => $siteTitle) : ?>
                                                <option value="<?=$site?>"
                                                    <?=$ReportFilterSelected->site == $site ? 'selected' : ''?>
                                                    >
                                                    <?=$siteTitle?>
                                                </option>
                                            <? endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label for="reservation">Дата отчета:</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text"
                                                   class="form-control pull-right"
                                                   id="reservation"
                                                   value="<?=$ReportFilterContent->getDateIntervalString()?>">
                                        </div><!-- /.input group -->
                                    </div>
                                </div>

                                <div class="row">
                                    <label for="interval-select">Интервал:</label>
                                    <select class="form-control" id="interval-select" name="interval">
                                        <option value="1d"  <?=isset($_GET['interval']) && $_GET['interval'] == '1d' ? "selected" : ""?>>1 день</option>
                                        <option value="7d"  <?=isset($_GET['interval']) && $_GET['interval'] == '7d' ? "selected" : ""?>>1 неделя</option>
                                        <option value="30d" <?=isset($_GET['interval']) && $_GET['interval'] == '30d' ? "selected" : ""?>>1 месяц</option>
                                    </select>
                                </div>

                                <div class="row" style="margin-top: 15px;">
                                    <div class="row" style="margin-top: 4px;">
                                        <a href="#" class="btn btn-success btn-apply-filter btn-controller">
                                            <i class="fa fa-fw fa-check"></i>
                                            Применить фильтр
                                        </a>
                                    </div>

                                    <div class="row" style="margin-top: 4px;">
                                        <a href="#" class="btn btn-primary export btn-controller">
                                            <i class="fa fa-fw fa-share"></i>
                                            Экспорт в csv
                                        </a>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-2">
                                <div class="row">
                                    <div>
                                        <label for="reservation">Столбцы для отображения</label>
                                        <div class="form-group">
                                            <?
                                            $numberColumn = 0;
                                            foreach($ReportFilterContent->hiddenColumns as $column => $columnConfig) : ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox"
                                                               class="toggle-vis"
                                                               data-column="<?=$numberColumn++?>"
                                                               value="<?=$column?>"
                                                               name="show-column[]"
                                                            <?=isset($columnConfig['canHide']) && !$columnConfig['canHide'] ? 'disabled checked' : '' ?>
                                                            <?=$ReportFilterSelected->isCheckedHidden($column) ? "checked" : "" ?>

                                                            />
                                                        <?=$columnConfig['title']?>
                                                    </label>
                                                </div>
                                            <? endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box box-solid" >
            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead></thead>
                    <tbody></tbody>
                    <tfoot></tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
var __table;
$(function() {
    var data    = <?=$results?>;
    var columns = <?=$columns?>;
    var total   = <?=$total?>;

    renderTable(data, columns, total);

    // Обработчик добавить элемент в раздел "Показывать запись если"
    $('#add-row-filter').click(function() {
        var $filterRow = $('.first-example-row').clone();

        $filterRow.removeClass('first-example-row');
        $('.form-control,.filter-column,.filter-operation,.filter-value', $filterRow).attr('value', '');

        $('.filter-rows').append($filterRow);
    });

    // Обработчик кнопки "применить"
    $('.btn-apply-filter').click(function() {
        var $form         = $( "form" );
        var serializeForm = $form.serialize();

        var time = $('#reservation').val().split(' - ');
        var from = strtotime(time[0]);
        var to   = strtotime(time[1]);

        serializeForm += "&from=" + from + "&to=" + to;
        location.href = '/index.php?r=cpanel/reports&' + serializeForm;

        console.log(serializeForm);

        return false;
    });

    // Выбор даты
    $('#reservation').daterangepicker({
        format: 'DD.MM.YYYY',
        locale: {
            applyLabel: 'Принять',
            cancelLabel: 'Отменить',
            fromLabel: 'От',
            toLabel: 'До',
            weekLabel: 'Н',
            customRangeLabel: 'По умолчанию',
            firstDay: 1,
            daysOfWeek: ['Вс', 'Пн','Вт','Ср','Чт','Пт','Сб'],
            monthNames: ['Янв', 'Фев','Март','Апр','Май','Июнь','Июль','Авг','Сент','Окт','Ноя','Дек']
        }
    });

    // Отображение столбцов
    $('.toggle-vis').change(function() {
        updateVisibleColumns();
    });
    updateVisibleColumns();

    // Экспорт в CSV
    $(".export").on('click', function (event) {
        exportTableToCSV.apply(this, [$('#example1'), 'export_' + ($('#reservation').val()) + '.csv']);
    });
});

/**
 * Обновить видимость колонок в зависимости от DOM'а
 */
function updateVisibleColumns() {
    $('.toggle-vis').each(function() {
        var $this = $(this);
        var column = __table.column($this.data('column'));

        column.visible( $this.is(':checked') );
    });
}

/**
 * Строка в UnixTime
 * @param string
 * @returns {number}
 */
function strtotime(string) {
    var time;

    time = string.split('.');
    time = new Date(parseInt(time[2]), parseInt(time[1]) - 1, parseInt(time[0]));

    return Math.round(time.getTime()/1000);
}

/**
 * Рендер таблички
 * @param data
 * @param columns
 * @param total
 */
function renderTable(data, columns, total) {
    // Футер таблицы
    var column;
    var footer = '<tr>';
    for (var i in columns) {
        column = columns[i]['data'];

        footer += '<th>' + total[column] + '</th>';
    }
    footer += '</tr>';

    var table = $('#example1');
    table.find('tfoot').html(footer);

    __table = table.DataTable({
        "paginate": false,
        "search": false,
        "bFilter": false,
        data: data,
        columns: columns,
        "order": [[ 1, "desc" ]]
    });
}

/**
 * Экспорт в CSV
 * @param $table
 * @param filename
 */
function exportTableToCSV($table, filename) {
    var $rows = $table.find('tr:has(th),tr:has(td)'),

    // Temporary delimiter characters unlikely to be typed by keyboard
    // This is to avoid accidentally splitting the actual contents
        tmpColDelim = String.fromCharCode(11), // vertical tab character
        tmpRowDelim = String.fromCharCode(0), // null character

    // actual delimiter characters for CSV format
        colDelim = '","',
        rowDelim = '"\r\n"',

    // Grab text from table into CSV formatted string
        csv = '"' + $rows.map(function (i, row) {
            var $row = $(row),
                $cols = $row.find('td,th');

            return $cols.map(function (j, col) {
                var $col = $(col),
                    text = $col.text();

                return text.replace('"', '""'); // escape double quotes

            }).get().join(tmpColDelim);

        }).get()
          .join(tmpRowDelim)
          .split(tmpRowDelim).join(rowDelim)
          .split(tmpColDelim).join(colDelim) + '"',

    // Data URI
    csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

    $(this)
        .attr({
            'download': filename,
            'href': csvData,
            'target': '_blank'
        });
}

</script>