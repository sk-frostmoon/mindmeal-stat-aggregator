<?
/* @var Controller $this */
/* @var array $tableHeadTitle */
/* @var array $table */
/* @var array $bodyRowsTitle */
/* @var array $tableSumm */

$cs = Yii::app()->getClientScript();
$cs->registerCssFile('/css/datatables/dataTables.bootstrap.css');
$cs->registerCoreScript('jquery');
$cs->registerScriptFile('/js/plugins/datatables/jquery.dataTables.js');
$cs->registerScriptFile('/js/plugins/datatables/dataTables.bootstrap.js');
$cs->registerScriptFile('/js/plugins/daterangepicker/daterangepicker.js');

$filter = isset($_GET['filter']) ? $_GET['filter'] : '';
$mode   = isset($_GET['mode'])   ? $_GET['mode'] : '';
$this->pageTitle = 'Отчет ' . $filter;
?>

<style>
    .table-responsive {
        overflow-x: hidden;
    }

    table * {
        text-align: center;
    }

    .text-left {
        text-align: left;
    }
    .text-right {
        text-align: right;
    }

    .box {
        min-width: 930px;
    }

    .box .navigate a {
        margin-left: 10px;
        margin-right: 10px;
        color: #000099;
        text-decoration: underline;
    }

    .box .navigate a:hover {
        text-decoration: none;
    }

    .box .navigate a.inactive {
        color: #808080;
        text-decoration: none;
        cursor: default;
    }

    .box-title {
        margin-left: 7px;
    }

    .daterangepicker {
        width: 700px;
    }

    .apply {
        margin-left: 7px;
        margin-top: 5px;
    }

    #example1 {
        width: 100%!important;
    }
</style>

<div class="box">
    <div class="header">
<!--        <h3 class="box-title">Таблица отчета</h3>-->

        <div class="row" style="padding-top: 5px; padding-left: 5px;">
            <div class="col-md-3">
                <!-- select -->
                <div class="form-group" style="margin-left: 7px;">
                    <label for="mode-select">Режим:</label>
                    <select class="form-control" id="mode-select">
                        <? foreach($queries as $query => $queryTitle): ?>
                        <option value="<?=$query?>" <?=isset($_GET['mode']) && $_GET['mode'] == $query ? 'selected' : ''?>>
                            <?=$queryTitle?>
                        </option>
                        <? endforeach; ?>
                    </select>
                </div>

                <!-- Date range -->
                <div class="form-group" style="margin-left: 7px;">
                    <label for="reservation">Дата отчета:</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="reservation" value="<?=$dateFrom?> - <?=$dateTo?>" />
                    </div><!-- /.input group -->
                </div>
                <div style="margin-left: 7px;"><a href="#" class="btn btn-success export">Экспорт в csv</a></div>
            </div>

            <div class="col-md-2">
                <label for="reservation">Столбцы для отображения</label>
                <div class="form-group" style="margin-left: 20px;">
                    <? $count = 0; ?>
                    <? foreach ($columnsTableArray as $a) :?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"
                                   class="toggle-vis"
                                   data-column="<?=($count++)?>" <?=isset($a['canHide']) && !$a['canHide'] ? 'disabled' : '' ?>
                                   <?= isset($a['isHide']) && $a['isHide'] ? '' : 'checked' ?>
                                />
                            <?=$a['title'] ?>
                        </label>
                    </div>
                    <? endforeach; ?>

                </div>
            </div>

            <div class="col-md-2">
                <? /*if ($filter): ?>
                    <div style="margin-left: 7px;">
                        <label for="interval-select">Интервал:</label>
                        <select class="form-control" id="interval-select">
                            <option value="1d" <?=$interval == '1d' ? "selected" : ''?>>1 день</option>
                            <option value="7d" <?=$interval == '7d' ? "selected" : ''?>>1 неделя</option>
                            <option value="30d" <?=$interval == '30d' ? "selected" : ''?>>1 месяц</option>
                        </select>
                    </div>
                <? endif;*/ ?>
            </div>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body table-responsive">
        <h3>Всего: <?=$totalRows?></h3>
        <table id="example1" class="table table-bordered table-striped">
            <thead></thead>
            <tbody></tbody>
            <tfoot></tfoot>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->

<script type="text/javascript">
    var data    = <?=$dataTable?>;
    var columns = <?=$columnsTable?>;
    var __tableData = null;

    $(function() {
        __tableData = $('#example1').DataTable({
            "paginate": false,
            "search": false,
            "bFilter": false,
            data: data,
            columns: columns,
            "order": [[ 0, "desc" ]]
        });

        // Выбор режима
        $("#mode-select").change(function() {
            var mode = $("#mode-select").val();
            window.location.search = '?r=cpanel/reports/details&mode=' + mode + '&from=<?=$timeFrom?>&to=<?=$timeTo?>&filter=<?=$filter?>';
        });

        // Выбор даты
        $('#reservation').daterangepicker({
            format: 'DD.MM.YYYY',
            locale: {
                applyLabel: 'Принять',
                cancelLabel: 'Отменить',
                fromLabel: 'От',
                toLabel: 'До',
                weekLabel: 'Н',
                customRangeLabel: 'По умолчанию',
                firstDay: 1,
                daysOfWeek: ['Вс', 'Пн','Вт','Ср','Чт','Пт','Сб'],
                monthNames: ['Янв', 'Фев','Март','Апр','Май','Июнь','Июль','Авг','Сент','Окт','Ноя','Дек'],
            }
        });

        $('.toggle-vis').change(function() {
            var $this = $(this);
            var column = __tableData.column($this.data('column'));

            column.visible( $this.is(':checked') );
        });


        $('.applyBtn').click(function() {
            setTimeout(function(){
                var time = $('#reservation').val().split(' - ');

    //                console.log(time);

                var timeFrom = time[0].split('.');
                timeFrom = {
                    day: timeFrom[0],
                    month: timeFrom[1],
                    year: timeFrom[2],
                };

                var timeTo = time[1].split('.');
                timeTo = {
                    day: timeTo[0],
                    month: timeTo[1],
                    year: timeTo[2],
                };

                var dateFrom = new Date(parseInt(timeFrom.year), parseInt(timeFrom.month) - 1, parseInt(timeFrom.day));
                var dateTo   = new Date(parseInt(timeTo.year), parseInt(timeTo.month) - 1, parseInt(timeTo.day));

                timeFrom = Math.round(dateFrom.getTime()/1000);
                timeTo = Math.round(dateTo.getTime()/1000);

                var search = window.location.search.slice(1).split('&');
                var new_search = [];
                var findFrom = false, findTo = false;
                for (var i in search) {
                    var pieceSearch = search[i].split('=');
                    if (pieceSearch[0] == 'from') {
                        pieceSearch[1] = timeFrom;
                        findFrom = true;
                    }
                    if (pieceSearch[0] == 'to') {
                        pieceSearch[1] = timeTo;
                        findTo = true;
                    }

                    new_search.push(pieceSearch[0] + '=' + pieceSearch[1]);
                }

                if (!findFrom) {
                    new_search.push('from=' + timeFrom);
                }

                if (!findTo) {
                    new_search.push('to=' + timeTo);
                }

                new_search = '?' + new_search.join('&');
                window.location.search = new_search;
            }, 100);
        });

        function exportTableToCSV($table, filename) {

            var $rows = $table.find('tr:has(th),tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
                colDelim = '","',
                rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                    var $row = $(row),
                        $cols = $row.find('td,th');

                    return $cols.map(function (j, col) {
                        var $col = $(col),
                            text = $col.text();

                        return text.replace('"', '""'); // escape double quotes

                    }).get().join(tmpColDelim);

                }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"',

            // Data URI
                csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
                .attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank'
                });
        }

        // This must be a hyperlink
        $(".export").on('click', function (event) {
            // todo refac
            var column = __tableData.column(8);

            column.visible( false );
            exportTableToCSV.apply(this, [$('#example1'), 'export_' + ($('#reservation').val()) + '.csv']);
            column.visible( true );

            // IF CSV, don't do event.preventDefault() or return false
            // We actually need this to be a typical hyperlink
        });
    });
</script>