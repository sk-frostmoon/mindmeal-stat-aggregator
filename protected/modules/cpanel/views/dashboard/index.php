<? /* @var Controller $this */

$cs = Yii::app()->getClientScript();
$cs->registerCssFile('/css/datatables/dataTables.bootstrap.css');
$cs->registerCoreScript('jquery');
$cs->registerScriptFile('/js/plugins/datatables/jquery.dataTables.js');
$cs->registerScriptFile('/js/plugins/datatables/dataTables.bootstrap.js');
?>

    <div class="box box-info collapsed-box" id="__1">
        <div class="box-header">
            <h3 class="box-title">Пользовательские агенты</h3>
            <div class="pull-right box-tools">                                        
                <button class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body" style="display: none;">
            <table id="ovBrowser" class="table table-bordered table-hover">
            <thead>
                <? $this->beginClip('table_ov_thead'); ?>
                <tr>
                    <th>Browser</th>
                    <th>Count</th>
                </tr>
                <? $this->endClip(); ?>
                <?=$this->clips['table_ov_thead'];?>
            </thead>
            <tbody>
                <? foreach (OvBrowser::model()->findAll(array("order"=>"count")) as $model) { ?>
                <tr>
                    <td><?=$model->browser?></td>
                    <td><?=$model->count?></td>
                </tr>
                <? } ?>
            <tbody>
            <tfoot>
                <?=$this->clips['table_ov_thead'];?>
            </tfoot>
            </table>
        </div>
    </div>

    <div class="box box-info collapsed-box" id="__2">
        <div class="box-header">
            <h3 class="box-title">Операционные системы</h3>
            <div class="pull-right box-tools">                                        
                <button class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body" style="display: none;">
            <table id="ovOs" class="table table-bordered table-hover">
            <thead>
                <? $this->beginClip('table_ov_thead'); ?>
                <tr>
                    <th>OS</th>
                    <th>Count</th>
                </tr>
                <? $this->endClip(); ?>
                <?=$this->clips['table_ov_thead'];?>
            </thead>
            <tbody>
                <? foreach (OvOs::model()->findAll(array("order"=>"count")) as $model) { ?>
                <tr>
                    <td><?=$model->os?></td>
                    <td><?=$model->count?></td>
                </tr>
                <? } ?>
            <tbody>
            <tfoot>
                <?=$this->clips['table_ov_thead'];?>
            </tfoot>
            </table>
        </div>
    </div>

    <div class="box box-info collapsed-box" id="__3">
        <div class="box-header">
            <h3 class="box-title">Геопозиция</h3>
            <div class="pull-right box-tools">                                        
                <button class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body" style="display: none;">
            <table id="ovGeo" class="table table-bordered table-hover">
            <thead>
                <? $this->beginClip('table_ov_thead'); ?>
                <tr>
                    <th>Country Name</th>
                    <th>City</th>
                    <th>Count</th>
                </tr>
                <? $this->endClip(); ?>
                <?=$this->clips['table_ov_thead'];?>
            </thead>
            <tbody>
                <? foreach (OvGeo::model()->findAll(array('order'=>'count, country_name, city_name')) as $model) { ?>
                <tr>
                    <td><?=$model->country_name?></td>
                    <td><?=$model->city_name?></td>
                    <td><?=$model->count?></td>
                </tr>
                <? } ?>
            <tbody>
            <tfoot>
                <?=$this->clips['table_ov_thead'];?>
            </tfoot>
            </table>
        </div>
    </div>

    <div class="box box-info collapsed-box">
        <div class="box-header">
            <h3 class="box-title">Разрешение экрана</h3>
            <div class="pull-right box-tools">                                        
                <button class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body" style="display: none;">
            <table id="ovScreen" class="table table-bordered table-hover">
            <thead>
                <? $this->beginClip('table_ov_thead'); ?>
                <tr>
                    <th>Screen Resolution</th>
                    <th>Count</th>
                </tr>
                <? $this->endClip(); ?>
                <?=$this->clips['table_ov_thead'];?>
            </thead>
            <tbody>
                <? foreach (OvScreen::model()->findAll(array("order" => "CONVERT(substr(screen, 1, instr(screen, 'x') - 1), UNSIGNED INTEGER) DESC,".
                                "CONVERT(substr(screen, instr(screen, 'x')+1), UNSIGNED INTEGER) DESC, count")) as $model) { ?>
                <tr>
                    <td><?=$model->screen?></td>
                    <td><?=$model->count?></td>
                </tr>
                <? } ?>
            <tbody>
            <tfoot>
                <?=$this->clips['table_ov_thead'];?>
            </tfoot>
            </table>
        </div>
    </div>

<script type="text/javascript">
    console.log($.fn.dataTable.defaults, $.fn.dataTableExt);
    $.fn.dataTableExt.oSort['resolution-asc'] = function (x,y) {
        var xa = (x=='none') ? ['0','0'] : x.split(/x/);
        var ya = (y=='none') ? ['0','0'] : y.split(/x/);
        xa[0] = parseInt(xa[0]);
        xa[1] = parseInt(xa[1]);
        ya[0] = parseInt(ya[0]);
        ya[1] = parseInt(ya[1]);
        if ((xa[0] > ya[0])) {
            return false;
        }
        if ((xa[0] == ya[0]) && (xa[1] > ya[1])) {
            return false;
        }

        return true;
    }
    $.fn.dataTableExt.oSort['resolution-desc'] = function (x,y) {
        var xa = (x=='none') ? ['0','0'] : x.split(/x/);
        var ya = (y=='none') ? ['0','0'] : y.split(/x/);
        xa[0] = parseInt(xa[0]);
        xa[1] = parseInt(xa[1]);
        ya[0] = parseInt(ya[0]);
        ya[1] = parseInt(ya[1]);
        if ((xa[0] > ya[0])) {
            return true;
        }
        if ((xa[0] == ya[0]) && (xa[1] > ya[1])) {
            return true;
        }

        return false;
    }
    $.extend( true, $.fn.dataTable.defaults, {
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '',
                "sNext": '',
            }
        },
    });
    $('#ovBrowser, #ovOs, #ovGeo').dataTable({
            "bPagination": true,
            "bAutoWidth": true,
    });
    $('#ovScreen').dataTable({
            "bPagination": true,
            "bAutoWidth": true,
            "aoColumns": [
                { "sType": "resolution" },
                null,
            ],
    });
</script>
