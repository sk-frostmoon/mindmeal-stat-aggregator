<? /* @var Controller $this */

$this->pageTitle = Yii::t('cpanel', 'Campaigns details');
?>
<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Клики</h3>
            </div>
            <div class="box-body">
                <center><h1><i class="fa fa-fw fa-arrow-circle-up" style="color: #00A65A;"></i><?=number_format($model->clicks, 0, '', ' ');?></h1></center>
            </div>
        </div>
    </div>
</div>
