<?
/* @var Controller $this */
/* @var AdCampaign $Ad */
/* @var AdCategory $Category */
use Ad\AdCampaign;
use Ad\Category\AdCategory;

$cs = Yii::app()->getClientScript();
$cs->registerCssFile('/css/datatables/dataTables.bootstrap.css');
$cs->registerCoreScript('jquery');
$cs->registerScriptFile('/js/plugins/daterangepicker/daterangepicker.js');

$cs->registerScriptFile('/js/plugins/input-mask/jquery.inputmask.js');
$cs->registerScriptFile('/js/plugins/input-mask/jquery.inputmask.date.extensions.js');
$cs->registerScriptFile('/js/plugins/input-mask/jquery.inputmask.extensions.js');

$fields = AdCampaign::getProperties();
//

$params = $Ad->isNew() ?
    array(
        'category' => $Category->id,
    ) :
    array(
        'adId'     => $Ad->id,
        'category' => $Category->id,
    );
?>
<style>
.form-group label {
    min-width: 150px;
}
.form-group input {
    display: inline-block;
    width: 75%;
}
.form-group input.params {
    width: 292px;
    min-width: 292px;
    margin-top: 5px;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.428571429;
    color: #555;
    vertical-align: middle;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
}

#add-field {
    display: inline-block;
    vertical-align: middle;
    margin-top: 6px;
    font-size: 1.2em;
}

#data-fields {
    display: inline-block;
    vertical-align: middle;
}

</style>

<div class="row">
    <div class="col-md-offset-3 col-md-6">
        <div class="box box-primary">
            <div class="box-header">

            </div>
            <form role="form" method="POST" action="<?=$this->createUrl('', $params)?>" enctype="multipart/form-data">
                <div class="box-body">
                    <? if (!$Ad->isNew()):?>
                        <div class="form-group">
                            <label for="ad_click_url">Click URL:</label>
                            <input style="width: 350px;"
                                   id="ad_click_url"
                                   class="form-control"
                                   type="text"
                                   value="http://ad.mindmeal.ru/ad/<?=$Ad->hashId?>"
                                   data-url="/ad/<?=$Ad->hashId?>"
                                />

                            <select class="form-control" id="url_select_domain" style="width: 241px; display: inline-block;" disabled>
                                <option>ad.mindmeal.ru</option>
                                <option>solgame.ru</option>
                                <option>playfd.ru</option>
                                <option>finaldesire.ru</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ad_image_url">Image URL:</label>
                            <input id="ad_image_url"
                                   class="form-control"
                                   type="text"
                                   value="http://ad.mindmeal.ru/img/<?=$Ad->hashId?>"
                                />
                        </div>
                    <? endif; ?>
                    <? foreach ($fields as $field => $properties): ?>
                        <? if ($Ad->isNew() && !$properties['isAddable']) {continue; }?>

                        <? $value = isset($Ad->$field) ? $Ad->$field : ''; ?>

                        <?if ($field == 'createTime'): ?>
                            <? $value = date('d.m.Y H:i:s', $value); ?>
                        <? endif; ?>

                        <div class="form-group">
                            <? if ($properties['type'] == 'int' || $properties['type'] == 'string' || $properties['type'] == 'datetime'): ?>
                                <label for="ad_<?=$field?>"><?=$properties['title']?>: </label>
                                <input id="ad_<?=$field?>"
                                   name="ad[<?=$field?>]"
                                   class="form-control <?=$properties['isEditable'] ? '' : 'readonly'?>"
                                   type="text"
                                   placeholder="<?=$properties['title']?>"
                                   value="<?=$value?>"
                                   <?=$properties['type'] == 'datetime' ? 'data-inputmask="\'alias\': \'dd.mm.yyyy\'" data-mask' : '' ?>
                                   <?=$properties['isEditable'] ? '' : 'disabled'?>
                                />

                            <?php elseif ($properties['type'] == 'file'): ?>
                                <label for="ad_<?=$field?>"><?=$properties['title']?>: </label>
                                <div class="form-group">
                                    <a href="upload_files/<?=$Ad->$field?>" target="_blank"><?=$Ad->$field?></a>
                                    <input id="ad<?=$field?>" name="ad[<?=$field?>]" type="file">
                                    <p class="help-block">Upload only JPEG, PNG, GIF or SWF</p>
                                </div>

                            <?php elseif ($properties['type'] == 'array'): ?>
                                <label for="ad_<?=$field?>" style="vertical-align: top; margin-top: 5px;">
                                    <?=$properties['title']?>:
                                </label>
                                <div class="form-group" style="display: inline-block;">
                                    <div id="data-fields">
                                        <? foreach ($Ad->$field as $key => $value) : ?>
                                            <input class="params" type="text" name="ad[<?=$field?>][]" value="<?=$key?>"> :
                                            <input class="params" type="text" name="ad[<?=$field?>][]" value="<?=$value?>">
                                            <br/>
                                        <? endforeach; ?>

                                        <?if (!sizeof($Ad->$field)) : ?>
                                            <input class="params" type="text" name="ad[<?=$field?>][]" value=""> :
                                            <input class="params" type="text" name="ad[<?=$field?>][]" value="">
                                            <br/>
                                        <? endif; ?>
                                    </div>
                                    <a href="javascript: void(0);" id="add-field">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </a>
                                </div>
                            <? endif; ?>

                        </div>
                    <? endforeach; ?>
                    <hr/>
                </div>
            </form>
            <div class="box-footer">
                <button class="btn btn-primary" type="submit" onclick="javascript: this.parentNode.previousElementSibling.submit()">
                    <?=Yii::t('yii', 'Сохранить');?>
                </button>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {
    $("[data-mask]").inputmask();

    $('#url_select_domain').change(function() {
        var domain = $('#url_select_domain').val();
        var input  = $('#ad_click_url');
        var adUrlPart = input.data('url');

        input.val('http://' + domain + adUrlPart);
    });

    $('#add-field').click(function() {
        $('#data-fields').append('<input class="params" type="text" name="ad[data_array][]" value=""> : <input class="params" type="text" name="ad[data_array][]" value=""><br/>');
    });


});
</script>