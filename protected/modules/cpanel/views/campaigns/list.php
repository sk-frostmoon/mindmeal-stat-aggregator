<?
use Access\AccessRules;
use Ad\AdCampaign;
use Ad\Category\AdCategory;

/* @var Controller $this */
/* @var AdCampaign[] $Campaigns */
/* @var AdCategory $CurrentCategory */
/* @var AdCategory[] $ParentsCategory */
/* @var AdCategory[] $Categories */
/* @var array $header */

$this->pageTitle = 'Список рекламных кампаний';

$cs = Yii::app()->clientScript;
$cs->registerCoreScript('jquery');
$cs->registerScriptFile($this->getModule()->assetsUrl . '/js/campaigns.js');

$header = AdCampaign::getProperties();
$userId = Yii::app()->user->getId();
?>

<?$this->beginClip('content-header')?>
<section class="content-header">
    <h1><?=$this->pageTitle?></h1>
</section>
<?$this->endClip();?>

<style>
#categories {
    list-style: none;
    margin: 0;
    padding: 0 0 0 20px;
}

#categories li {
    /*display: inline-block;*/
    margin-right: 10px;
}
</style>

<?if (Yii::app()->user->hasFlash('error')) : ?>
<div class="alert alert-danger alert-dismissable">
    <i class="fa fa-ban"></i>
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <b>Alert!</b> <?=var_export(Yii::app()->user->getFlashes('error'), true);?>
</div>
<? endif; ?>


<div class="row">
    <div class="col-xs-3">
        <div class="box box-primary">
            <div class="box-body">
                <div><b>Категории:</b></div>
                <div>
                    <? foreach ($ParentsCategory as $Parent) : ?>
                        <a href='?r=cpanel/campaigns&category=<?=$Parent->id?>'><?=$Parent->title?></a> /
                    <? endforeach; ?>
                    <?=$CurrentCategory->title?>
                </div>
                <hr style="margin-top: 5px; margin-bottom: 5px;"/>
                <div>
                    <ul id="categories">
                        <? foreach ($Categories as $Category): ?>
                            <li>
                                <i style="font-size: 0.8em; margin-right: 2px;" class="glyphicon glyphicon-folder-open"></i>
                                <a href='?r=cpanel/campaigns&category=<?=$Category->id?>'><?=$Category->title?></a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-3">
        <div class="box box-info">
            <div class="box-body">
                <a class="btn btn-success" href="<?=$this->createUrl("/cpanel/campaigns/create", ['category' => $CurrentCategory->id]);?>">
                    <i class="fa fa-bullhorn" style="margin-right: 4px;"></i>
                    <?=Yii::t("yii", 'Добавить кампанию');?>
                </a>
            </div>
            <div class="box-body">
                <a class="btn btn-info" id="add-category" href="javascript: void(0);">
                    <i class="glyphicon glyphicon-folder-open"  style="margin-right: 2px;"></i>
                    <?=Yii::t("yii", 'Добавить категорию');?>
                </a>
            </div>
        </div>
    </div>

</div>


<div class="row">
    <div class="col-xs-12">
        <div class="box box-tools">
            <div class="box-body">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <? foreach ($header as $property => $params) : ?>
                            <? if (isset($params['showOnTable']) && !$params['showOnTable']) {continue;} ?>
                                <th><?=$params['title']?></th>
                            <? endforeach; ?>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <? if (sizeof($Campaigns) > 0) : ?>
                            <? foreach ($Campaigns as $Campaign) : ?>
                            <? if (!AccessRules::hasAccess($userId, AccessRules::RULE_AD_CAMPAIGN_PAGE, AccessRules::ACTION_VIEW_ITEM, $Campaign->author)) {continue;} ?>
                            <tr>
                                <? foreach ($header as $property => $params) : ?>
                                    <? if (isset($params['showOnTable']) && !$params['showOnTable']) {continue;} ?>
                                <td>
                                    <? if ($property == 'createTime'): ?>
                                        <?=date('d.m.Y H:i:s', $Campaign->$property)?>
                                    <? else: ?>
                                        <?=isset($Campaign->$property) ? $Campaign->$property : ''?>
                                    <? endif; ?>
                                </td>
                                <? endforeach; ?>
                                <td>
                                    <? if (AccessRules::hasAccess($userId, AccessRules::RULE_AD_CAMPAIGN_PAGE, AccessRules::ACTION_EDIT, $Campaign->author)) :?>
                                    <a href="<?=Yii::app()->createUrl('/cpanel/campaigns/update', array('adId' => $Campaign->id, 'category'=>$Campaign->categoryId));?>">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <? endif; ?>
                                </td>
                            </tr>
                            <? endforeach; ?>
                        <? else: ?>
                            <tr>
                                <td colspan="<?=sizeof($header) + 1?>">Записей нет</td>
                            </tr>
                        <? endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {
    $('#add-category').click(function() {
        var title = prompt("Введи заголовок категории: ", "");
        if (!title) {
            return;
        }

        window.location = '/index.php?r=cpanel/campaigns/addCategory&category=<?=$CurrentCategory->id?>&title=' + title;
    });
});
</script>