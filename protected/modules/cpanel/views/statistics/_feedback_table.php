<? /* @var Controller $this; */
    $data = $model->getTableData();
    $summ = array_sum(CHtml::listData($data, 'day', 'count'));
?>
<? foreach($data as $row) { ?>
<tr data-day="<?=$row['day'];?>">
    <td><?=$row['day']?></td>
    <td><?=$row['count'];?></td>
</tr>
<? } ?>
<? //@ak summary ?>
<tr data-nop>
    <td></td>
    <td></td>
</tr>
<tr data-summary>
    <td><?= Yii::t('cpanel', 'Period summary');?></td>
    <td><?= $summ; ?></td>
</tr>
<tr data-summary>
    <td><?= Yii::t('cpanel', 'Users per day');?> (<?=$summ?>/<?=$model->getPeriodCountDays()?>)</td>
    <td><?= number_format(round($summ/$model->getPeriodCountDays(), 2), 2); ?></td>
</tr>
<? if ($model->getIsMonthGrouping()) { ?>
<tr data-summary>
    <td><?= Yii::t('cpanel', 'Users per month');?> (<?=$summ?>/<?=$model->getPeriodCountMonths()?>)</td>
    <td><?= number_format(round($summ/$model->getPeriodCountMonths(), 2), 2); ?></td>
</tr>
<? } ?>

<? ?>
