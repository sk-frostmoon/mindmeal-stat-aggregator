<?php /* @var Controller $this */
    $this->pageTitle = 'Сборная страница количества фидбеков с площадок';
    $cs = Yii::app()->clientScript;

    $cs->registerScriptFile('/js/jquery.min.js');
    $cs->registerScriptFile('/js/jquery-ui-1.10.3.min.js');
    $cs->registerScriptFile('/js/bootstrap.min.js');
    $cs->registerScriptFile('/js/AdminLTE/app.js');
    $cs->registerScriptFile('/js/plugins/flot/jquery.flot.js');
    $cs->registerScriptFile('/js/plugins/flot/jquery.flot.resize.min.js');
    $cs->registerScriptFile('/js/plugins/flot/jquery.flot.crosshair.min.js');
    //$cs->registerScriptFile('/js/plugins/flot/jquery.flot.pie.min.js');
    $cs->registerScriptFile('/js/plugins/flot/jquery.flot.categories_mm.js');
    $cs->registerScriptFile('/js/plugins/daterangepicker/daterangepicker.js');
?>
<style>
    tr.hover { background: #abc; }
    div[id$=_table] { padding: 5px; }
    div[id$=_table] > .table > thead > tr { background: #bcd; }
    div[id$=_table] > .table > tbody > tr > td{ padding: 3px; }

    .legendLabel { white-space: nowrap; }
</style>
<script type="text/javascript">
var plots = {};
var legends = {};
var updateLegendTimeout = {};
var latestPosition = {};
var flot_options = {
    grid: {
        hoverable: true,
        autoHightlight: false,
        borderColor: "#a3a3a3",
        borderWidth: 1,
        tickColor: "#f3f3f3",
    },
    series: {
        shadowSize: 0,
        lines: {
            show: true,
            fill: true,
        },
        points: {
            show: true,
        },
    },
    crosshair: {
        mode: "x",
    },
    yaxis: {
        show: true,
    },
    xaxis: {
        show: true,
        mode: "categories_mm",
        tickLength: function (num) { 
            return 5;
        },
        //tickSize: 6,
        
        /*tickFormatter: function (idx, node, a) {
            console.log(idx, node,a );
            return '';
        },*/
    }
};


</script>

<?$this->beginClip('chart_jumpers');?>
<div class="input-group-btn">
    <button data-toggle="dropdown" class="btn btn-small dropdown-toggle" type="button"><span class="fa fa-caret-down"></span></button>
    <ul class="dropdown-menu">
        <? foreach($models as $idModel => $m) { ?>
            <li><a href="#anchor_<?=$idModel?>"><?=$titles[$idModel];?></a></li>
        <? } ?>
    </ul>
</div>
<?$this->endClip();?>

<? foreach($models as $idModel => $m) { ?>
    <? /* @sk Фидбеки с mm.ru */ ?>
    <div class="box box-info">
        <div class="box-header">
            <div class="pull-right box-tools">
                <!-- button class="btn btn-info btn-sm" title="" data-toggle="tooltip" data-widget="collapse" data-original-title="Collapse"><i class="fa fa-minus"></i></button -->
            </div>
            <h3 class="box-title" id="anchor_<?=$idModel?>">
                <div class="input-group">
                     <?=$this->clips['chart_jumpers']?> <div class="" style="margin-left: 15px;"><?=$titles[$idModel];?></div>
                </div>
            </h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 chart-responsive" style="padding-top: 5px">
                    <div id="<?=$idModel?>_filter" class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <select class="type form-control">
                                    <option value="month" selected="">Last Month</option>
                                    <option value="year">Last Year</option>
                                    <option value="custom">Choose...</option>
                                </select>
                            </div>
                            <div id="<?=$idModel?>_filter_custom" class="hidden">
                                <div class="col-md-8">
                                    <input id="<?=$idModel?>_filter_range" class="form-control" name="range" value=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" id="<?=$idModel?>_chart" style="width: 100%; min-width: 300px; height: 300px; margin-top: 10px;"></div>
                </div>
                <div id="<?=$idModel?>_table" class="col-md-7 table-responsive">
                    <table class="table table-stripped" data-current-scroll="0">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Counter</th>
                            </tr>
                        </thead>
                        <tbody> 
                            <? $this->renderPartial('_feedback_table', array('model' => $m)); ?>
                        </tbody>
                    </table>
                    <!-- div class="table" id="mm_feed_main_table" style="height: 200px;">
                    </div -->
                </div> 
            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <?
    $cs->registerScript($idModel."_chart", ""
            . "

                _updateChart('" . $idModel . "', " . json_encode($m->getPlotData()) . ");
                filterBind('" . $idModel . "');
                $('#" . $idModel . "_filter_range').daterangepicker({
                    format: 'YYYY-MM-DD',
                    separator: ' - ',
                    maxDays: 365,
                    container: $('#".$idModel."_filter'),
                });

                // @ak: flush older filters
                $('#" . $idModel . "_filter').find('select.type option[selected]').prop('selected', true);
                $('#" . $idModel . "_filter_range').val('');

                $('.daterangepicker:last button.applyBtn').bind('click', function (event, obj) {
                    window.setTimeout(function () {
                        var range = $('#" . $idModel . "_filter_range').val().split(/ - /);
                        var filter = {
                            model: '".$idModel."',
                            period: 'custom',
                            range: {
                                from:  range[0],
                                to: range[1],
                            },
                        };
                        updateChart('" . $idModel . "', $('#" . $idModel . "_table'), filter);
                    }, 16);
                });
                "
    );
} ?>

<script type="text/javascript">
        var currentHoveredTr = null;
        var currentModel = null;

        function highlightTr(tr)
        {
            // @ak: using pseudo static variable
            if (highlightTr.currentTr != tr) {
                if (highlightTr.currentTr != null) {
                    highlightTr.currentTr.removeClass('hover');
                }
                if (tr != null) {
                    tr.addClass('hover');
                }

                highlightTr.currentTr = tr;
            }
        }

        function updateLegend(model) {
            var plot = plots[model];

            updateLegendTimeout[model] = null;

            var pos = latestPosition[model];

            var x = pos ? Math.floor(pos.x+0.5) : 0;

            var axes = plot.getAxes();
            if (pos.x < axes.xaxis.min || pos.x > axes.xaxis.max ||
                pos.y < axes.yaxis.min || pos.y > axes.yaxis.max) {
                return;
            }

            var i, j, dataset = plot.getData();
            for (i = 0; i < dataset.length; ++i) {

                var series = dataset[i];

                // Find the nearest points, x-wise
                for (j = 0; j < series.data.length; ++j) {
                    if (series.data[j][0] > pos.x) {
                        break;
                    }
                }

                // Now Interpolate
                var y,
                    p1 = series.data[j - 1],
                    p2 = series.data[j];

                if (p1 == null) {
                    y = p2[1];
                } else if (p2 == null) {
                    y = p1[1];
                } else {
                    y = p1[1] + (p2[1] - p1[1]) * (pos.x - p1[0]) / (p2[0] - p1[0]);
                }

                x = x - axes.xaxis.min; // @ak: important to correct label update by data index
                var dt = series.data[x];
                if (dt) {
                    legends[model].eq(i).text(dt[0]+': '+dt[1]);
                } else {
                    //console.log(dt, axes.xaxis.min, series.data, x);
                }
            }
        }
        function _filterBind(model, obj, apndx)
        {
            var model_filter = $('#'+model+'_filter');
            var model_filter_custom = $('#'+model+'_filter_custom');
            var data = {
                'model': model,
                'period': '',
            };
            var val = obj.find('option:selected').val();
            if (val == 'custom') {
                if (apndx == null) {
                    model_filter.find('select.type').parent().addClass('col-md-4').removeClass('col-md-12');
                    model_filter_custom.removeClass('hidden');
                    // get athoner attributes of period
                    $('#'+model+'_filter_range').focus();
                    return;
                } else {
                    // @ak: nothing todo, on apply doing any
                    return;
                }
            } else {
                model_filter_custom.addClass('hidden');
                model_filter.find('select.type').parent().addClass('col-md-12').removeClass('col-md-4');
                data['period'] = val;

            }

            updateChart(model, $('#'+model+'_table'), data); 
        }

        function filterBind(model)
        {
            $('#'+model+'_filter').find('select.type').on('change', function () {
                    _filterBind(model, $(this), null);
            });
        }

        function _plothover(event, pos, item) {
            var model = _plothover.model;
            latestPosition[model] = pos;
            if (item) {
                var dateIndex = item.series.data[item.dataIndex];
                if (dateIndex[1] > 0) {
                    var tr = $('#'+model+'_table > table > tbody > tr[data-day='+dateIndex[0]+']');
                    if (tr.length > 0) {
                        highlightTr(tr);
                    }
                }
            } else {
                highlightTr(null);
            }
            if (!updateLegendTimeout[model]) {
                updateLegendTimeout[model] = setTimeout(function () { updateLegend(model); }, 48);
            }
        }

        function _updateChart (model, data)
        {
            var chart = '#'+model+'_chart';

            if (plots[model] == null) {
                var plot = plots[model] = $.plot(chart, data, flot_options);
                $(chart).bind('plothover', function (event, pos, item) { _plothover.model = model; });
                $(chart).bind('plothover', _plothover);
            } else {
                var plot = plots[model];
                //console.log(data);
                plot.setData(data);
                plot.setupGrid();
                window.setTimeout(function () {
                    plot.draw();
                    $(chart).unbind('plothover', _plothover);
                    $(chart).bind('plothover', _plothover);
                }, 16);
            }

            legends[model] = $(chart+' .legendLabel');
            legends[model].each(function () {
                // fix the widths so they don't jump around
                $(this).css('width', $(this).width());
            });

            updateLegendTimeout[model] = null;
            latestPosition[model]      = null;

            //setChartTricks(chart);
            //setTableTricks(table, chart);
        }

        function _updateTable(table, html)
        {
            table.find('> table > tbody').html($(html));
        }

        function updateChart (chart, table, filter)
        {
            $.ajax({
                url: '<?=$this->createUrl('statistics/feedbackData');?>',
                type: 'POST',
                dataType: "json",
                "data": {
                    "f": filter,
                },
                complete: function (response, status) {
                    if (status == 'success') {
                        _updateChart(chart, response.responseJSON.data);
                        _updateTable(table, response.responseJSON.table);
                    }
                },
            });
        }
</script>
