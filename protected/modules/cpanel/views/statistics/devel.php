<?php /* @var Controller $this */ ?>
<div>
    <h3>Поддерживаемые события</h3>
    <table class="table table-bordered table-hover dataTable">
        <thead>
            <tr>
                <th>Класс</th>
                <th>Таблица</th>
                <th>Собираемые данные</th>
                <th>Последнее время сбора</th>
                <th>Количество (всего)</th>
            </tr>
        </thead>
        <tbody>
            <?
            $actions = array();
            foreach($models as $m) {
                $modelName = get_class($m);
                if ($modelName != 'defaultPushModel') {
                    $actions[] = substr($modelName, 0, strlen($modelName)-9);
                    ?>
                    <tr>
                        <td><?=get_class($m)?></td>
                        <td><?=$m->tableName()?></td>
                        <td><?=implode(', ', $m->getDataScheme());?></td>
                        <td><?=date("Y-m-d H:i:s", $m->getLastTime())?></td>
                        <td><?=$m->getCount()?></td>
                    </tr>
                    <?
                } else {
                    $defaultModel = $m;
                }
            }
            ?>
        </tbody>
        <tfoot>
        </tfoot>
    </table>

    <?
    if (isset($defaultModel)) {
        $m = $defaultModel;
        $keepedActions = $defaultModel->findDistinctActions();
        ?>
        <br/>
        <h3>Коллектор <?=$defaultModel->tableName();?></h3>
        <table class="table table-bordered table-hover dataTable">
            <thead>
                <tr>
                    <th>Событие</th>
                    <th>Поддерживаемый класс</th>
                    <th>Последнее время сбора</th>
                    <th>Количество (всего)</th>
                </tr>
            </thead>
            <tbody>
                <?
                $htmlClass = array(
                        0 => 'label-danger',
                        1 => 'label-warning',
                        2 => 'label-notice',
                );

                foreach($keepedActions as $key => $action) {
                    $status = array(
                            'class'      => isset($models[strtolower($action['name']) . 'PushModel']),
                            'processing' => false, // TODO: Make processing status check
                    );
                    $actionClass = $status['class'] ? $models[strtolower($action['name']) . 'PushModel'] : $defaultModel;
                    ?>
                    <tr class="action-class">
                        <td class="actionName"><span class="label <?=$htmlClass[array_sum($status)]?>"><?=$action['name']?></span></td>
                        <td class="className"><?=get_class($actionClass)?></td>
                        <td class="lastPushed"><?=date("Y-m-d H:i:s", $action['last_time'])?></td>
                        <td class="count"><?=$action['count_pushing']?></td>
                    </tr>
                    <?
                } ?>
            </tbody>
            <tfoot>
            </tfoot>
        </table>
        <?
    }
    ?>
</div>
