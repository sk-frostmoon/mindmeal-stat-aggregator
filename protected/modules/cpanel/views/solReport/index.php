<?
/* @var Controller $this */
/* @var array $tableHeadTitle */
/* @var array $table */
/* @var array $bodyRowsTitle */
/* @var array $tableSumm */

$cs = Yii::app()->getClientScript();
$cs->registerCssFile('/css/datatables/dataTables.bootstrap.css');
$cs->registerCoreScript('jquery');
$cs->registerScriptFile('/js/plugins/datatables/jquery.dataTables.js');
$cs->registerScriptFile('/js/plugins/datatables/dataTables.bootstrap.js');
$cs->registerScriptFile('/js/plugins/daterangepicker/daterangepicker.js');


$this->pageTitle = 'Отчет SOL';
?>

<style>
    table * {
        text-align: center;
    }

    .text-left {
        text-align: left;
    }
    .text-right {
        text-align: right;
    }

    .box {
        min-width: 930px;
    }

    .box .navigate a {
        margin-left: 10px;
        margin-right: 10px;
        color: #000099;
        text-decoration: underline;
    }

    .box .navigate a:hover {
        text-decoration: none;
    }

    .box .navigate a.inactive {
        color: #808080;
        text-decoration: none;
        cursor: default;
    }

    .box-title {
        margin-left: 7px;
    }

    .daterangepicker {
        width: 700px;
    }

    .apply {
        margin-left: 7px;
        margin-top: 5px;
    }

    .table-responsive {
        overflow-x: hidden;
    }

    #example1 {
        width: 100%!important;
    }
</style>

<div class="box">
    <div class="header">
        <div class="row">
            <? foreach ($reports as $_reportName): ?>
            <a href="?r=cpanel/solReport&report=<?=$_reportName?>"><?=$_reportName?></a> |
            <? endforeach; ?>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body table-responsive">
        <h3>Название отчета: "<?=$reportName?>"</h3>
        <table id="example1" class="table table-bordered table-striped">
            <thead></thead>
            <tbody></tbody>
<!--            <tfoot></tfoot>-->
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->

<script type="text/javascript">
    var __tableData;

    $(function() {
        var data    = <?=$dataTable?>;
        var columns = <?=$columnsTable?>;

        <? /*

        var total   = <?=$dataTableTotal?>;
        */ ?>

        // Футер таблицы
        /*var column;
        var footer = '<tr>';
        for (var i in columns) {
            column = columns[i]['data'];

            footer += '<th>' + total[column] + '</th>';
        }
        footer += '</tr>';
        $('#example1 tfoot').html(footer);*/

        __tableData = $('#example1').DataTable({
//            "paginate": false,
//            "search": false,
//            "bFilter": false,
            columns:columns,
            data: data
//            columns: columns,
//            "order": [[ 1, "desc" ]]
        });

        /*
        function exportTableToCSV($table, filename) {

            var $rows = $table.find('tr:has(th),tr:has(td)'),
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character
                colDelim = '","',
                rowDelim = '"\r\n"',
                csv = '"' + $rows.map(function (i, row) {
                    var $row = $(row),
                        $cols = $row.find('td,th');

                    return $cols.map(function (j, col) {
                        var $col = $(col),
                            text = $col.text();

                        return text.replace('"', '""'); // escape double quotes

                    }).get().join(tmpColDelim);

                }).get().join(tmpRowDelim)
                    .split(tmpRowDelim).join(rowDelim)
                    .split(tmpColDelim).join(colDelim) + '"',

            // Data URI
                csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
                .attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank'
                });
        }

        $(".export").on('click', function (event) {
            exportTableToCSV.apply(this, [$('#example1'), 'export_' + ($('#reservation').val()) + '.csv']);
        });*/
    });
</script>