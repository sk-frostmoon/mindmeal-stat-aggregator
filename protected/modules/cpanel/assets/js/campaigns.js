$(document).ready(function () {
    $('[data-href]').click(function (ev) {
        if (ev.target.parentNode == ev.currentTarget) { // tr > td 
            document.location.href = $(ev.currentTarget).data().href;
        }
    });
});
