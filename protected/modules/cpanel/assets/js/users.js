$(document).ready(function () {
        var modelId = $('#modelEditForm').attr('data-model-id');

        $('input[data-name=User\\[activated\\]]').on('ifChanged', function () {
            console.log(1*this.checked);
            $('input[name=User\\[activated\\]]').attr('value', 1*this.checked);
        });

        $('input[type=checkbox][name^=AuthAssignments\\[user\\]], input[type=checkbox][name^=AuthAssignments\\[moderator\\]]').on('ifChanged', function () {
                var checkbox = $(this);
                var boxBody = $(this.parentNode.parentNode.parentNode.parentNode);
                var spinner = $(boxBody[0].previousElementSibling).find('i.fa-spinner');
                var overlay = document.createElement('div');
                    overlay.className = 'overlay';
                    overlay.style.zIndex = 9;
                    boxBody.parent().append(overlay);
                spinner.removeClass('hidden').css({zIndex:10});
                while(spinner.parent()[0].nextElementSibling) {
                    spinner.parent()[0].nextElementSibling.remove();
                }

                $.ajax({
                    url: 'index.php?r=cpanel/user/ajax' +
                        (this.checked ? 'Assign' : 'Revoke') +
                        ((this.name.indexOf('[moderator]') !== -1) ? 'Moderate' : 'View') + '&id=' + modelId,
                    method: 'POST',
                    data: {
//                        id: modelId,
                        campaign_id: this.value,
                    },
                    complete: function (ptr, statusText) {
                        spinner.addClass('hidden');
                        $(overlay).remove();
                        while(spinner.parent()[0].nextElementSibling) {
                            spinner.parent()[0].nextElementSibling.remove();
                        }
                        spinner.parent().parent().append($(ptr.responseText));
                    },
                });
        });
});

function changeAssignment(type, modelId, campaignId, assign)
{
    console.log(arguments);
}
