<?php

class OvBrowser extends CActiveRecord
{
    static public function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'ov_browser';
    }

    public function attributeNames()
    {
        return array(
                'id',
                'browser',
                'count',
        );
    }

    public function attributeLabels()
    {
        return array(
                'id'      => Yii::t('cpanel', 'ID'),
                'browser' => Yii::t('cpanel', 'Browser'),
                'count'   => Yii::t('cpanel', 'Count'),
        );
    }

    public function rules()
    {
        return array();
    }

    public function save()
    {
        throw new CException('Model cannot be saved');
    }
}
