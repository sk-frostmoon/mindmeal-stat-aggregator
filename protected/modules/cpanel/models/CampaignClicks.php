<?php

class CampaignClicks extends CActiveRecord
{
    static public function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'adv_click';
    }

    public function rules()
    {
        return array();
    }

    public function relations()
    {
        return array(
                'campaign' => array(self::HAS_ONE, 'Campaign', 'campaign_id'),
        );
    }
}
