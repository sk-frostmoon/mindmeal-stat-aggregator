<?php

class Campaign extends CActiveRecord
{
    const CURRENT_SCRIPT_VERSION = '0';

    private $_saveCode;

    static public function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'adv_campaign';
    }

    public function attributesNames()
    {
        return array(
                'id',
                'author',
                'created',
                'name',
                'target_url',
                'link_type',
                'code',
        );
    }

    public function attributeLabels()
    {
        return array(
                'id'          => Yii::t('cpanel', 'ID'),
                'author'      => Yii::t('cpanel', 'Author'),
                'created'     => Yii::t('cpanel', 'Created At'),
                'target_url'  => Yii::t('cpanel', 'Target URL'),
                'link_type'   => Yii::t('cpanel', 'Link Type'),
                'code'        => Yii::t('cpanel', 'Code'),
                'upload_file' => Yii::t('cpanel', 'Upload File'),

                // Additional labels
                'generated_click_url' => Yii::t('cpanel', 'Generated Click Url'),
                'generated_image_url' => Yii::t('cpanel', 'Generated Image Url'),
        );
    }

    public function rules()
    {
        return array(
                array('name', 'required', 'on' => 'insert, update'),
                array('name', 'length', 'min'=>3, 'max'=>255, 'on' => 'insert, update'),

                array('author, target_url, code', 'required', 'on'=>'insert'),
                array('created', 'default', 'on'=>'insert'),
                array('target_url', 'url', 'allowEmpty'=>false, 'on'=>'insert'),
                array('code', 'file', 'types' => 'jpg, gif, png, svg, swf', 'on'=>'insert'),
                //array('code', 'length', 'min' => 12, 'max' => 128, 'on'=>'afterInsert'),
                array('id, name, author, created, target_url, link_type, code', 'default', 'on'=>'safe'),
        );
    }

    public function behaviors()
    {
        $behaviors = array(
                'renderBehavior' => array(
                    'class' => 'CampaignImageBehavior',
                    'code' => $this->code,
                    'webRootStat' => Yii::app()->getModule('cpanel')->webRootStat,
                ),
        );

        if (!empty($this->code)) {
            $mt   = $this->getPosibleMimeType(true);
            $type = substr($this->code, 1, 1);
            if (isset($mt[$type])) {
                $behaviors['renderBehavior']['class'] = $mt[$type];
            }
        }
    
        return $behaviors;
    }

    // public getPosibleMimeType(comparison=false) {{{ 
    /**
     * getPosibleMimeType
     * 
     * @param boolean $comparison 
     * @access public
     * @return array
     */
    public function getPosibleMimeType($comparison=false)
    {
        # http://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_MIME-%D1%82%D0%B8%D0%BF%D0%BE%D0%B2
        $mimeTypesRegexp = array(
            // ...
            '/^image\/(gif|jpeg|pjpeg|png)$/ui' => array('mime-type_code' => 'i', 'class'=>'CampaignImageBehavior'),
            '/^application\/x-shockwave-flash$/ui' => array('mime-type_code' => 'f', 'class'=>'CampaignFlashBehavior'),
            // ...
            '/^image\/svg\+xml$/ui'                => array('mime-type_code' => 's', 'class'=>'CampaignSvgBehavior'),
            // ...
        );

        if ($comparison) {
            $mimeTypes = array();
            foreach($mimeTypesRegexp as $data) {
                $mimeTypes[$data['mime-type_code']] = $data['class'];
            }
            return $mimeTypes;
        } else {
            return $mimeTypesRegexp;
        }
    }
    // }}}

    public function beforeSave()
    {
        if ($this->code instanceof CUploadedFile) {
            $versionStr = self::CURRENT_SCRIPT_VERSION;
            foreach ($this->getPosibleMimeType() as $pattern => $data)
            {
                if (preg_match($pattern, $this->code->type) > 0) {
                    $codeStr = $data['mime-type_code'];
                    break;
                }
            }

            if ($this->getIsNewRecord()) {
                $code = sha1($this->author . ':' . $this->target_url . ':' . sha1_file($this->code->tempName) . ':' . time() . rand());
            } else {
                $code = $this->_saveCode;
            }

            if (!isset($codeStr)) {
                $this->addError('code', 'Processing for type ' . $model->code->type . ' not found');
            }

            $codeStr = $versionStr . $codeStr . substr($code, 0, 10);
            $this->code->saveAs(Yii::getPathOfAlias('webroot') . '/media/campaigns/' . $codeStr);
            $this->code = $codeStr;
        }

        return parent::beforeSave();
    }

    public function afterFind()
    {
        $this->_saveCode = $this->code;
        return parent::afterFind();
    }

    public function getAuthorName()
    {
        return isset($this->creator) ? $this->creator->getFullName() : 'unknown';
    }

    public function getUserAssignments($role = NULL)
    {
        $command = $this->db->createCommand()
            ->select('*')
            ->from('AuthAssignment');
        if ($role !== NULL) {
            $command->where('itemname=:itemname', array(':itemname'=>$role));
        }
        $rows = $command->queryAll();
        $assignments = array();
        foreach($rows as $row) {
            $data = @unserialize($row['data']);
            if (isset($data['campaigns'][$this->id])) {
                $assignments[$row['itemname']][$row['userid']] = User::model()->findByPk($row['userid']);
            }
        }

        return $assignments;
    }

    public function relations()
    {
        return array(
            'creator' => array(self::HAS_ONE, 'User', array('id'=>'author'), 'alias'=>'cu'),
            'clicks'  => array(self::STAT, 'CampaignClicks', 'campaign_id', 'select'=>'COUNT(*)'),
        );
    }
}
