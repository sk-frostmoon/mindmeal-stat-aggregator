<?php

class OvOs extends CActiveRecord
{
    static public function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'ov_os';
    }

    public function attributeNames()
    {
        return array(
                'id',
                'os',
                'count',
        );
    }

    public function attributeLabels()
    {
        return array(
                'id' => Yii::t('cpanel', 'ID'),
                'os' => Yii::t('cpanel', 'OS'),
                'count' => Yii::t('cpanel', 'Count'),
        );
    }

    public function rules()
    {
        return array();
    }

    public function save()
    {
        throw new CException('Model cannot be saved');
    }
}

