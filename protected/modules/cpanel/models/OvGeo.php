<?php

class OvGeo extends CActiveRecord
{
    static public function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'ov_geo';
    }

    public function attributeNames()
    {
        return array(
                'id',
                'country_name',
                'country_code',
                'city_name',
                'count',
        );
    }

    public function attributeLabels()
    {
        return array(
                'id' => Yii::t('cpanel', 'ID'),
                'country_name' => Yii::t('cpanel', 'Country Name'),
                'country_code' => Yii::t('cpanel', 'Country Code'),
                'city_name' => Yii::t('cpanel', 'City Name'),
                'count' => Yii::t('cpanel', 'Count'),
        );
    }

    public function rules()
    {
        return array();
    }

    public function save()
    {
        throw new CException('Model cannot be saved');
    }
}

