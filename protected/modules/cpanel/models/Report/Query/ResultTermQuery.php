<?php
namespace Report\Query;


class ResultTermQuery extends ResultQuery {

    /**
     * Разворачивает массив результатов
     * @return mixed
     */
    public function expand() {
        $this->expandArrayColsToRows('term', 'count');
    }

    /**
     * Нормализует сырые данные, полученные из es
     * @return $this
     */
    public function normalizeData() {
        $data = $this->getResult();

        if (isset($data['facets']) && isset($data['facets']['terms']) && isset($data['facets']['terms']['terms'])) {
            $resData = $data['facets']['terms']['terms'];

            foreach ($resData as $key => $value) {
                if ($value['term'] == 'null_null_null') {
                    $resData[$key]['term'] = 'Нет данных';
                }
            }

            if (isset($data['facets']['terms']['missing']) && $data['facets']['terms']['missing']) {
                $resData[] = array (
                    'term'  => 'Нет данных',
                    'count' => $data['facets']['terms']['missing'],
                );
            }

            return  $this->setResult($resData);
        }

        return $this;
    }
}