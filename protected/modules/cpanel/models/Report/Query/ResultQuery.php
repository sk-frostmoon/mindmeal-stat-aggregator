<?php
namespace Report\Query;


abstract class ResultQuery {
    protected $result  = array();
    protected $colName = array();

    /**
     * Разворачивает массив результатов
     * @return $this
     */
    abstract public function expand();

    /**
     * Нормализует сырые данные, полученные из es
     * @return $this
     */
    abstract public function normalizeData();

    /**
     * @param array $result
     * @return $this
     */
    public function setResult(array $result) {
        $this->result = $result;
        return $this;
    }

    /**
     * @return array
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * Мержит данные
     * @param array $row
     * @return $this
     */
    public function mergeRow(array $row) {
        $result = array_merge($row, $this->getResult());
        return $this->setResult($result);
    }

    /**
     * Разворачивает колонки в строки
     * @param string $fieldRow
     * @param string $fieldValue
     * @return array
     */
    protected function expandArrayColsToRows($fieldRow, $fieldValue) {
        // Развернем сырые данные по колонкам -> по строкам
        $result           = $this->getResult();
        $tableDataPerRows = array();


        foreach ($result as $columnName => $tableDataPerColumn) {
            foreach ($tableDataPerColumn as $column) {
                $rowName = $column[$fieldRow];

                if (!isset($tableDataPerRows[$rowName])) {
                    $tableDataPerRows[$rowName] = array();
                }

                $tableDataPerRows[$rowName][$columnName] = $column[$fieldValue];
            }
        }

        return $result = $tableDataPerRows;
    }
} 