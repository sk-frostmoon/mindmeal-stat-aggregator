<?php
namespace Report\Query;
use Closure;


/**
 * Class StatisticsTermQueryBuilder
 * @package Report\Query
 */
class StatisticsTermQueryBuilder extends StatisticsQueryBuilder {
    /**
     * Возвращает выражение для поиска
     * @return string
     */
    public function getTerm() {
        return '"terms":{
            "field":"' . ($this->term) . '",
            "order":"count",
            "size":500,
            "exclude":[]
        }';
    }

    /**
     * @return ResultQuery
     */
    public function createResult() {
        return new ResultTermQuery();
    }

    /**
     * @return string
     */
    public function getFieldRow() {
        return 'term';
    }

    /**
     * @return string
     */
    public function getFieldValue() {
        return 'count';
    }

    /**
     * @param string $title
     * @param Closure $Closure
     * @return string
     */
    public function normalizeTitle($title, Closure $Closure = null) {
        if (!is_null($Closure)) {
            return $Closure($title);
        }

        return '<a href="?' . $title . '">' . $title . '</a>';
    }
}