<?php
namespace Report\Query;


class ResultHistogramQuery extends ResultQuery {

    /**
     * Разворачивает массив результатов
     * @return mixed
     */
    public function expand() {
        $this->expandArrayColsToRows('time', 'count');
    }

    /**
     * Нормализует сырые данные, полученные из es
     * @return $this
     */
    public function normalizeData() {
        $data = $this->getResult();

        if (isset($data['facets']) && isset($data['facets']['terms']) && isset($data['facets']['terms']['entries'])) {
            $data = $data['facets']['terms']['entries'];
            return  $this->setResult($data);
        }

        return $this;
    }
}