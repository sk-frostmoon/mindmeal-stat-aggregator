<?php
namespace Report\Query\Details;

use Closure;
use DateTime;
use Report\Query\ResultQuery;
use Report\Query\StatisticsQueryBuilder;

class DetailsQueryBuilder extends StatisticsQueryBuilder {

    /**
     * Возвращает выражение для поиска
     * @return string
     */
    public function getTerm() {
        return '';
    }

    /**
     * @return ResultQuery
     */
    public function createResult() {
        return new DetailsTermQuery();
    }

    /**
     * @return string
     */
    public function getFieldRow() {
        return 'term';
    }

    /**
     * @return string
     */
    public function getFieldValue() {
        return 'count';
    }

    /**
     * @param string $title
     * @param callable $Closure
     * @return string
     */
    public function normalizeTitle($title, Closure $Closure = null) {
        return $title;
    }

    public function makeQueryString() {
        $query  = $this->getLucene();
        $from   = date(DateTime::ISO8601, $this->getRangeFrom());
        $to     = date(DateTime::ISO8601, $this->getRangeTo());
        $filter = $this->getFilter();

        $query = <<<EOF
{
  "query": {
    "filtered": {
      "query": {
        "bool": {
          "should": [
            {
              "query_string": {
                "query": "{$query}"
              }
            }
          ]
        }
      },
      "filter": {
        "bool": {
          "must": [
            {
              "terms": {
                "_type": [
                  "event"
                ]
              }
            },
            {
              "range": {
                "@timestamp": {
                  "from": "{$from}",
                  "to": "{$to}"
                }
              }
            },
            {
              "fquery": {
                "query": {
                  "query_string": {
                    "query": "_type:event"
                  }
                },
                "_cache": true
              }
            }
          ]
        }
      }
    }
  },
  "highlight": {
    "fields": {},
    "fragment_size": 2147483647,
    "pre_tags": [
      "@start-highlight@"
    ],
    "post_tags": [
      "@end-highlight@"
    ]
  },
  "size": 2500,
  "sort": [
    {
      "@timestamp": {
        "order": "desc",
        "ignore_unmapped": true
      }
    }
  ]
}
EOF;

        return $query;
    }
}