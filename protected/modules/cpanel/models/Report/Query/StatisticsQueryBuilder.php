<?php
namespace Report\Query;
use Closure;
use DateTime;
use Report\Filter\ReportFilterContent;
use Report\Filter\ReportFilterSelectedValues;
use Report\TableColumnConfig;

/**
 * Формирует запрос выборки в es
 * @author akiselev
 */
abstract class StatisticsQueryBuilder {
    protected $term      = null;
    protected $query     = null;
    protected $rangeFrom = null;
    protected $rangeTo   = null;
    protected $filter    = "";
    protected $filterUTM = null;

    protected $data = null;

    /**
     * Возвращает выражение для поиска
     * @return string
     */
    abstract public function getTerm();

    /**
     * @return ResultQuery
     */
    abstract public function createResult();

    /**
     * @return string
     */
    abstract public function getFieldRow();

    /**
     * @return string
     */
    abstract public function getFieldValue();

    /**
     * @param string $title
     * @param callable $Closure
     * @return string
     */
    abstract public function normalizeTitle($title, Closure $Closure = null);

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data) {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Устанавливает выражение для поиска
     * @param mixed $term
     * @return StatisticsQueryBuilder
     */
    public function setTerm($term) {
        $this->term = $term;
        return $this;
    }

    /**
     * Устанавливает промежуток, за который выбираются данные
     * @param int $from
     * @param int $to
     * @return StatisticsQueryBuilder
     */
    public function setRange($from, $to) {
        $this->rangeFrom = $from;
        $this->rangeTo   = $to;

        return $this;
    }

    /**
     * Возвращает начало промежутка
     * @return int|null
     */
    public function getRangeFrom() {
        return $this->rangeFrom;
    }

    /**
     * Возвращает начало промежутка
     * @return int|null
     */
    public function getRangeTo() {
        return $this->rangeTo;
    }

    /**
     * Устанавливает lucene запрос
     * http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html#query-string-syntax
     * @param string $query
     * @return StatisticsQueryBuilder
     */
    public function setLucene($query) {
        $this->query = $query;
        return $this;
    }

    /**
     * Возвращает lucene запрос
     * @return string|null
     */
    public function getLucene() {
        return $this->query;
    }

    /**
     * Устанавливает lucene фильтр для запроса
     * @param string $filter
     * @return $this
     */
    public function setFilter($filter) {
        $this->filter = $filter;
        return $this;
    }

    /**
     * Возвращает lucene фильтр
     * @return string
     */
    public function getFilter() {
        return $this->filter;
    }

    /**
     * Устанавливает lucene фильтр для запроса
     * @param string $filter
     * @return $this
     */
    public function setFilterUTM($filter) {
        $this->filterUTM = $filter;
        return $this;
    }

    /**
     * Возвращает lucene фильтр
     * @return string
     */
    public function getFilterUTM() {
        return $this->filterUTM;
    }

    public function makeQueryString() {
        $term   = $this->getTerm();
        $query  = $this->getLucene();
        $filter = $this->getFilter();
        $from   = date(DateTime::ISO8601, $this->getRangeFrom());
        $to     = date(DateTime::ISO8601, $this->getRangeTo());

        if ($filter) {
            $fquery = ',{
                "fquery":{
                    "query":{
                        "query_string":{
                            "query":"' . $filter . '"
                        }
                    },
                    "_cache":true
                }
            }';
        } else {
            $fquery = '';
        }

        $query = <<<EOF
{
    "facets":{
        "terms":{
            {$term},
            "facet_filter":{
                "fquery":{
                    "query":{
                        "filtered":{
                            "query":{
                                "bool":{
                                    "should":[{
                                        "query_string":{
                                            "query":"{$query}"
                                        }
                                    }]
                                }
                            },
                            "filter":{
                                "bool":{
                                    "must":[{
                                        "range":{
                                            "@timestamp":{
                                                "from":"{$from}",
                                                "to":"{$to}"
                                            }
                                        }
                                    }{$fquery}]
                                }
                            }
                        }
                    }
                }
            }
        }
    },
    "size":0
}
EOF;

        return $query;
    }

    /**
     * @return StatisticsQueryBuilder
     */
    public function makeClone() {
        return clone $this;
    }

    /**
     * @param TableColumnConfig $TableConfig
     * @param ReportFilterSelectedValues $ReportFilterSelectedValues
     * @return array
     */
    public function makeQueries(TableColumnConfig $TableConfig, ReportFilterSelectedValues $ReportFilterSelectedValues) {
        $Queries = array();
        foreach ($TableConfig->queryColumns() as $shownColumn) {
//            if ($ReportFilterSelectedValues->hiddenColumns && !in_array($shownColumn, $ReportFilterSelectedValues->hiddenColumns)) {
//                continue;
//            }

            $columnQuery = $TableConfig->getQuery($shownColumn, $ReportFilterSelectedValues->site);

            $Query = $this->makeClone()
                ->setTerm($ReportFilterSelectedValues->term)
                ->setRange(
                    $ReportFilterSelectedValues->timeFrom + 60 * 60 * 4,
                    strtotime('+1 day', $ReportFilterSelectedValues->timeTo + 60 * 60 * 4)
                )
                ->setLucene($columnQuery);

            $Queries[$shownColumn] = $Query;
        }

        return $Queries;
    }
} 