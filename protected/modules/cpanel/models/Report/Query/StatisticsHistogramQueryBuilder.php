<?php
namespace Report\Query;
use Closure;

/**
 * Строитель запроса на получаение данных по датам
 * @author akiselev
 */
class StatisticsHistogramQueryBuilder extends StatisticsQueryBuilder {
    /**
     * Интервал
     * @var string
     */
    protected $interval = '1d';
    protected $term     = '@timestamp';

    /**
     * Устанавливает интервал
     * @param string $interval
     * @return $this
     */
    public function setInterval($interval) {
        $this->interval = $interval;
        return $this;
    }

    /**
     * Возвращает интервал
     * @return string
     */
    public function getInterval() {
        return $this->interval;
    }

    /**
     * Возвращает выражение для поиска
     * @return string
     */
    public function getTerm() {
        $interval = $this->getInterval();

        return '"date_histogram": {
            "key_field": "' . ($this->term) . '",
            "value_field": "@count",
            "interval": "' . $interval . '"
        }';
    }

    /**
     * @return ResultQuery
     */
    public function createResult()  {
        return new ResultHistogramQuery();
    }

    /**
     * @return string
     */
    public function getFieldRow() {
        return 'time';
    }

    /**
     * @return string
     */
    public function getFieldValue() {
        return 'count';
    }

    /**
     * Переопеределим
     * @param mixed $term
     * @return $this
     */
    public function setTerm($term) {
        return $this;
    }

    /**
     * @param string $title
     * @param Closure $Closure
     * @return string
     */
    public function normalizeTitle($title, Closure $Closure = null) {
        switch ($this->getInterval()) {
            case "7d":
                return date('Y/m/d', $title / 1000 );

            case "30d":
                return date('Y/m', $title / 1000 );

            case "1d":
            default:
                return date('Y/m/d', $title / 1000 );
        }
    }
}