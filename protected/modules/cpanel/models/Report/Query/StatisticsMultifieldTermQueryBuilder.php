<?php
namespace Report\Query;
use Closure;
use Report\Filter\ReportFilterContent;
use Report\TableColumnConfig;


/**
 * Class StatisticsTermQueryBuilder
 * @package Report\Query
 */
class StatisticsMultifieldTermQueryBuilder extends StatisticsQueryBuilder {
    /**
     * Возвращает выражение для поиска
     * @return string
     */
    public function getTerm() {
        $terms = $this->term;

        if (!is_array($terms)) {
            $terms = array($terms);
        }

        foreach ($terms as $key => $term) {
            $terms[$key] = sprintf('"%s"', $term);
        }

        $terms = implode(',', $terms);
        $termsQuery = '"terms": {
            "script_field": "kibana_multifield",
            "params": {
                "fields": [' . $terms . '],
                "separator": ":"
            },
            "size": 500,
            "order": "count",
            "exclude": []
        }';

        return $termsQuery;
    }

    /**
     * @return ResultQuery
     */
    public function createResult() {
        return new ResultTermQuery();
    }

    /**
     * @return string
     */
    public function getFieldRow() {
        return 'term';
    }

    /**
     * @return string
     */
    public function getFieldValue() {
        return 'count';
    }

    /**
     * @param string $title
     * @param Closure $Closure
     * @return string
     */
    public function normalizeTitle($title, Closure $Closure = null) {
        if (!is_null($Closure)) {
            return $Closure($title);
        }

        return '<a href="?' . $title . '">' . $title . '</a>';
    }
}