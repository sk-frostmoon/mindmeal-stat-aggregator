<?php
namespace Report\Filter;


use Report\TableColumnConfig;

class ReportFilterContent {
    /**
     * Фильтр по сайту
     * @var string[]
     */
    public $site = [];

    /**
     * Период времени "от"
     * @var int|null
     */
    public $timeFrom = 0;

    /**
     * Период времени "до"
     * @var int|null
     */
    public $timeTo = 0;

    /**
     * Не показывать колонки
     * @var string[]
     */
    public $hiddenColumns = array();

    /**
     * Фильтр по utm_campaign
     * @var string[]
     */
    public $utmCampaign = array();

    /**
     * Фильтр по utm_source
     * @var string[]
     */
    public $utmSource = array();

    /**
     * Фильтр по utm_medium
     * @var string[]
     */
    public $utmMedium = array();

    /**
     * Фильтр по utm_redirect
     * @var string[]
     */
    public $utmRedirect = array();

    /**
     * Фильтр по значению строк, где значение колонки "больше"
     * @var string[]
     */
    public $rowValue = array();

    /**
     * @param TableColumnConfig $ColumnConfig
     */
    public function __construct(TableColumnConfig $ColumnConfig = null) {
        if ($ColumnConfig) {
            $this->makeFromConfig($ColumnConfig);
        }
    }

    /**
     * Строит данные из конфига
     * @param TableColumnConfig $ColumnConfig
     */
    public function makeFromConfig(TableColumnConfig $ColumnConfig) {
        $this->site = $ColumnConfig->getQueries();

        foreach ($ColumnConfig->tableColumns() as $key => $column) {
            $this->hiddenColumns[$key] = $column;

            if (isset($column['canFiltered']) && $column['canFiltered']) {
                $this->rowValue[$key] = $column;
            }
        }
    }

    /**
     * Устанавливает интервал
     * @param int $timeFrom
     * @param int $timeTo
     */
    public function setInterval($timeFrom, $timeTo) {
        $this->timeFrom = $timeFrom;
        $this->timeTo   = $timeTo;
    }

    /**
     * @return string
     */
    public function getDateIntervalString() {
        return date('d.m.Y', $this->timeFrom) . ' - ' . date('d.m.Y', $this->timeTo);
    }
}