<?php
namespace Report\Filter;


class ReportFilter {
    /**
     * Фильтр по сайту
     * @var string|null
     */
    public $site = null;

    /**
     * Фильтр по странице
     * @var string|null
     */
    public $page = null;

    /**
     * Период времени "от"
     * @var int|null
     */
    public $timeFrom = null;

    /**
     * Период времени "до"
     * @var int|null
     */
    public $timeTo = null;

    /**
     * Не показывать колонки
     * @var string[]
     */
    public $hiddenColumns = array();

    /**
     * Фильтр по utm_campaign
     * @var string[]
     */
    public $utmCampaign = array();

    /**
     * Фильтр по utm_source
     * @var string[]
     */
    public $utmSource = array();

    /**
     * Фильтр по utm_medium
     * @var string[]
     */
    public $utmMedium = array();

    /**
     * Фильтр по utm_redirect
     * @var string[]
     */
    public $utmRedirect = array();

    /**
     * Фильтр по значению строк, где значение колонки "больше"
     * @var string[int]
     */
    public $rowValueLarger = array();

    /**
     * Фильтр по значению строк, где значение колонки "меньше"
     * @var string[int]
     */
    public $rowValueLess = array();
}