<?php
namespace Report\Filter;


class ReportFilterSelectedValues {
    /**
     * Фильтр по сайту
     * @var string[]
     */
    public $site = null;

    /**
     * Период времени "от"
     * @var int|null
     */
    public $timeFrom = null;

    /**
     * Период времени "до"
     * @var int|null
     */
    public $timeTo = null;

    /**
     * Не показывать колонки
     * @var string[]
     */
    public $hiddenColumns = array();

    /**
     * Фильтр по utm_campaign
     * @var string[]
     */
    public $utmCampaign = array();

    /**
     * Фильтр по utm_source
     * @var string[]
     */
    public $utmSource = array();

    /**
     * Фильтр по utm_medium
     * @var string[]
     */
    public $utmMedium = array();

    /**
     * Фильтр по utm_redirect
     * @var string[]
     */
    public $utmRedirect = array();

    /**
     * @var array
     */
    public $filterColumn    = array();
    public $filterOperation = array();
    public $filterValue     = array();

    /**
     * @var array
     */
    public $term = array();

    public function isCheckedHidden($field) {
        if (!$this->hiddenColumns) {
            return true;
        }

        if (in_array($field, $this->hiddenColumns)) {
            return true;
        }

        return false;
    }
}