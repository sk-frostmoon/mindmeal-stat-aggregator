<?php
namespace Report;

/**
 * Конфигурация для табличек конфига
 * @author akiselev
 */
class TableColumnConfig {
    const QUERY_ALL          = 'all';
    const QUERY_ONLY_SOLGAME = 'only_solgame';
    const QUERY_LANDING_1    = 'landing_1';
    const QUERY_LANDING_2    = 'landing_2';
    const QUERY_LANDING_3    = 'landing_3';
    const QUERY_LANDING_4    = 'landing_4';
    const QUERY_LANDING_5    = 'landing_5';
    const QUERY_MINDMEAL     = 'mindmeal';
    const QUERY_FD           = 'fd';

    // Запрос по умолчанию
    const QUERY_DEFAULT = self::QUERY_ALL;

    const GOAL_HITS     = 'hits';
    const GOAL_REGISTER = 'register';
    const GOAL_DOWNLOAD = 'download';
    const GOAL_SOL_ENTER_GAME = 'sol_enter_game';

    public function get() {
        return array(
            static::GOAL_HITS => array(
                'title' => 'Хиты',
                'query' => array(
                    static::QUERY_ALL          => '@key:site_hit_unique AND user.data.site:solgame',
                    static::QUERY_ONLY_SOLGAME => '@key:site_hit_unique AND user.data.site:solgame AND NOT user.data.page:landing',
                    static::QUERY_LANDING_1    => '@key:site_hit_unique AND user.data.site:solgame AND user.data.page:landing AND user.data.landing_id:1',
                    static::QUERY_LANDING_2    => '@key:site_hit_unique AND user.data.site:solgame AND user.data.page:landing AND user.data.landing_id:2',
                    static::QUERY_LANDING_3    => '@key:site_hit_unique AND user.data.site:solgame AND user.data.page:landing AND user.data.landing_id:3',
                    static::QUERY_LANDING_4    => '@key:site_hit_unique AND user.data.site:solgame AND user.data.page:landing AND user.data.landing_id:4',
                    static::QUERY_LANDING_5    => '@key:site_hit_unique AND user.data.site:solgame AND user.data.page:landing AND user.data.landing_id:5',
                    static::QUERY_MINDMEAL     => '@key:site_hit_unique AND user.data.site:mindmeal',
                    static::QUERY_FD           => '@key:site_hit_unique AND (user.data.site:playfd_ru OR user.data.site:finaldesire_ru)',
                ),
            ),
            static::GOAL_REGISTER => array(
                'title' => 'Регистрации',
                'query' => array(
                    static::QUERY_ALL          => '@key:user_register AND user.data.site:solgame',
                    static::QUERY_ONLY_SOLGAME => '@key:user_register AND user.data.site:solgame AND NOT user.data.is_landing:true',
                    static::QUERY_LANDING_1    => '@key:user_register AND user.data.site:solgame AND user.data.page:static_landing_1',
                    static::QUERY_LANDING_2    => '@key:user_register AND user.data.site:solgame AND user.data.page:static_landing_2',
                    static::QUERY_LANDING_3    => '@key:user_register AND user.data.site:solgame AND user.data.page:static_landing_3',
                    static::QUERY_LANDING_4    => '@key:user_register AND user.data.site:solgame AND user.data.page:static_landing_4',
                    static::QUERY_LANDING_5    => '@key:user_register AND user.data.site:solgame AND user.data.page:static_landing_5',
                    static::QUERY_MINDMEAL     => '@key:user_register AND user.data.site:mindmeal',
                    static::QUERY_FD           => '@key:user_register AND (user.data.site:playfd_ru OR user.data.site:finaldesire_ru)',
                ),
            ),
            static::GOAL_DOWNLOAD => array(
                'title' => 'Загрузки',
                'query' => array(
                    static::QUERY_ALL          => '@key:client_download_unique AND user.data.site:solgame',
                    static::QUERY_ONLY_SOLGAME => '@key:client_download_unique AND user.data.site:solgame AND NOT user.data.is_landing:true',
                    static::QUERY_LANDING_1    => '@key:client_download_unique AND user.data.site:solgame AND user.data.page:static_landing_1',
                    static::QUERY_LANDING_2    => '@key:client_download_unique AND user.data.site:solgame AND user.data.page:static_landing_2',
                    static::QUERY_LANDING_3    => '@key:client_download_unique AND user.data.site:solgame AND user.data.page:static_landing_3',
                    static::QUERY_LANDING_4    => '@key:client_download_unique AND user.data.site:solgame AND user.data.page:static_landing_4',
                    static::QUERY_LANDING_5    => '@key:client_download_unique AND user.data.site:solgame AND user.data.page:static_landing_5',
                    static::QUERY_MINDMEAL     => '@key:client_download_unique AND user.data.site:mindmeal',
                    static::QUERY_FD           => '@key:client_download_unique AND (user.data.site:playfd_ru OR user.data.site:finaldesire_ru)',
                ),
            ),
            static::GOAL_SOL_ENTER_GAME => array(
                'title' => 'Первый раз зашли в SOL',
                'query' => array(
                    static::QUERY_ALL          => '@key:sol_first_begin_play_game',
                    static::QUERY_ONLY_SOLGAME => '@key:sol_first_begin_play_game AND NOT user.data.is_landing:true',
                    static::QUERY_LANDING_1    => '@key:sol_first_begin_play_game AND user.data.page:static_landing_1',
                    static::QUERY_LANDING_2    => '@key:sol_first_begin_play_game AND user.data.page:static_landing_2',
                    static::QUERY_LANDING_3    => '@key:sol_first_begin_play_game AND user.data.page:static_landing_3',
                    static::QUERY_LANDING_4    => '@key:sol_first_begin_play_game AND user.data.page:static_landing_4',
                    static::QUERY_LANDING_5    => '@key:sol_first_begin_play_game AND user.data.page:static_landing_5',
                    static::QUERY_MINDMEAL     => '@key:sol_first_begin_play_game AND user.data.site:mindmeal',
                    static::QUERY_FD           => '@key:sol_first_begin_play_game AND (user.data.site:playfd_ru OR user.data.site:finaldesire_ru)',
                ),
            ),
        );
    }

    /**
     * Колонки, которые нужно запросить
     * @return array
     */
    public function queryColumns() {
        return array(
            static::GOAL_HITS,
            static::GOAL_REGISTER,
            static::GOAL_DOWNLOAD,
            static::GOAL_SOL_ENTER_GAME,
        );
    }

    public function getQueries() {
        return array(
            static::QUERY_ALL          => 'Весь solgame.ru',
            static::QUERY_ONLY_SOLGAME => 'Только solgame.ru',
            static::QUERY_LANDING_1    => 'Landing #1',
            static::QUERY_LANDING_2    => 'Landing #2',
            static::QUERY_LANDING_3    => 'Landing #3',
            static::QUERY_LANDING_4    => 'Landing #4',
            static::QUERY_LANDING_5    => 'Landing #5',
            static::QUERY_MINDMEAL     => 'mindmeal.ru',
            static::QUERY_FD           => 'finaldesire.ru и playfd.ru',
        );
    }

    public function tableColumns() {
        return array(
            'title' => array(
                'title'    => 'utm',
                'default'  => null,
                'notValue' => true,
                'data'     => 'title',
                'canHide'  => false,
                'canFiltered' => false,
                'class' => 'big-right-border text-center',
            ),
            static::GOAL_HITS => array(
                'title'   => 'Хиты',
                'default' => 0,
                'data'    => static::GOAL_HITS,
                'canHide' => false,
                'canFiltered' => true,
                'class' => 'big-right-border text-center',
            ),
            static::GOAL_REGISTER . '_per' => array(
                'title'           => 'Регистрации %',
                'default'         => 0,
                'isConversion'    => static::GOAL_HITS,
                'conversionField' => static::GOAL_REGISTER,
                'data'            => static::GOAL_REGISTER . '_per',
                'canFiltered'     => false,
                'class'           => 'text-center',
            ),
            static::GOAL_REGISTER => array(
                'title'   => 'Регистрации',
                'default' => 0,
                'data'    => static::GOAL_REGISTER,
                'canFiltered' => true,
                'class' => 'big-right-border text-center',
            ),
            static::GOAL_DOWNLOAD . '_per_hits' => array(
                'title'           => 'Загрузки % из хитов',
                'default'         => 0,
                'isConversion'    => static::GOAL_HITS,
                'conversionField' => static::GOAL_DOWNLOAD,
                'data'            => static::GOAL_DOWNLOAD . '_per_hits',
                'canFiltered'     => false,
                'class'           => 'text-center',
            ),
            static::GOAL_DOWNLOAD . '_per' => array(
                'title'           => 'Загрузки %',
                'default'         => 0,
                'isConversion'    => static::GOAL_REGISTER,
                'conversionField' => static::GOAL_DOWNLOAD,
                'data'            => static::GOAL_DOWNLOAD . '_per',
                'canFiltered'     => false,
                'class'           => 'text-center',
            ),
            static::GOAL_DOWNLOAD => array(
                'title'   => 'Загрузки',
                'default' => 0,
                'data'    => static::GOAL_DOWNLOAD,
                'canFiltered' => true,
                'class' => 'big-right-border text-center',
            ),
            static::GOAL_SOL_ENTER_GAME . '_per_hits' => array(
                'title'           => 'Входы % из хитов',
                'default'         => 0,
                'isConversion'    => static::GOAL_HITS,
                'conversionField' => static::GOAL_SOL_ENTER_GAME,
                'data'            => static::GOAL_SOL_ENTER_GAME . '_per_hits',
                'canFiltered'     => false,
                'class'           => 'text-center',
            ),
            static::GOAL_SOL_ENTER_GAME . '_per' => array(
                'title'           => 'Входы %',
                'default'         => 0,
                'isConversion'    => static::GOAL_DOWNLOAD,
                'conversionField' => static::GOAL_SOL_ENTER_GAME,
                'data'            => static::GOAL_SOL_ENTER_GAME . '_per',
                'canFiltered'     => false,
                'class'           => 'text-center',
            ),
            static::GOAL_SOL_ENTER_GAME => array(
                'title'       => 'Входы',
                'default'     => 0,
                'data'        => static::GOAL_SOL_ENTER_GAME,
                'canFiltered' => true,
                'class'       => 'text-center',
            ),
        );
    }

    /**
     * Возвращает запрос
     * @param string $column
     * @param string $query
     * @return string|null
     */
    public function getQuery($column, $query = null) {
        $config = $this->get();

        if (array_key_exists($column, $config)) {
            if (is_null($query)) {
                $query = self::QUERY_DEFAULT;
            }

            return $config[$column]['query'][$query];
        }

        return null;
    }
} 