<?php

class CampaignSvgBehavior extends CampaignImageBehavior
{
    public function renderPreview()
    {
        return sprintf('<object type="image/svg+xml" data="%s/a/%s">%s</object>', $this->webRootStat, $this->code, Yii::t('cpanel', 'Ваш браузер не поддерживает SVG'));
    }
}
