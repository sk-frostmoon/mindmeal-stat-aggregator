<?php

class CampaignImageBehavior extends CModelBehavior
{
    public $code;
    public $webRootStat;

    public function getGeneratedClientScript()
    {
        return sprintf('<script type="text/javascript" src="%s/js/stat.js?%s"></script>', $this->webRootStat, $this->code);
    }

    public function renderPreview()
    {
        return sprintf('<img src="%s/a/%s"/>', $this->webRootStat, $this->code);
    }
}
