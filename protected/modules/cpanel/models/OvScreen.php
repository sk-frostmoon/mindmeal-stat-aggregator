<?php

class OvScreen extends CActiveRecord
{
    static public function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function attributeNames()
    {
        return array(
                'id',
                'screen',
                'count',
        );
    }

    public function tableName()
    {
        return 'ov_screen';
    }

    public function attributeLabels()
    {
        return array(
                'id' => Yii::t('cpanel', 'ID'),
                'screen' => Yii::t('cpanel', 'Screen Resolution'),
                'count' => Yii::t('cpanel', 'Count'),
        );
    }

    public function rules()
    {
        return array();
    }

    public function save()
    {
        throw new CException('Model cannot be saved');
    }
}


