<? /* @var $this Controller */
$params = array(
    'error'   => array(
        'alertClass' => 'alert-danger',
        'iconClass'  => 'fa-ban',
    ),
    'warning' => array(
        'alertClass' => 'alert-warning',
        'iconClass'  => 'fa-ban',
    ),
    'notice'  => array(
        'alertClass' => 'alert-info',
        'iconClass'  => 'fa-info',
    ),
    'success' => array(
        'alertClass' => 'alert-success',
        'iconClass'  => 'fa-check',
    ),
);

if (!isset($params[$type])) {
    $type = 'warning';
}
?>
<div class="alert <?=$params[$type]['alertClass']?> alert-dismissable">
    <i class="fa <?=$params[$type]['iconClass']?>"></i>
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <?=$htmlText;?>
</div>
