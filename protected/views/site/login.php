<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = 'Login';
$this->breadcrumbs = array(
	'Login',
);
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
    'htmlOptions' => array(
        'class' => 'col-sm-6',
    ),
)); ?>
<div class="box box-info">
    <div class="box-header">
        <h4 class="box-title">Please fill out the following form with your login credentials:</h4>
    </div>
    <div class="box-body">

        <div class="form-group">Fields with <span class="required">*</span> are required.</div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'username'); ?>
            <?php echo $form->textField($model,'username',array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'password'); ?>
            <?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'password'); ?>
            <p class="help-block">
                Hint: You may login. <span style="color: white;"> OR DIE!!!</span>
            </p>
        </div>

        <div class="checkbox">
            <label class="">
                <?php echo $form->checkBox($model,'rememberMe',array('class'=>'')); ?>
                <?=$model->getAttributeLabel('rememberMe');?>
            </label>
            <?php echo $form->error($model,'rememberMe'); ?>
        </div>

        <div class="form-group buttons">
            <?php echo CHtml::submitButton('Login', array('class'=>'btn btn-primary')); ?>
        </div>

    </div>
</div><!-- form -->
<?php $this->endWidget(); ?>
