-- MySQL dump 10.14  Distrib 5.5.36-MariaDB, for Linux (x86_64)
--
-- Host: stat.mindmeal.ru    Database: mm_stat
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `created` int(11) NOT NULL,
  `activated` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_uniq` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (28,'villicus','$2a$13$rQwqCKDoC78qeW9eKt7U2.ILh6NPHtiNcpbGgeAmk07jZPPXwsgSi','admin@stat.mindmeal.ru',0,1),(29,'ak','','ak@mindmeal.ru',0,1),(30,'moderator','$2a$13$KczGHgP0IPByVAifO0MnluUlCWIA3qjkvV47jl89qWAoET6LauytK','moderator.test@stat.mindmeal.ru',0,1),(31,'user','','user.test@stat.mindmeal.ru',0,0),(32,'user3','','user3@mindmeal.ru',0,0),(33,'user4','','user4@mindmeal.ru',0,0),(34,'user5','','user5@mindmeal.ru',0,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AuthItem`
--

DROP TABLE IF EXISTS `AuthItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AuthItem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AuthItem`
--

LOCK TABLES `AuthItem` WRITE;
/*!40000 ALTER TABLE `AuthItem` DISABLE KEYS */;
INSERT INTO `AuthItem` VALUES ('addCampaign',0,'Add new campaign',NULL,'N;'),('addUser',0,'Create new users',NULL,'N;'),('admin',2,'',NULL,'N;'),('deleteUser',0,'Delete user',NULL,'N;'),('developer',2,'',NULL,'N;'),('moderator',2,'User can view, detail view and update accessed campaigns',NULL,'N;'),('updateCampaign',0,'Update campaign',NULL,'N;'),('updateUser',0,'Update user',NULL,'N;'),('user',2,'User can view and detail view of him accessed campaign',NULL,'N;'),('viewCampaign',0,'View campaign',NULL,'N;'),('viewCampaignList',0,'View all campaigns',NULL,'N;'),('viewDevelopersParts',0,'Developers parts view',NULL,'N;'),('viewUser',0,'View user',NULL,'N;'),('viewUserList',0,'View all users',NULL,'N;');
/*!40000 ALTER TABLE `AuthItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AuthAssignment`
--

DROP TABLE IF EXISTS `AuthAssignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AuthAssignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `AuthAssignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AuthAssignment`
--

LOCK TABLES `AuthAssignment` WRITE;
/*!40000 ALTER TABLE `AuthAssignment` DISABLE KEYS */;
INSERT INTO `AuthAssignment` VALUES ('admin','28',NULL,'N;'),('developer','29',NULL,'N;'),('moderator','28','return (!isset($params[\"campaign\"])) || isset($data[\"campaigns\"][$params[\"campaign\"]->id]);','a:1:{s:9:\"campaigns\";a:1:{i:1;i:1;}}'),('moderator','30','return (!isset($params[\"campaign\"])) || isset($data[\"campaigns\"][$params[\"campaign\"]->id]);','a:1:{s:9:\"campaigns\";a:2:{i:1;i:1;i:17;i:1;}}'),('moderator','31','return (!isset($params[\"campaign\"])) || isset($data[\"campaigns\"][$params[\"campaign\"]->id]);','a:1:{s:9:\"campaigns\";a:1:{i:1;i:1;}}'),('moderator','32','return (!isset($params[\"campaign\"])) || isset($data[\"campaigns\"][$params[\"campaign\"]->id]);','a:1:{s:9:\"campaigns\";a:0:{}}'),('moderator','33','return (!isset($params[\"campaign\"])) || isset($data[\"campaigns\"][$params[\"campaign\"]->id]);','a:1:{s:9:\"campaigns\";a:0:{}}'),('user','28','return (!isset($params[\"campaign\"])) || isset($data[\"campaigns\"][$params[\"campaign\"]->id]);','a:1:{s:9:\"campaigns\";a:0:{}}'),('user','29','return (!isset($params[\"campaign\"])) || isset($data[\"campaigns\"][$params[\"campaign\"]->id]);','a:1:{s:9:\"campaigns\";a:0:{}}'),('user','30','return (!isset($params[\"campaign\"])) || isset($data[\"campaigns\"][$params[\"campaign\"]->id]);','a:1:{s:9:\"campaigns\";a:1:{i:16;i:1;}}'),('user','31','return (!isset($params[\"campaign\"])) || isset($data[\"campaigns\"][$params[\"campaign\"]->id]);','a:1:{s:9:\"campaigns\";a:1:{i:16;i:1;}}'),('user','32','return (!isset($params[\"campaign\"])) || isset($data[\"campaigns\"][$params[\"campaign\"]->id]);','a:1:{s:9:\"campaigns\";a:2:{i:1;i:1;i:16;i:1;}}'),('user','33','return (!isset($params[\"campaign\"])) || isset($data[\"campaigns\"][$params[\"campaign\"]->id]);','a:1:{s:9:\"campaigns\";a:2:{i:1;i:1;i:16;i:1;}}'),('user','34','return (!isset($params[\"campaign\"])) || isset($data[\"campaigns\"][$params[\"campaign\"]->id]);','a:1:{s:9:\"campaigns\";a:0:{}}');
/*!40000 ALTER TABLE `AuthAssignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AuthItemChild`
--

DROP TABLE IF EXISTS `AuthItemChild`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AuthItemChild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `AuthItemChild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `AuthItemChild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AuthItemChild`
--

LOCK TABLES `AuthItemChild` WRITE;
/*!40000 ALTER TABLE `AuthItemChild` DISABLE KEYS */;
INSERT INTO `AuthItemChild` VALUES ('admin','addCampaign'),('admin','addUser'),('admin','deleteUser'),('admin','updateCampaign'),('moderator','updateCampaign'),('admin','updateUser'),('moderator','user'),('admin','viewCampaign'),('user','viewCampaign'),('admin','viewCampaignList'),('user','viewCampaignList'),('developer','viewDevelopersParts'),('admin','viewUser');
/*!40000 ALTER TABLE `AuthItemChild` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-28 19:59:04
