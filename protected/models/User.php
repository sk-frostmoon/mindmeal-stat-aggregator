<?php

class User extends CActiveRecord
{
    public $confirmPassword;
    const ASSIGNMENT_BIZRULE = 'return (!isset($params["campaign"])) || isset($data["campaigns"][$params["campaign"]->id]);';

    static public function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'users';
    }

    public function attributeNames()
    {
        return array(
                'id',
                'username',
                'password',
                'email',
                'created',
                'activated',
        );
    }

    public function attributeLabels()
    {
        return array(
                'username' => Yii::t('cpanel.labels', 'Username'),
        );
    }

    public function rules()
    {
        return array(
                array('username, email, activated, password, confirmPassword', 'required', 'on'=>'insert'),
                array('username, email, activated',                            'required', 'on'=>'update'),
                array('activated', 'in', 'range'=>array(0,1), 'on'=>'insert, update'),
                array('username', 'unique', 'on'=>'insert, update'),
                array('email', 'email', 'on'=>'insert, update'),
                array('username, email', 'length', 'max' => '128', 'on'=>'insert, update'),

                array('password, confirmPassword', 'required', 'on' => 'insert, changePassword'),
                array('password', 'length', 'max' => '128',    'on' => 'insert, changePassword'),
                array('confirmPassword', 'compare', 'compareAttribute'=>'password', 'on'=>'insert, changePassword'),

                array('id, username, password, email, created, activated', 'default', 'on'=>'safe'),
        );
    }

    public function assignCampaignRole($role, $campaign)
    {
        $assignment = Yii::app()->authManager->getAuthAssignment($role, $this->id);
        if ($assignment === null) {
            return Yii::app()->authManager->assign($role, $this->id, self::ASSIGNMENT_BIZRULE, array('campaigns'=>array($campaign=>1)) );
        } else {
            $data = $assignment->getData();
            if (!isset($data['campaigns'])) {
                $data['campaigns'] = array();
            }
            $data['campaigns'][$campaign] = 1;
            $assignment->setData($data);
            return true;
        }
        return false;
    }

    public function revokeCampaignRole($role, $campaign)
    {
        $assignment = Yii::app()->authManager->getAuthAssignment($role, $this->id);
        if ($assignment === null) {
            return Yii::app()->authManager->assign($role, $this->id, self::ASSIGNMENT_BIZRULE, array('campaigns'=>array($campaign=>1)) );
        } else {
            $data = $assignment->getData();
            $campaign = intval($campaign);
            if (isset($data['campaigns'][$campaign])) {
                unset($data['campaigns'][$campaign]);
                $assignment->setData($data);
                return true;
            }
        }
        return false;
    }

    public function beforeSave()
    {
        $testScenario = array(
                'insert'=>true,
                'changePassword'=>true,
        );
        if ($this->getIsNewRecord()) {
            $attributes = $this->attributes;
            $attributes['created'] = time();
            $this->attributes = $attributes;
        }
        if (isset($testScenario[$this->scenario])) {
            $this->password = crypt($this->password, UserIdentity::blowfishSalt());
        }
        return parent::beforeSave();
    }

    public function afterSave() {
        /*
        $assignments = Yii::app()->authManager->getAuthAssignments($this->id);
        if (!empty($assignments)) {
            foreach ($assignments as $key => $assignment) {
                Yii::app()->authManager->revoke($key, $this->id);
            }
        }
        Yii::app()->authManager->assign($this->role, $this->id);
        */
        return parent::afterSave();
    }

    public function getFullName()
    {
        // TODO: User profile
        return $this->username;
    }
}
