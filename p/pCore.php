<?php

class pCore {
    public $postData;
    private $Includer;
    private $_errors = array();

    /**
     * @param array $data
     */
    public function __construct($data = array()) {
        $this->postData = $data;
    }

    /**
     * @return bool
     */
    public function run() {
        try {
            if (!is_array($this->postData)) {
                $this->_errors[] = 'postData is not array';
                return false;
            }

            $this->Includer = new pInclude();
            if (!$this->Includer->initAction($this->postData['action'])) {
                $this->_errors = $this->Includer->getErrors();
                return false;
            }

            $attr = $this->postData['data'];
            $attr['action'] = $this->postData['action'];
            $attr['time'] = time();

            $model = $this->Includer->getModel();
            $model->setAttributes($attr);
            if (!$model->insert()) {
                $this->_errors = $model->getErrors();
                return false;
            }

        } catch (Exception $e) {
            $this->_errors[] = $e->getMessage();
            return false;
        }

        return true;
    }

    public function getErrors() {
        return $this->_errors;
    }
}