<?
//@sk Класс подключающий модели

class pInclude {
    /**
     * Массив ошибок
     * @var array
     */
    public $errors = array();

    /**
     * Модель, с которой работаем
     * @var p__basePushModel
     */
    public $model;

    /**
     * Путь до папки с моделями
     * @var string
     */
    protected $modelPath = null;

    /**
     * Действие для вызова модели
     * @var string
     */
    protected $action;

    public function __construct() {
        $this->setModelPath(WEBROOT . '/models/'); // Установим путь до моделей
    }

    /**
     * @param string $action
     * @return bool
     */
    public function initAction($action) {
        if (!$this->isSuitableAction($action)) {
            $this->errors[] = 'Wrong action initialization';
            return false;
        }

        $modelName = $this->getModelName($action);
        $modelPath = $this->makeModelPath($modelName);

        include_once($modelPath);

        $this->action = $action;
        $this->model  = new $modelName();

        return true;
    }

    /**
     * @return array
     */
    public function getErrors() {
        return $this->errors;
    }

    /**
     * @return p__basePushModel
     */
    public function getModel() {
        return $this->model;
    }

    /**
     * Возвращает имя модели
     * @param string $action
     * @return string
     */
    protected function getModelName($action) {
        if (file_exists(WEBROOT . '/models/' . $action . 'PushModel.php')) {
            return $action . 'PushModel';
        }

        return 'defaultPushModel';
    }

    /**
     * Строит путь до модели
     * @param string $modelName
     * @return string
     */
    protected function makeModelPath($modelName) {
        return $this->getModelPath() . $modelName . '.php';
    }

    /**
     * Подходит ли имя действия под правило
     * @param string $action
     * @return bool
     */
    protected function isSuitableAction($action) {
        return preg_match('/^[0-9a-zA-Z]+$/uis', $action) > 0;
    }

    /**
     * Возвращает путь до модели
     * @return string
     */
    public function getModelPath() {
        return $this->modelPath;
    }

    /**
     * Устанавливает путь до модели
     * @param string $modelPath
     */
    public function setModelPath($modelPath) {
        $this->modelPath = $modelPath;
    }
}

