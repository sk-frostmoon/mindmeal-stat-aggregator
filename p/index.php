<?
//@sk точка входа сбора mm.push
define('WEBROOT', __DIR__);

require_once("pInclude.php");
require_once("pCore.php");

if (empty($_POST)) {
    $postData = file_get_contents("php://input");
    $postData = json_decode($postData, true);
} else {
    $postData = $_POST;
}

$core = new pCore($postData);
$core->run();

die(__METHOD__);

// todo Оно нужно???
$core = new pInclude();
$core->init();

if($core->errors === true){
    die("Error Init");
}

$core->save();
//$core->model->debug();
