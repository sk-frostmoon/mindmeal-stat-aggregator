<?php
//@sk Модель для сбора количества фидбеков на solgame.ru
require_once (WEBROOT . '/models/p__baseCounterPushModel.php');

class solFeedbackCounterPushModel extends p__baseCounterPushModel {
    protected $dataScheme = array(
        'id',
        'action',
        'utm_user',
        'time',
        'json'
    );

    public function tableName() {
        return 'p_sol_feedback_counter';
    }
}


