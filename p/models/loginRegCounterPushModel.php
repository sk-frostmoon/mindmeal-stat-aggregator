<?php
//@sk Модель для сбора количества регистраций на forum.solgame.ru
require_once (WEBROOT . '/models/p__baseCounterPushModel.php');

class loginRegCounterPushModel extends p__baseCounterPushModel {
    protected $dataScheme = array(
        'id',
        'action',
        'utm_user',
        'time',
        'json'
    );

    public function tableName() {
        return 'p_login_reg_counter';
    }
}


