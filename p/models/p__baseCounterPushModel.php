<?php

require_once(WEBROOT . "/models/p__basePushModel.php");

abstract class p__baseCounterPushModel extends p__basePushModel {
    const GROUPBY_DAY = 'day';
    const GROUPBY_MONTH = 'month';

    private $groupBy = self::GROUPBY_DAY;

    private $filter = array(
            'dayFrom' => 0,
            'dayCount' => 29,
    );

    public $query = '';

    public function setGroupBy($group) {
        $possible = array(self::GROUPBY_DAY=>true, self::GROUPBY_MONTH=>true);
        if (isset($possible[$group])) {
            $this->groupBy = $group;
        }
    }

    public function setFilter($f) {
        if (is_array($f)) {
            $d1 = new DateTime('now');
            $d2 = new DateTime('-1 year');
            $interval = $d1->diff($d2);
            $maxDays = $interval->format('%a');
            foreach ($f as $key => $value) {
                if (isset($this->filter[$key])) {
                    $this->filter[$key] = min($value, $maxDays);
                }
            }
        }
    }

    public function getIsMonthGrouping() {
        return (strcmp($this->groupBy, self::GROUPBY_MONTH) === 0);
    }

    public function getPeriodCountDays() {
        return (int)$this->filter['dayCount'];
    }

    public function getPeriodCountMonths() {
        $dayFrom = $this->filter['dayFrom'];
        $dayCount = $this->filter['dayCount'];

        if ($this->groupBy == self::GROUPBY_MONTH) {
            $lower  = date('Y-m-01 00:00:00', strtotime('-' . ($dayCount + $dayFrom). ' days'));
            $higher = date('Y-m-t 23:59:59', strtotime('-' . $dayFrom . ' days'));
            $format = '%Y-%m';
        } else {
            $lower  = date('Y-m-d 00:00:00', strtotime('-' . ($dayCount + $dayFrom). ' days'));
            $higher = date('Y-m-d 23:59:59', strtotime('-' . $dayFrom . ' days'));
            $format = '%Y-%m-%d';
        }

        $d1 = new DateTime($lower);
        $d2 = new DateTime($higher);

        $interval = $d2->diff($d1);

        return ((int)($interval->format("%y")*12) + ((int) $interval->format("%m")));
    }

    public function getDataRaw() {
        $pdo = $this->getPdo();

        $dayFrom = $this->filter['dayFrom'];
        $dayCount = $this->filter['dayCount'];

        if ($this->groupBy == self::GROUPBY_MONTH) {
            $lower  = strtotime(date('Y-m-01 00:00:00', strtotime('-' . ($dayCount + $dayFrom). ' days')));
            $higher = strtotime(date('Y-m-t 23:59:59', strtotime('-' . $dayFrom . ' days')));
            $format = '%Y-%m';
        } else {
            $lower  = strtotime(date('Y-m-d 00:00:00', strtotime('-' . ($dayCount + $dayFrom). ' days')));
            $higher = strtotime(date('Y-m-d 23:59:59', strtotime('-' . $dayFrom . ' days')));
            $format = '%Y-%m-%d';
        }

        $this->query = $query = "SELECT COUNT(*) as cnt, DATE_FORMAT(FROM_UNIXTIME(time), '" . $format . "') as day FROM " . $this->tableName() . " "
                . " WHERE time BETWEEN " . $lower . " AND " . $higher
                . " GROUP BY day ORDER BY day desc";

        return $pdo->createCommand($query)->query();
    }

    /**
     * getMorrisMonthData
     * Returns array of counters by day
     * For example:
     *   array(
     *     array('y' => '2014-05-07', count: 2),
     *     array('y' => '2014-05-06', count: 4),
     *     array('y' => '2014-05-05', count: 0),
     *     ...
     *   )
     * 
     * @access public
     * @return array
     */
    public function getMorrisMonthData() {
        $dayFrom = $this->filter['dayFrom'];
        $dayCount = $this->filter['dayCount'];

        $data = array();
        for ($t = ($dayCount+$dayFrom); $t >= $dayFrom; $t--) {
            if ($this->groupBy == self::GROUPBY_MONTH) {
                $key = date('Y-m', strtotime('-' . $t . ' day'));
            } else {
                $key = date('Y-m-d', strtotime('-' . $t . ' day'));
            }
            if (!isset($data[$key])) {
                $data[ $key ] = array(
                        'y' => $key,
                        'count' => 0,
                );
            }
        }
    
        foreach ($this->getDataRaw() as $row) {
            if (isset($data[ $row['day'] ])) {
                $data[ $row['day'] ]['count'] = (int)$row['cnt'];
            }
        }
       
        return array_values($data);
    }

    public function getPlotData() {
        $dayFrom  = $this->filter['dayFrom'];
        $dayCount = $this->filter['dayCount'];

        $data = array();
        for ($t = ($dayCount+$dayFrom); $t >= $dayFrom; $t--) {
            if ($this->groupBy == self::GROUPBY_MONTH) {
                $key = date('Y-m', strtotime('-' . $t . ' day'));
            } else {
                $key = date('Y-m-d', strtotime('-' . $t . ' day'));
            }
            if (!isset($data[$key])) {
                $data[ $key ] = array(
                        $key,
                        0,
                );
            }
        }
    
        foreach ($this->getDataRaw() as $row) {
            if ( isset($data[ $row['day'] ]) ) {
                $data[ $row['day'] ][1] = (int)$row['cnt'];
            }
        }

        //return array_values($data);
        $maxDate = strtotime('-' . $t . ' day');
        $label = date(($this->groupBy == self::GROUPBY_MONTH) ? 'Y-m' : 'Y-m-d', $maxDate) . ': 0';
        $data = array(array(
                'data' => array_values($data),
                'label' => $label,
                'color' => '#3c8dbc',
        ));
        return $data;

    }

    public function getTableData() {
        $data = array();

        foreach($this->getDataRaw() as $row) {
            $data[ $row['day'] ] = array(
                    'day' => $row['day'],
                    'count' => (int) $row['cnt'],
            );
        }

        return array_values($data);
    }
}
