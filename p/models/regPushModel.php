<?php
//@sk Дефолтная модель для записи mm.push(...);
include_once (WEBROOT . '/models/p__basePushModel.php');

class regPushModel extends p__basePushModel {
    protected $dataScheme = array(
        'id',
        'action',
        'utm_user',
        'time',
        'json'
    );

    public function tableName() {
        return 'p_reg_push';
    }
}


?>
