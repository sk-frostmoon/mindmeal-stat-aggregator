<?php

abstract class p__basePushModel {
    private $_attributes;
    private $_pdo;
    private $_new = true;

    public $_errors = array();

    protected $dataScheme;
    protected $primaryKey = 'id';

    public function __construct() {
        if (isset($this->dataScheme['time'])) {
            $this->time = time();
        }
    }

    public function __get($name) {
        if (isset($this->_attributes[$name])) {
            return $this->_attributes[$name];
        }
        return NULL;
    }

    public function __set($name, $value) {
        $dataScheme = array_flip($this->dataScheme);
        if (isset($this->dataScheme[$name])) {
            $this->_attributes[$name] = $value;
        }
    }

    /**
     * tableName
     * 
     * @abstract
     * @access public
     * @return void
     */
    abstract function tableName();

    public function getErrors() {
        return $this->_errors;
    }

    public function getDataScheme() {
        return $this->dataScheme;
    }

    public function getPdo() {
        if (!$this->_pdo) {
            if (class_exists('Yii', false)) {
                $this->_pdo = Yii::app()->db;
            } else {
//                $this->_pdo = new PDO("mysql:host=stat.mindmeal.ru;dbname=mm_stat;", "mm_stat", "ssx5i5uJ9IG7");
                $this->_pdo = new PDO("mysql:host=localhost;dbname=mm_stat;", "mm_stat", "ssx5i5uJ9IG7");
            }
        }

        return $this->_pdo;
    }

    /**
     * setAttributes
     * 
     * @param mixed $attributes 
     * @access public
     * @return void
     */
    public function setAttributes($attributes) {
        $jsonA = array();

        $ds = array_flip($this->dataScheme);
        foreach($attributes as $field => $a) {
            if (isset($ds[$field])) {
                $this->_attributes[$field] = $a;
            } else {
                $jsonA[$field] = $a;
            }
        }

        if (isset($ds['json'])) {
            $this->_attributes['json'] = json_encode($jsonA);
        }
    }

    /**
     * insert
     *
     * @return bool
     * @throws Exception
     */
    public function insert() {
        if (!$this->getPdo() instanceof PDO) {
            throw new Exception();
        }

        $saveFields = array();
        $bindFields = array();
        foreach($this->dataScheme as $fieldName) {
            if (isset($this->_attributes[$fieldName])) {
                if ((!$this->_new) || (strcmp($fieldName, $this->primaryKey) !== 0)) {
                    $saveFields[$fieldName]       = ':' . $fieldName;
                    $bindFields[':' . $fieldName] = $this->_attributes[$fieldName];
                }
            }
        }

        if (empty($saveFields)) {
            $this->_errors[] = 'Empty request for insert';
        }

        $sql = sprintf("INSERT INTO `%s` (%s) VALUES (%s)", $this->tableName(),
                implode(', ', array_keys($saveFields)),
                implode(', ', $saveFields)
        );
        $query = $this->getPdo()->prepare($sql);

        $res = $query->execute($bindFields);
        if ($res === true) {
            return true;
        }
        $this->_errors[] = 'PDO insert error in sql request: ' . $sql;
        $this->_errors[] = implode(" ", $this->getPdo()->errorInfo());
        return false;
    }

    /**
     * getCount
     * Calling over Yii (CDbConnect only)
     *
     * @return mixed
     * @throws Exception
     */
    public function getCount() {
        $pdo = $this->getPdo();
        if ($pdo instanceof PDO) {
            throw new Exception(__METHOD__ . ' can be called only in yii framework');
        }

        return array_shift($pdo->createCommand("SELECT COUNT(*) as cnt FROM " . $this->tableName())->queryRow());
    }

    /**
     * getLastTime
     * Effectively only for non-defaultPushModel
     *
     * @return mixed
     * @throws Exception
     */
    public function getLastTime() {
        $pdo = $this->getPdo();
        if ($pdo instanceof PDO) {
            throw new Exception("Only over Yii using");
        }

        return array_shift($pdo->createCommand("SELECT max(time) FROM " . $this->tableName())->queryRow());
    }
}
