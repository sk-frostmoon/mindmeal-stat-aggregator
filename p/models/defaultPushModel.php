<?php
//@sk Дефолтная модель для записи mm.push(...);
include_once (WEBROOT . '/models/p__basePushModel.php');

class defaultPushModel extends p__basePushModel {
    protected $dataScheme = array(
        'id',
        'action',
        'time',
        'json'
    );

    public function tableName() {
        return 'p_default_push';
    }

    /**
     * findDistinctActions
     *
     * @return array|CDbDataReader
     * @throws Exception
     */
    public function findDistinctActions() {
        $pdo = $this->getPdo();
        if ($pdo instanceof CDbConnection) {
            $actions = $pdo->createCommand("select action as name, max(time) as last_time, count(action) as count_pushing from p_default_push group by action")->queryAll();
        } else {
            throw new Exception("No PDO select writed");

            // todo оно нужнно тут?
            /*
            $sql = sprintf("SELECT DISTINCT action FROM `%s`", $this->tableName());
            $query = $this->_pdo->prepare($sql);

            $query->fetchAll();
            */
        }
        return $actions;
    }
}


?>
