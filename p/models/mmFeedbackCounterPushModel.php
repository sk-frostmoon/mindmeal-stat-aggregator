<?php
//@sk Модель для сбора количества фидбеков на mindmeal.ru
require_once (WEBROOT . '/models/p__baseCounterPushModel.php');

class mmFeedbackCounterPushModel extends p__baseCounterPushModel {
    protected $dataScheme = array(
        'id',
        'action',
        'utm_user',
        'time',
        'json'
    );

    public function tableName() {
        return 'p_mm_feedback_counter';
    }
}


?>
